您好：

本压缩包下载自  http://www.joomlagate.com 网站。

如果文件名里面含有 _Pack_ 字样，基本上是一个“大礼包”形式文件，需要解压，里面含有多个安装包。

如果没有特别说明，请不要解压本压缩包，你可以通过 Joomla! 后台的安装界面直接安装 zip 或者 .tar.gz 格式的压缩文件。

如果安装包大于 500KB，推荐使用“从目录安装”的方式来安装。这种情况下，你就需要先解压本压缩包，然后将所有文件上传到网站的某个临时目录再安装。

如果您在使用过程中遇到问题，欢迎到 Joomla! 中文论坛交流。论坛地址是：

http://www.joomlagate.com/component/option,com_smf/Itemid,31/


感谢您对 joomlagate.com 网站的支持！


白建鹏

http://www.joomlagate.com


************  For Foreign Users  ***************

This package was downloaded from http://www.joomlagate.com , if you have any problem when using this package or this software, you can contact us on our website.

You also can submit your question on our forum: http://www.joomlagate.com/component/option,com_smf/Itemid,31/

Good luck!

joomlagate.com