<?php

/**
* @author: GavickPro
* @copyright: 2008
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

class GKLang
{
	var $GROUP_ADDED_CORECTLY = '新增群组成功';
	var $ERROR_WHEN_ADDING_GROUP = '新增群组时发生错误!';
	var $GROUP_NAME = '群组名称:';
	var $IMAGE_QUALITY = '图片质量 (0-100):';
	var $IMAGE_WIDTH = '图片宽度:';
	var $IMAGE_HEIGHT = '图片高度:';
	var $THUMB_WIDTH = '缩略图宽度:';
	var $THUMB_HEIGHT = '缩略图高度:';
	var $BG_COLOR = '背景色:';
	var $TITLE_COLOR = '标题颜色:';
	var $TEXT_COLOR = '文字颜色:';
	var $LINK_COLOR = '链接颜色:';
	var $HLINK_COLOR = '停留时链接颜色:';
	var $GROUP_EDITED = '编辑群组成功';
	var $GROUP_EDIT_ERROR = '编辑群组时发生错误';
	var $ERROR_REMOVING_GROUP = '移除群组时发生错误';
	var $REMOVED_GROUP = '群组已移除';
	var $SLIDE_ADDED = '新增幻灯片成功';
	var $SLIDE_ADDING_ERROR = '新增幻灯片时发生错误';
	var $SLIDE_NAME = '名称:';
	var $SLIDE_IMAGE = '图片:';
	var $SLIDE_ACCESS = '层级:';
	var $SLIDE_SPECIAL = '特定的';
	var $SLIDE_REGISTRED = '注册会员';
	var $SLIDE_PUBLIC = '公开的';
	var $SLIDE_TITLE = '标题:';
	var $SLIDE_TEXT = '文字:';
	var $SLIDE_LINKTYPE = '链接类型:';
	var $SLIDE_ARTICLE_LINK = '文章链接';
	var $SLIDE_OWN_LINK = '您自己的链接参数';
	var $SLIDE_LINKVALUE = '链接参数:';
	var $SLIDE_ARTICLE = '文章:';
	var $SLIDE_OWN_ARTICLE = '您自己的文章';
	var $SLIDE_WORDCOUNT = '字数:';
	var $SLIDE_STYLE = '图片延伸:';
	var $SLIDE_STRETCH = '延伸图片';
	var $SLIDE_NONSTRETCH = '请勿延伸图片';
	var $INVALID_TYPE = '无效的图片纇型: 必需为 JPG/PNG/GIF.';
	var $ERROR_MOVING_FILE = '移动档案时发生错误';
	var $SLIDE_EDITED = '编辑幻灯片成功.';
	var $SLIDE_EDIT_ERROR = '编辑幻灯片时发生错误.';
	var $SLIDE_REMOVED = '幻灯片移除成功';
	var $SLIDE_REMOVE_ERROR = '移除幻灯片时发生错误';
	var $GroupSettings = '群组设定';
	var $GroupName = '群组名称';
	var $ImageSize = '图片大小';
	var $ThumbnailSize = '缩略图大小';
	var $Backgroud = '背景';
	var $Title = '标题';
	var $Link = '链接';
	var $HoverLink = '停留链接';
	var $Quality = '图片质量';
	var $SlideSettings = '幻灯片设定';
	var $SlideName = '名称';
	var $SlideAccess = '访问';
	var $SlideTitle = '标题';
	var $SlideText = '文字';
	var $SlideLinkType = '链接类型';
	var $SlideLinkValue = '链接';
	var $SlideArticle = '文章ID';
	var $SlideWordcount = '字数t';
	var $SlideStretch = '幻灯片延伸';
	var $YES = '是';
	var $NO = '否';
	var $EGROUPNAME = '请设置群组名称';
	var $EIMAGEWIDTH = '\n 图片的宽度必须是整数';
	var $EIMAGEHEIGHT = '\n 图片的高度必须是整数';
	var $ETHUMBWIDTH = '\n 缩略图的宽度必须是整数';
	var $ETHUMBHEIGHT = '\n 缩略图的高度必须是整数';
	var $EBGCOLOR = '\n 背景颜色必须输入十六进制格式';
	var $ETITLECOLOR = '\n 标题颜色必须输入十六进制格式';
	var $ETEXTCOLOR = '\n 文字颜色必须输入十六进制格式';
	var $ELINKCOLOR = '\n 链接颜色必须输入十六进制格式';
	var $EHLINKCOLOR = '\n 停留链接颜色必须输入十六进制格式';
	var $ESLIDENAME = '请设置幻灯片名称';
	var $EFILE = '\n 请选择上传图片';
	var $ECONTENT = '\n 请设置您自己的内容标题及文字或选择文章';
	var $ELINKVALUE = '\n 请设置链接参数';
	var $EWORDCOUNT = '\n 字数必需是整数';
	var $SlidePreview = '预览幻灯片';
	var $SlideImage = '幻灯片';
	var $SlideThumb = '缩略图';
}

?>