<?php

/**
* @author: GavickPro
* @copyright: 2008
**/
	
// no direct access
defined('_JEXEC') or die('Restricted access');

class GKLang
{
	var $GROUP_ADDED_CORECTLY = '新增群組成功';
	var $ERROR_WHEN_ADDING_GROUP = '新增群組時發生錯誤!';
	var $GROUP_NAME = '群組名稱:';
	var $IMAGE_QUALITY = '圖片解析度 (0-100):';
	var $IMAGE_WIDTH = '圖片寬度:';
	var $IMAGE_HEIGHT = '圖片高度:';
	var $THUMB_WIDTH = '縮圖寬度:';
	var $THUMB_HEIGHT = '縮圖高度:';
	var $BG_COLOR = '背景色:';
	var $TITLE_COLOR = '標題顏色:';
	var $TEXT_COLOR = '文字顏色:';
	var $LINK_COLOR = '連結顏色:';
	var $HLINK_COLOR = '停留時連結顏色:';
	var $GROUP_EDITED = '編輯群組成功';
	var $GROUP_EDIT_ERROR = '編輯群組時發生錯誤';
	var $ERROR_REMOVING_GROUP = '移除群組時發生錯誤';
	var $REMOVED_GROUP = '群組已移除';
	var $SLIDE_ADDED = '新增幻燈片成功';
	var $SLIDE_ADDING_ERROR = '新增幻燈片時發生錯誤';
	var $SLIDE_NAME = '名稱:';
	var $SLIDE_IMAGE = '圖片:';
	var $SLIDE_ACCESS = '層級:';
	var $SLIDE_SPECIAL = '特定的';
	var $SLIDE_REGISTRED = '註冊會員';
	var $SLIDE_PUBLIC = '公開的';
	var $SLIDE_TITLE = '標題:';
	var $SLIDE_TEXT = '文字:';
	var $SLIDE_LINKTYPE = '連結類型:';
	var $SLIDE_ARTICLE_LINK = '文章連結';
	var $SLIDE_OWN_LINK = '您自己的連結參數';
	var $SLIDE_LINKVALUE = '連結參數:';
	var $SLIDE_ARTICLE = '文章:';
	var $SLIDE_OWN_ARTICLE = '您自己的文章';
	var $SLIDE_WORDCOUNT = '字數:';
	var $SLIDE_STYLE = '圖片延伸:';
	var $SLIDE_STRETCH = '延伸圖片';
	var $SLIDE_NONSTRETCH = '請勿延伸圖片';
	var $INVALID_TYPE = '無效的圖片纇型: 必需為 JPG/PNG/GIF.';
	var $ERROR_MOVING_FILE = '移動檔案時發生錯誤';
	var $SLIDE_EDITED = '編輯幻燈片成功.';
	var $SLIDE_EDIT_ERROR = '編輯幻燈片時發生錯誤.';
	var $SLIDE_REMOVED = '幻燈片移除成功';
	var $SLIDE_REMOVE_ERROR = '移除幻燈片時發生錯誤';
	var $GroupSettings = '群組設定';
	var $GroupName = '群組名稱';
	var $ImageSize = '圖片大小';
	var $ThumbnailSize = '縮圖大小';
	var $Backgroud = '背景';
	var $Title = '標題';
	var $Link = '連結';
	var $HoverLink = '停留連結';
	var $Quality = '圖片解析度';
	var $SlideSettings = '幻燈片設定';
	var $SlideName = '名稱';
	var $SlideAccess = '訪問';
	var $SlideTitle = '標題';
	var $SlideText = '文字';
	var $SlideLinkType = '連結類型';
	var $SlideLinkValue = '連結';
	var $SlideArticle = '文章ID';
	var $SlideWordcount = '字數t';
	var $SlideStretch = '幻燈片延伸';
	var $YES = '是';
	var $NO = '否';
	var $EGROUPNAME = '請設置群組名稱';
	var $EIMAGEWIDTH = '\n 圖片的寬度必須是整數';
	var $EIMAGEHEIGHT = '\n 圖片的高度必須是整數';
	var $ETHUMBWIDTH = '\n 縮圖的寬度必須是整數';
	var $ETHUMBHEIGHT = '\n 縮略圖的高度必須是整數';
	var $EBGCOLOR = '\n 背景顏色必須輸入十六進位格式';
	var $ETITLECOLOR = '\n 標題顏色必須輸入十六進位格式';
	var $ETEXTCOLOR = '\n 文字顏色必須輸入十六進位格式';
	var $ELINKCOLOR = '\n 連結顏色必須輸入十六進位格式';
	var $EHLINKCOLOR = '\n 停留連結顏色必須輸入十六進位格式';
	var $ESLIDENAME = '請設置幻燈片名稱';
	var $EFILE = '\n 請選擇上傳圖片';
	var $ECONTENT = '\n 請設置您自己的內容標題及文字或選擇文章';
	var $ELINKVALUE = '\n 請設置連結參數';
	var $EWORDCOUNT = '\n 字數必需是整數';
	var $SlidePreview = '預覽幻燈片';
	var $SlideImage = '幻燈片';
	var $SlideThumb = '縮圖';
}

?>