<?php
/*
# ------------------------------------------------------------------------
# JA Extensions Manager
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/

//no direct access
defined( '_JEXEC' ) or die( 'Retricted Access' );

global $mainframe, $option, $jauc;

$extID = $this->obj->extId;

$compareVersion = JRequest::getVar('version');
?>
<script language="javascript">
// Proccess for check update button
/*<![CDATA[*/
function submitbutton(pressbutton) {
	var form = document.adminForm;
	// Check update
	if ( pressbutton == 'upgrade'){
		doUpgrade(jQuery("[name=id]").val(), jQuery("[name=version]").val(), 'UpgradeStatus');
		return;
	}
	
	submitform( pressbutton );
}

jQuery(document).ready(function(){
	new JATooltips ([$('diffview-status-new')], {
		content: '<?php echo JText::_("These files are newly added to new version.<br /> These will be added to user site on upgrade.", true); ?>'
	});
	new JATooltips ([$('diffview-status-bmodified')], {
		content: '<?php echo JText::sprintf("<span style=\"color:red;\">Conflicted files:</span> Modified by you + and updated in %s as well.<br /> This file will be replaced with %s version file after the upgrade.<br /> You need to take backup of this file and reapply all of your customizations to the new file.", $compareVersion, $compareVersion); ?>'
	});
	new JATooltips ([$('diffview-status-updated')], {
		content: '<?php echo JText::sprintf("These files are changed in %s, and are not modified by you in your live site.<br /> These will be updated to user site on upgrade", $compareVersion, $compareVersion); ?>'
	});
	new JATooltips ([$('diffview-status-removed')], {
		content: '<?php echo JText::sprintf("These files are deleted in %s version.<br /> These will be removed from user site on upgrade.", $compareVersion, $compareVersion); ?>'
	});
	new JATooltips ([$('diffview-status-umodified')], {
		content: '<?php echo JText::sprintf("Modified by you ONLY, there is NO code change from version %s to %s.<br /> This file will not be overwritten, Your customization will be retained after the upgrade, however, if your customization cause the error,<br /> you will need to overwrite with the clean file of version %s, and then reapply your customization.", $this->obj->version, $compareVersion, $compareVersion); ?>'
	});
	new JATooltips ([$('diffview-status-ucreated')], {
		content: '<?php echo JText::_("These files are custom created by user / or were moved to new directory during the installation process.<br /> Non-extension files will not be affected. Extension files which needs to move to other folders during installation will be overwritten.", true); ?>'
	});
	new JATooltips ([$('diffview-status-nochange')], {
		content: '<?php echo JText::_("These files are not changed between the old and new version.<br /> They will not be affected by upgrade.", true); ?>'
	});
});
/*]]>*/
</script>
<form name="adminForm" id="adminForm" action="index.php" method="post">
  <?php echo JHTML::_( 'form.token'); ?>
  <input type="hidden" name="option" value="<?php echo $option;?>" />
  <input type="hidden" name="view" value="<?php echo JRequest::getVar("view", "default")?>" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="version" value="<?php echo JRequest::getVar("version")?>" />
  <input type="hidden" name="cId[]" id="cId0" value="<?php echo $extID; ?>" />
  <input type="hidden" name="id" value="<?php echo $extID; ?>" />
  <?php if (isset($this->showMessage) && $this->showMessage) : ?>
    <?php echo $this->loadTemplate('message'); ?>
  <?php endif; ?>
  <!-- Include DTree 3rd party to show file tree view -->
	<script language="javascript" src="components/<?php echo JACOMPONENT; ?>/assets/dtree/dtree.js"></script>
	<link rel="stylesheet" href="components/<?php echo JACOMPONENT; ?>/assets/dtree/dtree.css" type="text/css" />
  <link rel="stylesheet" type="text/css" src="components/<?php echo JACOMPONENT; ?>/assets/css/default.css"  />
  
<fieldset>
<legend><?php echo JText::sprintf('File Comparison between "%s" version %s and %s', $this->obj->name, $this->obj->version, $compareVersion); ?></legend>
  <div class="ja-compare-result">
	<div id="UpgradeStatus" style="color:#0066CC; font-weight:bold"></div><br/>
    <fieldset class="Legends">
              <div class="Item">
                <div class="Desc"><?php echo JText::_('Show')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo JText::_('File type')?></div>
              </div>
              <div class="Item">
                <div class="Desc">
                    <input name="file_type" type="checkbox" value="new" checked="checked" />&nbsp;
                    <img src="components/<?php echo JACOMPONENT; ?>/assets/dtree/img/icon_new.gif" />&nbsp;
                    <a href="#" id="diffview-status-new" title="" class="ja-tips-title"><?php echo JText::_('New file in new version')?></a>
                    </div>
              </div>
              <div class="Item">
                <div class="Desc">
                <input name="file_type" type="checkbox" value="bmodified" checked="checked" />&nbsp;
                <img src="components/<?php echo JACOMPONENT; ?>/assets/dtree/img/icon_bmodified.gif" />&nbsp; 
                <a href="#" id="diffview-status-bmodified" title="" class="ja-tips-title"><?php echo JText::_('CONFLICTED Files')?></a>
                </div>
              </div>
              <div class="Item">
                <div class="Desc">
                <input name="file_type" type="checkbox" value="updated" checked="checked" />&nbsp;
                <img src="components/<?php echo JACOMPONENT; ?>/assets/dtree/img/icon_updated.gif" />&nbsp; 
                <a href="#" id="diffview-status-updated" title="" class="ja-tips-title"><?php echo JText::_('Updated file in new version')?></a>
                </div>
              </div>
             <div class="Item">
                <div class="Desc">
                <input name="file_type" type="checkbox" value="removed" checked="checked" />&nbsp;
                <img src="components/<?php echo JACOMPONENT; ?>/assets/dtree/img/icon_removed.gif" />&nbsp;
                <a href="#" id="diffview-status-removed" title="" class="ja-tips-title"><?php echo JText::_('Removed file in new version')?></a>
                </div>
              </div>
               <div class="Item">
                <div class="Desc">
                <input name="file_type" type="checkbox" value="umodified" checked="checked" />&nbsp;
                <img src="components/<?php echo JACOMPONENT; ?>/assets/dtree/img/icon_umodified.gif" />&nbsp; 
                <a href="#" id="diffview-status-umodified" title="" class="ja-tips-title"><?php echo JText::_('Modified by User')?></a>
                </div>
              </div>
              <div class="Item">
                <div class="Desc">
                <input name="file_type" type="checkbox" value="ucreated" checked="checked" />&nbsp;
                <img src="components/<?php echo JACOMPONENT; ?>/assets/dtree/img/icon_ucreated.gif" />&nbsp; 
                <a href="#" id="diffview-status-ucreated" title="" class="ja-tips-title"><?php echo JText::_('Created by User')?></a>
                </div>
              </div>
              <div class="Item">
                <div class="Desc">
                <input name="file_type" type="checkbox" value="nochange" />&nbsp;
                <img src="components/<?php echo JACOMPONENT; ?>/assets/dtree/img/icon_nochange.gif" />&nbsp;
                <a href="#" id="diffview-status-nochange" title="" class="ja-tips-title"><?php echo JText::_('No change')?></a>
                </div>
              </div>

    </fieldset>
    <table class="adminlist" cellpadding="1" cellspacing="1">
      <thead>
        <tr>
          <th><?php echo JText::_("File changes") ?></th>
        </tr>
      </thead>
      <tbody>
	  	<tr>
            <td>
            	<div class="dtree">
                <!-- Write to tree -->
                <script type="text/javascript">
                    <?php
						echo printChildNode($this->obj->diffInfo);
                    ?>
                </script>
              </div>
              </td>
         </tr>
      </tbody>
    </table>
  </div>
</fieldset>
</form>

<script type="text/javascript">
/*<![CDATA[*/
jQuery(document).ready(function () {
	jaTreeAddActions('<?php echo $extID; ?>', '<?php echo $this->obj->version; ?>', '<?php echo $compareVersion; ?>');
	jaShowTreeFiles(numTreeNode, '');
	jQuery("[name=file_type]").each(function(){
		jQuery(this).click(function(){
			jaShowTreeFiles(numTreeNode, jQuery(this).val());
		});
	});
});
/*]]>*/
</script>
