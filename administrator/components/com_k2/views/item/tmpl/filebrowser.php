<?php
/**
 * @version		$Id: filebrowser.php 478 2010-06-16 16:11:42Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
//error_log('im in the browser.'); 
?>
<script>
// File browser
window.addEvent('domready', function () {
	$$('.galleryFile').addEvent('click', function(e){

		e = new Event(e).stop();
		parent.$$('input[name=existingGallery]').setProperty('value', this.getProperty('href'));
		parent.$('sbox-window').close();
	});

    // File browser
    $$('.videoFile').addEvent('click', function(e){
        e = new Event(e).stop();
        parent.$$('input[name=remoteVideo]').setProperty('value', this.getProperty('href'));
        parent.$('sbox-window').close();
    });

    // File browser
    $$('.video2File').addEvent('click', function(e){
        e = new Event(e).stop();
        parent.$$('input[name=remoteVideo2]').setProperty('value', this.getProperty('href'));
        parent.$('sbox-window').close();
    });

    $$('.imageFile').addEvent('click', function(e){
        e = new Event(e).stop();
        parent.$$('input[name=existingImage]').setProperty('value', this.getProperty('href'));
        parent.$('sbox-window').close();
    });

})
</script>
<form method="post" name="fileBrowserForm" class="fileBrowserForm" action="index.php">

	  <h1><?php echo $this->title;?></h1>

		<div id="fileBrowserFolderPath">
			<a id="fileBrowserFolderPathUp" href="<?php echo JRoute::_('index.php?option=com_k2&view=item&task=filebrowser&folder='.$this->parent.'&type='.$this->type.'&tmpl=component');?>">
				<span><?php echo JText::_('Up'); ?></span>
			</a>
			<input type="text" value="<?php echo $this->path;?>" name="path" disabled="disabled" id="addressPath" maxlength="255" />
			<div class="clr"></div>
		</div>

	  <div id="fileBrowserFolders">

			<?php if(count($this->folders)): ?>
				<?php foreach ($this->folders as $folder) :	?>
					<div class="item">
						<a href="<?php echo JRoute::_('index.php?option=com_k2&view=item&task=filebrowser&folder='.$this->path.'/'.$folder.'&type='.$this->type.'&tmpl=component');?>">
							<img alt="<?php echo $folder;?>" src="<?php echo JURI::root()?>administrator/components/com_k2/images/system/folder.png">
							<span><?php echo $folder;?></span>
						</a>
					</div>
				<?php endforeach;?>
			<?php endif;?>

			<?php if(count($this->files)): ?>
			
			<?php foreach($this->files as $file): ?>
			<?php //error_log('type is '.$this->type); ?>
			
			<?php 
			
			$icon = JURI::root().$this->path.'/'.$file;

			if($this->type=='video' || $this->type=='video2' ){
				$icon = JURI::root().'administrator/components/com_k2/images/system/video.png';
			}else if($this->type=='gallery'){
				$icon = JURI::root().'administrator/components/com_k2/images/system/zip.jpg';
			}?>
				<div class="item">
					<a href="/<?php echo $this->path.'/'.$file;?>" class="<?php echo $this->type;?>File">
					
						<img width="64" alt="<?php echo $file;?>" src="<?php echo $icon;?>">
						<span><?php echo $file;?></span>
					</a>
				</div>
			<?php endforeach;?>
			<?php endif;?>

			<div class="clr"></div>
		</div>

</form>
