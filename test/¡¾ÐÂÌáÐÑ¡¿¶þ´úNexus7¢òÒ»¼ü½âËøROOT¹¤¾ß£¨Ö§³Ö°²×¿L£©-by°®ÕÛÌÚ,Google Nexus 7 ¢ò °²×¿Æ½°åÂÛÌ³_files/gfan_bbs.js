﻿// JavaScript Document created by zxy 2012/11/08
function getDefaultStyle(obj,attribute){ 
	var result='';
	result=obj.currentStyle?obj.currentStyle[attribute]:document.defaultView.getComputedStyle(obj,false)[attribute];
	return attribute=='opacity'? Math.round(parseFloat(result)*100):parseInt(result);   
}
function startMove(obj, json, fnCallBack)
{
	clearInterval(obj.timer);
	obj.timer=setInterval(function (){
		var attr,bStop=true;	
		for(attr in json)
		{
			var iCur=getDefaultStyle(obj, attr);
			var iSpeed=(json[attr]-iCur)/5;
			iSpeed=iSpeed>0?Math.ceil(iSpeed):Math.floor(iSpeed);
			if(iCur!=json[attr]){bStop=false;}			
			if(attr=='opacity'){obj.style.filter='alpha(opacity:'+(iCur+iSpeed)+')';obj.style.opacity=(iCur+iSpeed)/100;}
			else{obj.style[attr]=iCur+iSpeed+'px';}
		}		
		if(bStop)
		{
			clearInterval(obj.timer);			
			if(fnCallBack){fnCallBack();}
		}
	}, 30);
}
function myAddEvent(obj, sEventName, fnHandler)
{obj.attachEvent?obj.attachEvent('on'+sEventName, fnHandler):obj.addEventListener(sEventName, fnHandler, false);};
function getByClass(oParent, sClass){
	oParent=oParent||document;
	var aEle=oParent.getElementsByTagName('*');
	var re=new RegExp('(^|\\s)'+sClass+'(\\s|$)', 'i');
	var result=[];
	for(var i=0;i<aEle.length;i++){
		if(re.test(aEle[i].className)){result.push(aEle[i]);}
	}
	return result;
}
function hoverTab(oParent,aTitle,aTabs){
	var i=0;	
	for(i=0;i<aTitle.length;i++){
		aTitle[i].index=i;
		aTitle[i].onmouseover=function(){
			for(var j=0;j<aTitle.length;j++){
				aTitle[j].className='';
				aTabs[j].style.display='none';	
			}
			aTitle[this.index].className='active';	
			aTabs[this.index].style.display='block';
		};	
	};	
};

/*选项卡切换公用*/
function switchTab(oTitle,oTabs){
	var aTitle=oTitle.getElementsByTagName('a');
	var aTabs= oTabs.getElementsByTagName('ul');;
	var i=0;	
	for(i=0;i<aTitle.length;i++){
		aTitle[i].index=i;
		aTitle[i].onclick=function(){
			for(var j=0;j<aTitle.length;j++){
				aTitle[j].className='';
				aTabs[j].style.display='none';	
			}
			aTitle[this.index].className='current';
			var oTextarea=aTabs[this.index].getElementsByTagName('textarea')[0];
			if(oTextarea){
				aTabs[this.index].innerHTML=oTextarea.value;
			}
			aTabs[this.index].style.display='block';
		};	
	};	
};
//公告滚动效果
function gfan_announcement() {
	var ann = new Object();
	ann.anndelay = 3000;ann.annst = 0;ann.annstop = 0;ann.annrowcount = 0;ann.anncount = 0;ann.annlis = $('gfan_anc').getElementsByTagName("li");ann.annrows = new Array();
	ann.announcementScroll = function () {
		if(this.annstop) {this.annst = setTimeout(function () {ann.announcementScroll();}, this.anndelay);return;}
		if(!this.annst) {
			var lasttop = -1;
			for(i = 0;i < this.annlis.length;i++) {
				if(lasttop != this.annlis[i].offsetTop) {
					if(lasttop == -1) lasttop = 0;
					this.annrows[this.annrowcount] = this.annlis[i].offsetTop - lasttop;this.annrowcount++;
				}
				lasttop = this.annlis[i].offsetTop;
			}
			if(this.annrows.length == 1) {
				$('gfan_an').onmouseover = $('gfan_an').onmouseout = null;
			} else {
				this.annrows[this.annrowcount] = this.annrows[1];
				$('gfan_ancl').innerHTML += $('gfan_ancl').innerHTML;
				this.annst = setTimeout(function () {ann.announcementScroll();}, this.anndelay);
				$('gfan_an').onmouseover = function () {ann.annstop = 1;};
				$('gfan_an').onmouseout = function () {ann.annstop = 0;};
			}
			this.annrowcount = 1;
			return;
		}
		if(this.annrowcount >= this.annrows.length) {
			$('gfan_anc').scrollTop = 0;
			this.annrowcount = 1;
			this.annst = setTimeout(function () {ann.announcementScroll();}, this.anndelay);
		} else {
			this.anncount = 0;
			this.announcementScrollnext(this.annrows[this.annrowcount]);
		}
	};
	ann.announcementScrollnext = function (time) {
		$('gfan_anc').scrollTop++;
		this.anncount++;
		if(this.anncount != time) {
			this.annst = setTimeout(function () {ann.announcementScrollnext(time);}, 10);
		} else {
			this.annrowcount++;
			this.annst = setTimeout(function () {ann.announcementScroll();}, this.anndelay);
		}
	};
	ann.announcementScroll();
}


/**
*<gfan>
*列表页改版js效果
*/
function singleShow(oParent){
    var arankLi=oParent.getElementsByTagName('li');
    for(var j=0;j<arankLi.length;j++){
        arankLi[j].index=j;
        arankLi[j].onmouseover=function(){
            for(var k=0;k<arankLi.length;k++){
                arankLi[k].className='';	
            };
            arankLi[this.index].className='current';	
        }
    }		
}

function extraRank(){
    var oERankUlR = document.getElementById('extraRankR');
    var oERankUlL = document.getElementById('extraRankL');
    if(oERankUlR){
        singleShow(oERankUlR);	
    }
    if(oERankUlL){
        singleShow(oERankUlL);	
    }
}
myAddEvent(window, "load", extraRank);
