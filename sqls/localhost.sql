-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 03, 2011 at 04:38 PM
-- Server version: 5.1.54
-- PHP Version: 5.3.5-1ubuntu7.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `joomla_video`
--
CREATE DATABASE `joomla_video` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `joomla_video`;

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_banner`
--

DROP TABLE IF EXISTS `ja_tel_banner`;
CREATE TABLE IF NOT EXISTS `ja_tel_banner` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) NOT NULL DEFAULT 'banner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(100) NOT NULL DEFAULT '',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `date` datetime DEFAULT NULL,
  `showBanner` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `viewbanner` (`showBanner`),
  KEY `idx_banner_catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ja_tel_banner`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_bannerclient`
--

DROP TABLE IF EXISTS `ja_tel_bannerclient`;
CREATE TABLE IF NOT EXISTS `ja_tel_bannerclient` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` time DEFAULT NULL,
  `editor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ja_tel_bannerclient`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_bannertrack`
--

DROP TABLE IF EXISTS `ja_tel_bannertrack`;
CREATE TABLE IF NOT EXISTS `ja_tel_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_bannertrack`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_categories`
--

DROP TABLE IF EXISTS `ja_tel_categories`;
CREATE TABLE IF NOT EXISTS `ja_tel_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `ja_tel_categories`
--

INSERT INTO `ja_tel_categories` (`id`, `parent_id`, `title`, `name`, `alias`, `image`, `section`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `editor`, `ordering`, `access`, `count`, `params`) VALUES
(98, 0, '视频类', '', '2011-02-05-09-07-06', 'articles.jpg', '15', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(99, 0, '漫画类', '', '2011-02-05-09-07-28', '', '15', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_components`
--

DROP TABLE IF EXISTS `ja_tel_components`;
CREATE TABLE IF NOT EXISTS `ja_tel_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_menu_link` varchar(255) NOT NULL DEFAULT '',
  `admin_menu_alt` varchar(255) NOT NULL DEFAULT '',
  `option` varchar(50) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `admin_menu_img` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_option` (`parent`,`option`(32))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `ja_tel_components`
--

INSERT INTO `ja_tel_components` (`id`, `name`, `link`, `menuid`, `parent`, `admin_menu_link`, `admin_menu_alt`, `option`, `ordering`, `admin_menu_img`, `iscore`, `params`, `enabled`) VALUES
(34, 'JA Extensions Manager', 'option=com_jaextmanager', 0, 0, 'option=com_jaextmanager', 'JA Extensions Manager', 'com_jaextmanager', 0, 'components/com_jaextmanager/assets/images/jauc.png', 0, '', 1),
(1, 'Banners', '', 0, 0, '', 'Banner Management', 'com_banners', 0, 'js/ThemeOffice/component.png', 0, 'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n', 1),
(2, 'Banners', '', 0, 1, 'option=com_banners', 'Active Banners', 'com_banners', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(3, 'Clients', '', 0, 1, 'option=com_banners&c=client', 'Manage Clients', 'com_banners', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(4, 'Web Links', 'option=com_weblinks', 0, 0, '', 'Manage Weblinks', 'com_weblinks', 0, 'js/ThemeOffice/component.png', 0, 'show_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 1),
(5, 'Links', '', 0, 4, 'option=com_weblinks', 'View existing weblinks', 'com_weblinks', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(6, 'Categories', '', 0, 4, 'option=com_categories&section=com_weblinks', 'Manage weblink categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(7, 'Contacts', 'option=com_contact', 0, 0, '', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/component.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(8, 'Contacts', '', 0, 7, 'option=com_contact', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/edit.png', 1, '', 1),
(9, 'Categories', '', 0, 7, 'option=com_categories&section=com_contact_details', 'Manage contact categories', '', 2, 'js/ThemeOffice/categories.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(10, 'Polls', 'option=com_poll', 0, 0, 'option=com_poll', 'Manage Polls', 'com_poll', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(11, 'News Feeds', 'option=com_newsfeeds', 0, 0, '', 'News Feeds Management', 'com_newsfeeds', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(12, 'Feeds', '', 0, 11, 'option=com_newsfeeds', 'Manage News Feeds', 'com_newsfeeds', 1, 'js/ThemeOffice/edit.png', 0, 'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 1),
(13, 'Categories', '', 0, 11, 'option=com_categories&section=com_newsfeeds', 'Manage Categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(14, 'User', 'option=com_user', 0, 0, '', '', 'com_user', 0, '', 1, '', 1),
(15, 'Search', 'option=com_search', 0, 0, 'option=com_search', 'Search Statistics', 'com_search', 0, 'js/ThemeOffice/component.png', 1, 'enabled=0\n\n', 1),
(16, 'Categories', '', 0, 1, 'option=com_categories&section=com_banner', 'Categories', '', 3, '', 1, '', 1),
(17, 'Wrapper', 'option=com_wrapper', 0, 0, '', 'Wrapper', 'com_wrapper', 0, '', 1, '', 1),
(18, 'Mail To', '', 0, 0, '', '', 'com_mailto', 0, '', 1, '', 1),
(19, 'Media Manager', '', 0, 0, 'option=com_media', 'Media Manager', 'com_media', 0, '', 1, 'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\nimage_path=images/stories\nrestrict_uploads=1\nallowed_media_usergroup=3\ncheck_mime=1\nimage_extensions=bmp,gif,jpg,png\nignore_extensions=\nupload_mime=image/jpeg,image/gif,image/png,image/bmp,application/x-shockwave-flash,application/msword,application/excel,application/pdf,application/powerpoint,text/plain,application/x-zip\nupload_mime_illegal=text/html\nenable_flash=1\n\n', 1),
(20, 'Articles', 'option=com_content', 0, 0, '', '', 'com_content', 0, '', 1, 'show_noauth=0\nshow_title=1\nlink_titles=1\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=0\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=0\nfilter_tags=\nfilter_attritbutes=\n\n', 1),
(21, 'Configuration Manager', '', 0, 0, '', 'Configuration', 'com_config', 0, '', 1, '', 1),
(22, 'Installation Manager', '', 0, 0, '', 'Installer', 'com_installer', 0, '', 1, '', 1),
(23, 'Language Manager', '', 0, 0, '', 'Languages', 'com_languages', 0, '', 1, 'administrator=en-GB\nsite=en-GB', 1),
(24, 'Mass mail', '', 0, 0, '', 'Mass Mail', 'com_massmail', 0, '', 1, 'mailSubjectPrefix=\nmailBodySuffix=\n\n', 1),
(25, 'Menu Editor', '', 0, 0, '', 'Menu Editor', 'com_menus', 0, '', 1, '', 1),
(27, 'Messaging', '', 0, 0, '', 'Messages', 'com_messages', 0, '', 1, '', 1),
(28, 'Modules Manager', '', 0, 0, '', 'Modules', 'com_modules', 0, '', 1, '', 1),
(29, 'Plugin Manager', '', 0, 0, '', 'Plugins', 'com_plugins', 0, '', 1, '', 1),
(30, 'Template Manager', '', 0, 0, '', 'Templates', 'com_templates', 0, '', 1, '', 1),
(31, 'User Manager', '', 0, 0, '', 'Users', 'com_users', 0, '', 1, 'allowUserRegistration=1\nnew_usertype=Registered\nuseractivation=1\nfrontend_userparams=1\n\n', 1),
(32, 'Cache Manager', '', 0, 0, '', 'Cache', 'com_cache', 0, '', 1, '', 1),
(33, 'Control Panel', '', 0, 0, '', 'Control Panel', 'com_cpanel', 0, '', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_contact_details`
--

DROP TABLE IF EXISTS `ja_tel_contact_details`;
CREATE TABLE IF NOT EXISTS `ja_tel_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ja_tel_contact_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_content`
--

DROP TABLE IF EXISTS `ja_tel_content`;
CREATE TABLE IF NOT EXISTS `ja_tel_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `title_alias` varchar(255) NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(11) unsigned NOT NULL DEFAULT '0',
  `mask` int(11) unsigned NOT NULL DEFAULT '0',
  `catid` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_section` (`sectionid`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=391 ;

--
-- Dumping data for table `ja_tel_content`
--

INSERT INTO `ja_tel_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(390, '视频1', '1', '', '<p>视频一</p>', '', 1, 15, 0, 98, '2011-02-05 09:10:22', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-02-05 09:10:22', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 1, '', '', 0, 0, 'robots=\nauthor=');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_content_frontpage`
--

DROP TABLE IF EXISTS `ja_tel_content_frontpage`;
CREATE TABLE IF NOT EXISTS `ja_tel_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_content_frontpage`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_content_rating`
--

DROP TABLE IF EXISTS `ja_tel_content_rating`;
CREATE TABLE IF NOT EXISTS `ja_tel_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_content_rating`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_core_acl_aro`
--

DROP TABLE IF EXISTS `ja_tel_core_acl_aro`;
CREATE TABLE IF NOT EXISTS `ja_tel_core_acl_aro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_value` varchar(240) NOT NULL DEFAULT '0',
  `value` varchar(240) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ja_tel_section_value_value_aro` (`section_value`(100),`value`(100)),
  KEY `ja_tel_gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ja_tel_core_acl_aro`
--

INSERT INTO `ja_tel_core_acl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', '62', 0, 'Administrator', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_core_acl_aro_groups`
--

DROP TABLE IF EXISTS `ja_tel_core_acl_aro_groups`;
CREATE TABLE IF NOT EXISTS `ja_tel_core_acl_aro_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ja_tel_gacl_parent_id_aro_groups` (`parent_id`),
  KEY `ja_tel_gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `ja_tel_core_acl_aro_groups`
--

INSERT INTO `ja_tel_core_acl_aro_groups` (`id`, `parent_id`, `name`, `lft`, `rgt`, `value`) VALUES
(17, 0, 'ROOT', 1, 22, 'ROOT'),
(28, 17, 'USERS', 2, 21, 'USERS'),
(29, 28, 'Public Frontend', 3, 12, 'Public Frontend'),
(18, 29, 'Registered', 4, 11, 'Registered'),
(19, 18, 'Author', 5, 10, 'Author'),
(20, 19, 'Editor', 6, 9, 'Editor'),
(21, 20, 'Publisher', 7, 8, 'Publisher'),
(30, 28, 'Public Backend', 13, 20, 'Public Backend'),
(23, 30, 'Manager', 14, 19, 'Manager'),
(24, 23, 'Administrator', 15, 18, 'Administrator'),
(25, 24, 'Super Administrator', 16, 17, 'Super Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_core_acl_aro_map`
--

DROP TABLE IF EXISTS `ja_tel_core_acl_aro_map`;
CREATE TABLE IF NOT EXISTS `ja_tel_core_acl_aro_map` (
  `acl_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(230) NOT NULL DEFAULT '0',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_core_acl_aro_map`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_core_acl_aro_sections`
--

DROP TABLE IF EXISTS `ja_tel_core_acl_aro_sections`;
CREATE TABLE IF NOT EXISTS `ja_tel_core_acl_aro_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(230) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(230) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ja_tel_gacl_value_aro_sections` (`value`),
  KEY `ja_tel_gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ja_tel_core_acl_aro_sections`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_core_acl_groups_aro_map`
--

DROP TABLE IF EXISTS `ja_tel_core_acl_groups_aro_map`;
CREATE TABLE IF NOT EXISTS `ja_tel_core_acl_groups_aro_map` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(240) NOT NULL DEFAULT '',
  `aro_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `group_id_aro_id_groups_aro_map` (`group_id`,`section_value`,`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_core_acl_groups_aro_map`
--

INSERT INTO `ja_tel_core_acl_groups_aro_map` (`group_id`, `section_value`, `aro_id`) VALUES
(25, '', 10);

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_core_log_items`
--

DROP TABLE IF EXISTS `ja_tel_core_log_items`;
CREATE TABLE IF NOT EXISTS `ja_tel_core_log_items` (
  `time_stamp` date NOT NULL DEFAULT '0000-00-00',
  `item_table` varchar(50) NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_core_log_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_core_log_searches`
--

DROP TABLE IF EXISTS `ja_tel_core_log_searches`;
CREATE TABLE IF NOT EXISTS `ja_tel_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_core_log_searches`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_groups`
--

DROP TABLE IF EXISTS `ja_tel_groups`;
CREATE TABLE IF NOT EXISTS `ja_tel_groups` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_groups`
--

INSERT INTO `ja_tel_groups` (`id`, `name`) VALUES
(0, 'Public'),
(1, 'Registered'),
(2, 'Special');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_jaem_log`
--

DROP TABLE IF EXISTS `ja_tel_jaem_log`;
CREATE TABLE IF NOT EXISTS `ja_tel_jaem_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_id` varchar(50) DEFAULT NULL,
  `check_date` datetime DEFAULT NULL,
  `check_info` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ext_id` (`ext_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ja_tel_jaem_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_jaem_services`
--

DROP TABLE IF EXISTS `ja_tel_jaem_services`;
CREATE TABLE IF NOT EXISTS `ja_tel_jaem_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ws_name` varchar(255) NOT NULL,
  `ws_mode` varchar(50) NOT NULL DEFAULT 'local',
  `ws_uri` varchar(255) NOT NULL,
  `ws_user` varchar(100) NOT NULL,
  `ws_pass` varchar(100) NOT NULL,
  `ws_default` tinyint(1) NOT NULL DEFAULT '0',
  `ws_core` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ja_tel_jaem_services`
--

INSERT INTO `ja_tel_jaem_services` (`id`, `ws_name`, `ws_mode`, `ws_uri`, `ws_user`, `ws_pass`, `ws_default`, `ws_core`) VALUES
(1, 'Local Service', 'local', '', '', '', 1, 1),
(2, 'JoomlArt Updates', 'remote', 'http://update.joomlart.com/service/', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_menu`
--

DROP TABLE IF EXISTS `ja_tel_menu`;
CREATE TABLE IF NOT EXISTS `ja_tel_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text,
  `type` varchar(50) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `componentid` int(11) unsigned NOT NULL DEFAULT '0',
  `sublevel` int(11) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pollid` int(11) NOT NULL DEFAULT '0',
  `browserNav` tinyint(4) DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `utaccess` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `lft` int(11) unsigned NOT NULL DEFAULT '0',
  `rgt` int(11) unsigned NOT NULL DEFAULT '0',
  `home` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `componentid` (`componentid`,`menutype`,`published`,`access`),
  KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=478 ;

--
-- Dumping data for table `ja_tel_menu`
--

INSERT INTO `ja_tel_menu` (`id`, `menutype`, `name`, `alias`, `link`, `type`, `published`, `parent`, `componentid`, `sublevel`, `ordering`, `checked_out`, `checked_out_time`, `pollid`, `browserNav`, `access`, `utaccess`, `params`, `lft`, `rgt`, `home`) VALUES
(1, 'mainmenu', 'Home', 'home', 'index.php?option=com_content&view=frontpage', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'num_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=front\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 1),
(472, 'magazine', 'Home', 'home', 'index.php?option=com_content&view=frontpage', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'num_leading_articles=1\nnum_intro_articles=8\nnum_columns=2\nnum_links=0\norderby_pri=\norderby_sec=front\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\npage_title=Welcome to the Frontpage\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 1),
(473, 'magazine', '政策类', '', 'index.php?option=com_content&view=section&layout=blog&id=15', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'num_leading_articles=1\nnum_intro_articles=8\nnum_columns=2\nnum_links=0\n\n', 0, 0, 0),
(474, 'magazine', '青春期', '', 'index.php?option=com_content&view=section&layout=blog&id=16', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'num_leading_articles=1\nnum_intro_articles=8\nnum_columns=2\nnum_links=0\n\n', 0, 0, 0),
(475, 'magazine', '视频类', '', 'index.php?option=com_content&view=category&layout=blog&id=98', 'component', 1, 473, 20, 1, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'num_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=0\n\n', 0, 0, 0),
(476, 'magazine', '漫画类', '', 'index.php?option=com_content&view=category&layout=blog&id=99', 'component', 1, 473, 20, 1, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'num_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=0\n\n', 0, 0, 0),
(477, 'mainmenu', '视频类', '2011-02-05-09-09-31', 'index.php?option=com_content&view=category&layout=blog&id=98', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\nmega_showtitle=1\nmega_desc=\nmega_cols=1\nmega_group=0\nmega_width=\nmega_colw=\nmega_colxw=\nmega_class=\nmega_subcontent=0\n\n', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_menu_types`
--

DROP TABLE IF EXISTS `ja_tel_menu_types`;
CREATE TABLE IF NOT EXISTS `ja_tel_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ja_tel_menu_types`
--

INSERT INTO `ja_tel_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(14, 'magazine', 'magazine', '');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_messages`
--

DROP TABLE IF EXISTS `ja_tel_messages`;
CREATE TABLE IF NOT EXISTS `ja_tel_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(11) NOT NULL DEFAULT '0',
  `priority` int(1) unsigned NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ja_tel_messages`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_messages_cfg`
--

DROP TABLE IF EXISTS `ja_tel_messages_cfg`;
CREATE TABLE IF NOT EXISTS `ja_tel_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_messages_cfg`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_migration_backlinks`
--

DROP TABLE IF EXISTS `ja_tel_migration_backlinks`;
CREATE TABLE IF NOT EXISTS `ja_tel_migration_backlinks` (
  `itemid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `sefurl` text NOT NULL,
  `newurl` text NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_migration_backlinks`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_modules`
--

DROP TABLE IF EXISTS `ja_tel_modules`;
CREATE TABLE IF NOT EXISTS `ja_tel_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) DEFAULT NULL,
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `numnews` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `control` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=135 ;

--
-- Dumping data for table `ja_tel_modules`
--

INSERT INTO `ja_tel_modules` (`id`, `title`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `published`, `module`, `numnews`, `access`, `showtitle`, `params`, `iscore`, `client_id`, `control`) VALUES
(1, 'Main Menu', '', 0, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 1, 'menutype=mainmenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(2, 'Login', '', 1, 'login', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, '', 1, 1, ''),
(3, 'Popular', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_popular', 0, 2, 1, '', 0, 1, ''),
(4, 'Recent added Articles', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_latest', 0, 2, 1, 'ordering=c_dsc\nuser_id=0\ncache=0\n\n', 0, 1, ''),
(5, 'Menu Stats', '', 5, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_stats', 0, 2, 1, '', 0, 1, ''),
(6, 'Unread Messages', '', 1, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_unread', 0, 2, 1, '', 1, 1, ''),
(7, 'Online Users', '', 2, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_online', 0, 2, 1, '', 1, 1, ''),
(8, 'Toolbar', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', 1, 'mod_toolbar', 0, 2, 1, '', 1, 1, ''),
(9, 'Quick Icons', '', 1, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_quickicon', 0, 2, 1, '', 1, 1, ''),
(10, 'Logged in Users', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_logged', 0, 2, 1, '', 0, 1, ''),
(11, 'Footer', '', 0, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_footer', 0, 0, 1, '', 1, 1, ''),
(12, 'Admin Menu', '', 1, 'menu', 0, '0000-00-00 00:00:00', 1, 'mod_menu', 0, 2, 1, '', 0, 1, ''),
(13, 'Admin SubMenu', '', 1, 'submenu', 0, '0000-00-00 00:00:00', 1, 'mod_submenu', 0, 2, 1, '', 0, 1, ''),
(14, 'User Status', '', 1, 'status', 0, '0000-00-00 00:00:00', 1, 'mod_status', 0, 2, 1, '', 0, 1, ''),
(15, 'Title', '', 1, 'title', 0, '0000-00-00 00:00:00', 1, 'mod_title', 0, 2, 1, '', 0, 1, ''),
(16, 'Polls', '', 7, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_poll', 0, 0, 1, 'id=14\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(17, 'User Menu', '', 12, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 1, 1, 'menutype=usermenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(18, 'Member', '', 3, 'mega-item', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 0, 'cache=0\nmoduleclass_sfx=_icon1\npretext=\nposttext=\nlogin=\nlogout=\ngreeting=1\nname=0\nusesecure=0\n\n', 1, 0, ''),
(19, 'Most Reviews', '', 3, 'ja-tabs', 0, '0000-00-00 00:00:00', 0, 'mod_latestnews', 0, 0, 1, 'count=5\nordering=c_dsc\nuser_id=0\nshow_front=1\nsecid=\ncatid=\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 1, 0, ''),
(20, 'Statistics', '', 11, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_stats', 0, 0, 1, 'serverinfo=1\nsiteinfo=1\ncounter=1\nincrease=0\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(21, 'Who''s Online', '', 6, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_whosonline', 0, 0, 1, 'cache=0\nshowmode=0\nmoduleclass_sfx=\n\n', 0, 0, ''),
(22, 'Popular', '', 2, 'user6', 0, '0000-00-00 00:00:00', 0, 'mod_mostread', 0, 0, 1, 'moduleclass_sfx=\nshow_front=1\ncount=5\ncatid=\nsecid=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(23, 'Archive', '', 14, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_archive', 0, 0, 1, 'cache=1', 1, 0, ''),
(24, 'Sections', '', 0, 'mootabs', 0, '0000-00-00 00:00:00', 0, 'mod_sections', 0, 0, 1, 'count=5\nmoduleclass_sfx=\nshow_noauth=0\ncache=1\ncache_time=900\n\n', 1, 0, ''),
(25, 'Newsflash', '', 1, 'top', 0, '0000-00-00 00:00:00', 1, 'mod_newsflash', 0, 0, 1, 'catid=3\r\nstyle=random\r\nitems=\r\nmoduleclass_sfx=', 0, 0, ''),
(26, 'Related Items', '', 15, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_related_items', 0, 0, 1, '', 0, 0, ''),
(27, 'Search', '', 0, 'search', 0, '0000-00-00 00:00:00', 1, 'mod_search', 0, 0, 0, 'moduleclass_sfx=\nwidth=20\ntext=\nbutton=1\nbutton_pos=right\nimagebutton=\nbutton_text=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(28, 'Random Image', '', 3, 'user6', 0, '0000-00-00 00:00:00', 0, 'mod_random_image', 0, 0, 1, 'type=jpg\nfolder=images/stories\nlink=\nwidth=\nheight=\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(29, 'Top Menu', '', 1, 'footnav', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 0, 'menutype=topmenu\nmenu_style=list_flat\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=-nav\nmoduleclass_sfx=\nmaxdepth=10\nmenu_images_link=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=-1\nindent_image2=-1\nindent_image3=-1\nindent_image4=-1\nindent_image5=-1\nindent_image6=-1\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(30, 'Sport', '', 9, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_banners', 0, 0, 0, 'target=1\ncount=1\ncid=2\ncatid=97\ntag_search=0\nordering=random\nheader_text=\nfooter_text=\nmoduleclass_sfx=\ncache=1\ncache_time=15\n\n', 1, 0, ''),
(31, 'Resources', '', 2, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 1, 'menutype=othermenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 0, 0, ''),
(32, 'Wrapper', '', 16, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_wrapper', 0, 0, 1, '', 0, 0, ''),
(33, 'Footer', '', 1, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_footer', 0, 0, 0, 'cache=1\n\n', 1, 0, ''),
(34, 'Feed Display', '', 17, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_feed', 0, 0, 1, '', 1, 0, ''),
(35, 'Breadcrumbs', '', 1, 'breadcrumb', 0, '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 0, 0, 1, 'moduleclass_sfx=\ncache=0\nshowHome=1\nhomeText=Home\nshowComponent=1\nseparator=\n\n', 1, 0, ''),
(36, 'Syndication', '', 5, 'mega-item', 0, '0000-00-00 00:00:00', 1, 'mod_syndicate', 0, 0, 0, 'cache=0\ntext=Feed Entries\nformat=rss\nmoduleclass_sfx=\n\n', 1, 0, ''),
(38, 'Advertisement', '', 2, 'col-mass2', 0, '0000-00-00 00:00:00', 1, 'mod_banners', 0, 0, 1, 'target=1\ncount=4\ncid=0\ncatid=14\ntag_search=0\nordering=0\nheader_text=Featured Links:\nfooter_text=<a href="http://www.joomla.org">Ads by Joomla!</a>\nmoduleclass_sfx=_text\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(39, 'Example Pages', '', 13, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 1, 'cache=1\nclass_sfx=\nmoduleclass_sfx=_menu\nmenutype=ExamplePages\nmenu_style=list_flat\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nfull_active_id=0\nmenu_images=0\nmenu_images_align=0\nexpand_menu=0\nactivate_parent=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\nwindow_open=\n\n', 0, 0, ''),
(40, 'Key Concepts', '', 0, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 0, 1, 'menutype=keyconcepts\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 0, 0, ''),
(41, 'Welcome to Joomla!', '<div style="padding: 5px"><p>Congratulations on choosing Joomla! as your content management system. We hope you are able to create a successful Web site with our program and maybe you will be able to give something back to the community later.</p><p>To make your start with Joomla! as smooth as possible, we want to give you some pointers to documentation, common questions and help on securing your server. A good place to start is the &quot;<a href="http://docs.joomla.org/beginners" target="_blank">Absolute Beginners Guide to Joomla!</a>&quot;.</p><p>For your most common questions the best place to start looking for answers is the <a href="http://help.joomla.org/component/option,com_easyfaq/Itemid,268/" target="_blank">Frequently Asked Questions (FAQ)</a> area.  If you are stuck on a particular screen in the Administration area of Joomla! (which is where you are now), then try clicking on the Help toolbar button which you will find on almost all screens.  This will take you to a page about that screen on our <a href="http://help.joomla.org" target="_blank">Help Site</a>.  If you still have questions then take a look on our <a href="http://forum.joomla.org" target="_blank">forum</a> which is most likely the biggest resource on Joomla! there is and you will find almost every question answered at least once, so please try using the search function before asking your question.</p><p>Security is a big concern for us, which is why we would like you to subscribe to the <a href="http://forum.joomla.org/index.php/board,8.0.html" target="_blank">announcement forum</a> (by clicking on the Notify button) to always get the latest information on new releases for Joomla! You should also read the <a href="http://help.joomla.org/component/option,com_easyfaq/task,view/id,167/Itemid,268/" target="_blank">Joomla! Administrator''s Security Checklist</a> and regularly check the <a href="http://forum.joomla.org/index.php/board,267.0.html" target="_blank">security forum</a>.</p><p>We hope you have much fun and success with Joomla! and hope to see you in the forum among the hundreds and thousands of contributors.</p><p>Your Joomla! team.<img alt="Smile" border="0" src="../plugins/editors/tinymce/jscripts/tiny_mce/plugins/emotions/images/smiley-smile.gif" title="Smile" /></p><p>P.S.: To remove this message, delete the &quot;Welcome to Joomla!&quot; Module in the Administrator screen of the Module Manager (on the Extensions menu).  Here is a <a href="index.php?option=com_modules&amp;client=1">quick link</a> to this screen.</p></div>', 1, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 2, 1, 'moduleclass_sfx=\n\n', 1, 1, ''),
(85, 'Feature news', '<div class="ja-innerdiv">\r\n<h4>World</h4>\r\n<img src="images/stories/demo/other/cat-1.jpg" alt="Obama" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Sport</h4>\r\n<img src="images/stories/demo/other/cat-2.jpg" alt="Obama" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Food and Drink</h4>\r\n<img src="images/stories/demo/other/cat-3.jpg" alt="Obama" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Art</h4>\r\n<img src="images/stories/demo/other/cat-4.jpg" alt="Obama" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Entertaiment</h4>\r\n<img src="images/stories/demo/other/cat-5.jpg" alt="Obama" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Technology</h4>\r\n<img src="images/stories/demo/other/cat-6.jpg" alt="Technology" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 4, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(94, 'Book reviews', '<div class="clearfix">\r\n<img src="images/stories/demo/other/book-review.jpg" alt="The Battle for America 2008" align="left" style="margin-right: 10px; padding: 5px; border: 5px solid #CCCCCC;" />\r\n\r\n<h4>The Battle for America 2008: The Story of an Extraordinary Election</h4>\r\n<p>Fringilla justo vel pretium at Vestibulum dui tellus wisi congue auctor. Augue Nullam amet Phasellus Ut eu sociis libero id libero est. In interdum ac nibh habitasse malesuada nibh justo at orci Maecenas. Massa sociis quis et odio massa id id Pellentesque vestibulum interdum.</p>\r\n\r\n<a href="#" title="Comment" class="comment">Discuss: Do you think about this book?</a>\r\n</div>', 0, 'col-mass2', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=_hilite badge-top\n\n', 0, 0, ''),
(93, 'Discussion', '<div class="discussion-wrap clearfix"><div class="discussion-content">\r\n<img src="images/stories/demo/other/discussion.jpg" alt="" align="left" />\r\n<h4><a href="#" title="Discussion">Iran appoints new judiciary head</a></h4>\r\nAc Nunc tempor a eget et morbi Vivamus vitae nunc metus. Morbi feugiat neque tellus justo commodo hac lacinia gravida Curabitur Curabitur. Risus ut Phasellus eget Vivamus dis justo cursus semper <a class="comment" title="Comment" href="#">Comment</a>&nbsp;&nbsp;<a class="video" title="Video" href="#">Video</a>&nbsp;&nbsp;<a class="photo" title="Slideshow" href="#">Slideshow</a>\r\n\r\n</div></div>\r\n\r\n<span class="author"><strong>Author:</strong> Mr Truly</span>', 0, 'col-mass2', 62, '2009-09-08 12:28:29', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(99, 'Banner 1', '<p><a href="index.php?option=com_content&amp;view=article&amp;id=389:10-things-you-must-know-about-teline-iii" title="What''s new in JA Teline iii?"><img src="images/stories/demo/other/what-new.jpg" border="0" alt="What''s new in teline iii" /></a></p>', 4, 'col-mass1', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(127, 'JA MegaMenu', '', 0, 'megamenu', 0, '0000-00-00 00:00:00', 1, 'mod_jamegamenu', 0, 0, 1, 'cache=0\nclass_sfx=\nmoduleclass_sfx=\nposition=mega-item\nsubwidth=\n\n', 0, 0, ''),
(110, 'Health highlight', '<div class="ja-innerdiv">\r\n<h4>You &amp; Your Family</h4>\r\n<img src="images/stories/demo/other/hel-1.jpg" alt="You &amp; Your Family" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus Sed congue. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>What Ails You</h4>\r\n<img src="images/stories/demo/other/hel-2.jpg" alt="What Ails You" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Diet &amp; Fitness</h4>\r\n<img src="images/stories/demo/other/hel-3.jpg" alt="Diet &amp; Fitness" />\r\nNon ante gravida eros quis justo sed nonummy et Donec et.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 10, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(75, 'Sport', '<img src="images/stories/demo/sport/sport-1.jpg" alt="Sport" />\r\n\r\nNulla tincidunt tincidunt <a href="#" title="Sport">velit tristique leo.</a>', 0, 'user9', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(76, 'Fashion', '<img src="images/stories/demo/style/fashion-1.jpg" alt="Fashion" />\r\n\r\nPellentesque porttitor Vestibulum Mauris', 0, 'user8', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(77, 'Art and Design', '<ul>\r\n<li><a target="_blank" href="#">Praesent ac libero ornare venenatis</a></li>\r\n\r\n<li><a target="_blank" href="#">Nunc auctor vel Aenean neque montes</a></li>\r\n\r\n<li><a target="_blank" href="#">Et id tortor Cras id nunc montes lobortis lacus et fringilla</a></li>\r\n\r\n<li><a target="_blank" href="#">Hendrerit semper eget convallis neque eleifend orci iaculis</a></li>\r\n\r\n<li><a target="_blank" href="#">Tortor et laoreet quis Nullam augue justo</a></li>\r\n</ul>', 0, 'user7', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(78, 'MarketWatch', '<ul>\r\n<li><a target="_blank" href="#">Currencies: U.S. dollar, yen reverse gains in Asia</a></li>\r\n\r\n<li><a target="_blank" href="#">Amcor buys most of Rio Tinto''s Alcan Packaging</a></li>\r\n\r\n<li><a target="_blank" href="#">Mark Hulbert: Gold, stocks are more allies than enemies</a></li>\r\n\r\n<li><a target="_blank" href="#">Stocks to Watch: Stocks in focus for Tuesday</a></li>\r\n\r\n<li><a target="_blank" href="#">Steelmakers rise in volatile Shanghai market</a></li>\r\n</ul>', 0, 'user6', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(86, 'JA Tabs', '', 0, 'col-mass1', 0, '0000-00-00 00:00:00', 1, 'mod_jatabs', 0, 0, 0, 'moduleclass_sfx=_tabs\ntype=modules\nmodules-position=ja-tabs\nmodule-modulename=mod_search, mod_login\narticlesIDs-ids=1, 2, 3, 4\ncategoryID-catid=10\ncontent-content=[tab title=''Title tab 1'']This is tab content 1[/tab]  [tab title=''Title tab 2'']This is tab content 2[/tab]  [tab title=''Title tab 3'']This is tab content 3[/tab]  [tab title=''Title tab 4'']This is tab content 4[/tab]\nstyle=teline_iii\nHeight=auto\nWidth=auto\nposition=top\ntHeight=auto\ntWidth=auto\nanimType=animNone\nmouseType=click\najax=0\nview=introtext\nduration=1000\ncolors=\n\n', 0, 0, ''),
(92, 'Adv', '<img src="images/stories/demo/other/bmw-x5.jpg" alt="BMW-X5" />', 4, 'col-mass2', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(90, 'Latest News', '', 2, 'ja-tabs', 0, '0000-00-00 00:00:00', 1, 'mod_jabulletin', 0, 0, 1, 'type=latest\nmoduleclass_sfx=\nshow_front=1\ncount=5\ncatid=\nsecid=\nshow_image=1\nwidth=25\nheight=25\nshow_date=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(91, 'Most Read', '', 1, 'ja-tabs', 0, '0000-00-00 00:00:00', 1, 'mod_jabulletin', 0, 0, 1, 'type=mostread\nmoduleclass_sfx=\nshow_front=1\ncount=5\ncatid=\nsecid=\nshow_image=1\nwidth=25\nheight=25\nshow_date=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(123, 'Twitter', '', 4, 'mega-item', 0, '0000-00-00 00:00:00', 1, 'mod_jatwitter', 0, 0, 0, 'moduleclass_sfx=\ntaccount=joomlart\napikey=wMOXZUmWegMztLtrPB9I1Q\ntwitter_method=user_timeline\nshow_username=0\nshow_icon=0\nicon_size=30\nshow_limit=2\ntweets_cachetime=300\nshow_source=0\nshowtextheading=0\nheadingtext=My Twitter Updates\nshowfollowlink=0\nuse_display_taccount=0\nsize_iconaccount=32\nuse_friends=0\nsize_iconfriend=24\nmax_friends=18\nenable_cache=1\ncache_time=900\nuse_legacy=0\ndisplayitem=1\n\n', 0, 0, ''),
(97, 'JA News2', '', 0, 'content-bot', 0, '0000-00-00 00:00:00', 1, 'mod_janews2', 0, 0, 0, 'moduleclass_sfx=\nhiddenClasses=\nordering=ordering\nsections=13:red\\n14:orange\\n8:deepblue\\n12:cyan\\n5:green\\n9:pink\\n11:lime\\n6\nshowcattitle=1\nintroitems=1\nlinkitems=3\nmaxchars=50\nshowcreater=0\nshowdate=0\nshowcontentfrontpage=1\ncols=3\nshowreadmore=0\nshowimage=1\nthumbnail_mode=crop\nthumbnail_mode-resize-use_ratio=1\nwidth=160\nheight=80\nalign=0\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(98, 'Banner right 1', '', 8, 'right', 62, '2009-09-15 10:00:29', 1, 'mod_banners', 0, 0, 0, 'target=1\ncount=1\ncid=1\ncatid=85\ntag_search=0\nordering=random\nheader_text=\nfooter_text=\nmoduleclass_sfx=\ncache=1\ncache_time=15\n\n', 0, 0, ''),
(108, 'Tech highlight', '<div class="ja-innerdiv">\r\n<h4>Business Tech</h4>\r\n<img src="images/stories/demo/other/tech-1.jpg" alt="Business Tech" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus Sed congue. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Science</h4>\r\n<img src="images/stories/demo/other/tech-2.jpg" alt="Science" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Personal Tech</h4>\r\n<img src="images/stories/demo/other/tech-3.jpg" alt="Personal Tech" />\r\nNon ante gravida eros quis justo sed nonummy et Donec et.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Space</h4>\r\n<img src="images/stories/demo/other/tech-4.jpg" alt="Space" />\r\nDolor ut id enim dolor auctor a Aenean Vestibulum lorem egestas.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Green Tech</h4>\r\n<img src="images/stories/demo/other/tech-5.jpg" alt="Green Tech" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 6, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(100, 'JA Tabs 2', '', 2, 'col-mass1', 0, '0000-00-00 00:00:00', 1, 'mod_jatabs', 0, 0, 0, 'moduleclass_sfx=_tabs\ntype=modules\nmodules-position=ja-tabs2\nmodule-modulename=mod_search, mod_login\narticlesIDs-ids=1, 2, 3, 4\ncategoryID-catid=10\ncontent-content=[tab title=''Title tab 1'']This is tab content 1[/tab]  [tab title=''Title tab 2'']This is tab content 2[/tab]  [tab title=''Title tab 3'']This is tab content 3[/tab]  [tab title=''Title tab 4'']This is tab content 4[/tab]\nstyle=teline_iii\nHeight=auto\nWidth=100%\nposition=top\ntHeight=auto\ntWidth=\nanimType=animNone\nmouseType=click\najax=0\nview=introtext\nduration=1000\ncolors=\n\n', 0, 0, ''),
(101, 'Videos of the day', '<div class="ja-video-thumb">\r\n{japopup type="youtube" content="http://www.youtube.com/v/cdphzxz64BY?hl=en&amp;fs=1" width="640" height="380" title="Political Roast : Obama Emanuel 2005"}<img src="http://img.youtube.com/vi/cdphzxz64BY/2.jpg" alt="Political Roast : Obama Emanuel 2005" />{/japopup}\r\n\r\n{japopup type="youtube" content="http://www.youtube.com/v/i18_4jZbrQ4?hl=en&fs=1" width="640" height="380" title="Beardyman and Reggie Watts on Channel 4 Headspace"}<img src="http://img.youtube.com/vi/i18_4jZbrQ4/2.jpg" alt="Beardyman and Reggie Watts on Channel 4 Headspace" />{/japopup}\r\n\r\n{japopup type="youtube" content="http://www.youtube.com/v/YgW7or1TuFk?hl=en&amp;fs=1" width="640" height="380" title="A Closer Look At The iPhone"}<img src="http://img.youtube.com/vi/YgW7or1TuFk/2.jpg" alt="A Closer Look At The iPhone" />{/japopup}\r\n\r\n{japopup type="youtube" content="http://www.youtube.com/v/DOvbv-LkK6w?hl=en&amp;fs=1" width="640" height="380" title="Star Wars: The Old Republic E3 2009 Jedi vs. Sith Trailer [HQ]" }<img src="http://img.youtube.com/vi/DOvbv-LkK6w/2.jpg" alt="Star Wars: The Old Republic E3 2009 Jedi vs. Sith Trailer [HQ]" />{/japopup}\r\n\r\n{japopup type="youtube" content="http://www.youtube.com/v/ezdfVbOqzRc?hl=en&amp;fs=1" width="640" height="380" title="Bare-Knuckle Brawling : KICK ASS ! | 2009"}<img src="http://img.youtube.com/vi/ezdfVbOqzRc/2.jpg" alt="Bare-Knuckle Brawling : KICK ASS ! | 2009" />{/japopup}\r\n\r\n{japopup type="youtube" content="http://www.youtube.com/v/XOZw7sBUI7s?hl=en&amp;fs=1" width="640" height="380" title="Bernard the bear:The desert island"}<img src="http://img.youtube.com/vi/XOZw7sBUI7s/2.jpg" alt="Bernard the bear:The desert island" />{/japopup}\r\n</div>', 0, 'ja-tabs2', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(102, 'JA Tabs 3', '', 3, 'col-mass1', 0, '0000-00-00 00:00:00', 1, 'mod_jatabs', 0, 0, 0, 'moduleclass_sfx=_tabs\ntype=modules\nmodules-position=ja-tabs3\nmodule-modulename=mod_search, mod_login\narticlesIDs-ids=1, 2, 3, 4\ncategoryID-catid=10\ncontent-content=[tab title=''Title tab 1'']This is tab content 1[/tab]  [tab title=''Title tab 2'']This is tab content 2[/tab]  [tab title=''Title tab 3'']This is tab content 3[/tab]  [tab title=''Title tab 4'']This is tab content 4[/tab]\nstyle=teline_iii\nHeight=auto\nWidth=100%\nposition=top\ntHeight=auto\ntWidth=\nanimType=animNone\nmouseType=click\najax=0\nview=introtext\nduration=1000\ncolors=\n\n', 0, 0, ''),
(109, 'Entertainment highlight', '<div class="ja-innerdiv">\r\n<h4>Television</h4>\r\n<img src="images/stories/demo/other/enter-1.jpg" alt="Television" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus Sed congue. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Movies</h4>\r\n<img src="images/stories/demo/other/enter-2.jpg" alt="Movies" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Books</h4>\r\n<img src="images/stories/demo/other/enter-3.jpg" alt="Books" />\r\nNon ante gravida eros quis justo sed nonummy et Donec et.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Music</h4>\r\n<img src="images/stories/demo/other/enter-4.jpg" alt="Music" />\r\nDolor ut id enim dolor auctor a Aenean Vestibulum lorem egestas.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Games</h4>\r\n<img src="images/stories/demo/other/enter-5.jpg" alt="Movies" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Theatre</h4>\r\n<img src="images/stories/demo/other/enter-6.jpg" alt="Television" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus Sed congue. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 7, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(105, 'JA News Frontpage', '', 0, 'content-top', 62, '2011-02-05 09:23:39', 1, 'mod_janews_fp', 0, 0, 0, 'moduleclass_sfx=\nhiddenClasses=\nnumberofheadlinenews=9\nbigitems=1\nbigmaxchars=200\nbigshowimage=1\nbigimg_w=260\nbigimg_h=220\nsmallmaxchars=100\nsmallshowimage=1\nsmallimg_w=60\nsmallimg_h=60\nfp_layout=telineiii_fp.php\nmore_article=3\nheadlinelang=TOP STORIES\nshowhltools=1\nshowhltitle=1\nshowhlreadmore=1\nheadlineheight=0\nautoroll=1\ndelaytime=5\n\n', 0, 0, ''),
(107, 'Tech banner', '<img src="images/stories/demo/other/imgad-2.jpg" alt="iPhone 3GS" />', 10, 'right', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(111, 'World highlight', '<div class="ja-innerdiv">\r\n<h4>Africa</h4>\r\n<img src="images/stories/demo/other/wor-1.jpg" alt="Africa" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus Sed congue. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Middle East</h4>\r\n<img src="images/stories/demo/other/wor-2.jpg" alt="Middle East" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>U.S Politics</h4>\r\n<img src="images/stories/demo/other/wor-3.jpg" alt="U.S Politics" />\r\nNon ante gravida eros quis justo sed nonummy et Donec et.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Europe</h4>\r\n<img src="images/stories/demo/other/wor-4.jpg" alt="Europe" />\r\nDolor ut id enim dolor auctor a Aenean Vestibulum lorem egestas.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 5, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(112, 'Life and Style', '<div class="ja-innerdiv">\r\n<h4>Fashion &amp; Beauty</h4>\r\n<img src="images/stories/demo/other/life-1.jpg" alt="Fashion &amp; Beauty" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus Sed congue. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Food &amp; Drink</h4>\r\n<img src="images/stories/demo/other/life-2.jpg" alt="Food &amp; Drink" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Wife &amp; Husband</h4>\r\n<img src="images/stories/demo/other/life-3.jpg" alt="Wife &amp; Husband" />\r\nNon ante gravida eros quis justo sed nonummy et Donec et.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Shop</h4>\r\n<img src="images/stories/demo/other/life-4.jpg" alt="Shop" />\r\nDolor ut id enim dolor auctor a Aenean Vestibulum lorem egestas.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Mother &amp; Children</h4>\r\n<img src="images/stories/demo/other/life-5.jpg" alt="Mother &amp; Children" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Home &amp; Gardens</h4>\r\n<img src="images/stories/demo/other/life-6.jpg" alt="Home &amp; Gardens" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus Sed congue. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 9, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(113, 'Photo gallery', '<div class="ja-video-thumb">\r\n\r\n{japopup type="image" content="images/stories/demo/gallery/gl-1.png" width="700" height="380" title="Sample image" }<img src="images/stories/demo/gallery/gl-1.png"  width="100" alt="Sample image" />{/japopup}\r\n\r\n{japopup type="image" content="images/stories/demo/gallery/gl-2.png" width="700" height="380" title="Sample image" }<img src="images/stories/demo/gallery/gl-2.png" width="100" alt="Sample image" />{/japopup}\r\n\r\n{japopup type="image" content="images/stories/demo/gallery/gl-3.png" width="700" height="380" title="Sample image"}<img src="images/stories/demo/gallery/gl-3.png" width="100" alt="Sample image" />{/japopup}\r\n\r\n{japopup type="image" content="images/stories/demo/gallery/gl-4.jpg" width="700" height="380" title="Sample image" }<img src="images/stories/demo/gallery/gl-4.jpg"  width="100" alt="Sample image" />{/japopup}\r\n\r\n{japopup type="image" content="images/stories/demo/gallery/gl-5.jpg" width="700" height="380" title="Sample image"}<img src="images/stories/demo/gallery/gl-5.jpg"  width="100" alt="Sample image" />{/japopup}\r\n\r\n{japopup type="image" content="images/stories/demo/gallery/gl-6.jpg" width="700" height="380" title="Sample image"}<img src="images/stories/demo/gallery/gl-6.jpg"  width="100" alt="Sample image" />{/japopup}\r\n\r\n</div>', 0, 'ja-tabs2', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(114, 'Travel highlight', '<div class="ja-innerdiv">\r\n<h4>Getaway</h4>\r\n<img src="images/stories/demo/other/travel-1.jpg" alt="Getaway" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus Sed congue. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Travel Tips</h4>\r\n<img src="images/stories/demo/other/travel-2.jpg" alt="Travel Tips" />\r\nOrci rutrum lacus feugiat egestas venenatis Sed dolor condimentum montes ornare.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Business Travel</h4>\r\n<img src="images/stories/demo/other/travel-3.jpg" alt="Business Travel" />\r\nNon ante gravida eros quis justo sed nonummy et Donec et.<a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 8, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(115, 'Bookmark pages', '<ul class="bookmark-links">\r\n<li>{japopup type="iframe" content="http://www.facebook.com" title="www.facebook.com" width="1024" height="600"} Facebook {/japopup}</li>\r\n<li>{japopup type="iframe" content="http://www.joomlart.com" width="1024" height="600"}Joomla Templates Club - Professional - High quality Joomla template{/japopup}</li>\r\n<li>{japopup type="iframe" content="http://www.magentocommerce.com" width="1024" height="600"}Magento - Home  - eCommerce Software for Growth{/japopup}</li>\r\n<li>{japopup type="iframe" content="http://www.joomlancers.com" width="1024" height="600"}JoomLancers - Freelance Marketplace for Opensource Development{/japopup}</li>\r\n<li>{japopup type="iframe" content="http://www.iconfinder.net/" width="1024" height="600"}<strong>Icon Finder</strong><br />Tool icons | Free icons | Search through more than 100,000 icons{/japopup}</li>\r\n</ul>', 0, 'ja-tabs2', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(116, 'Artist highlight', '<div class="ja-innerdiv">\r\n<h4>Photography</h4>\r\n<img src="images/stories/demo/other/artist-1.jpg" alt="Photography" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Design</h4>\r\n<img src="images/stories/demo/other/artist-2.jpg" alt="Design" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Architecture</h4>\r\n<img src="images/stories/demo/other/artist-3.jpg" alt="Architecture" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Art</h4>\r\n<img src="images/stories/demo/other/artist-4.jpg" alt="Art" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>3D Graphics</h4>\r\n<img src="images/stories/demo/other/artist-5.jpg" alt="3D Graphics" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Graphics</h4>\r\n<img src="images/stories/demo/other/artist-6.jpg" alt="Graphics" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 2, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(117, 'Sport highlight', '<div class="ja-innerdiv">\r\n<h4>Tennis</h4>\r\n<img src="images/stories/demo/other/sport-1.jpg" alt="Tennis" />\r\nUt justo id nibh pulvinar mauris pede Phasellus metus. <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Cricket</h4>\r\n<img src="images/stories/demo/other/sport-2.jpg" alt="Cricket" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Motosport</h4>\r\n<img src="images/stories/demo/other/sport-3.jpg" alt="Motosport" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Horse racing</h4>\r\n<img src="images/stories/demo/other/sport-4.jpg" alt="Horse racing" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Boxing</h4>\r\n<img src="images/stories/demo/other/sport-5.jpg" alt="Boxing" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>\r\n\r\n<div class="ja-innerdiv">\r\n<h4>Football</h4>\r\n<img src="images/stories/demo/other/sport-6.jpg" alt="Football" />\r\nIpsum vel Phasellus ligula dis convallis eu risus consequat justo justo <a href="#" class="Read more..." style="font-size: 92%;">More detail &raquo;</a>\r\n</div>', 3, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(122, 'JA News Sticker Module', '', 0, 'headlines', 62, '2009-09-11 09:02:29', 1, 'mod_janewsticker', 0, 0, 1, 'moduleclass_sfx=\nusing_mode=categories_selected\ncategory=1|3|76|28|27|31|32|29|25|30|58|59|60|62|63|92|67|68|69|70|95|96|37|38|39|42|43|44|45|93|94|81|82|83|84|88|89|54|55|56|71|72|73|74|90|91|77|78|79|80|86|87|0\nmax_items=5\nsort_order_field=created\nsort_order=DESC\ntitle_max_chars=1600\nanimation_type=opacity\nanimation_speed=400\nanimation_interval=2500\nanimation_transition=Fx.Transitions.linear\nenable_cache=0\ncache_time=900\n\n', 0, 0, ''),
(131, 'Features', '<div>\r\n<p style="text-align: center"><img src="images/stories/demo/other/icon_feature.png" border="0" alt="Feature" title="Feature" /> <span style="color: #cc0000; font-size: 150%">10</span> Things you <span style="color: #cc0000">MUST</span> know about Teline III</p>\r\n<h4 style="text-align: center; padding-top: 5px;"><a href="index.php?option=com_content&amp;view=article&amp;id=389&amp;Itemid=467">Find out more... </a></h4>\r\n</div>', 0, 'mega-item', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(133, 'Follow us', '<img src="images/stories/demo/other/follow.jpg" alt="Follow us" />', 0, 'user10', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(134, 'topmega', '', 1, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 1, 'menutype=topmega', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_modules_menu`
--

DROP TABLE IF EXISTS `ja_tel_modules_menu`;
CREATE TABLE IF NOT EXISTS `ja_tel_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_modules_menu`
--

INSERT INTO `ja_tel_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(86, 472),
(97, 0),
(99, 472),
(105, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_newsfeeds`
--

DROP TABLE IF EXISTS `ja_tel_newsfeeds`;
CREATE TABLE IF NOT EXISTS `ja_tel_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text NOT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(11) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(11) unsigned NOT NULL DEFAULT '3600',
  `checked_out` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ja_tel_newsfeeds`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_plugins`
--

DROP TABLE IF EXISTS `ja_tel_plugins`;
CREATE TABLE IF NOT EXISTS `ja_tel_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `element` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) NOT NULL DEFAULT '',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder` (`published`,`client_id`,`access`,`folder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `ja_tel_plugins`
--

INSERT INTO `ja_tel_plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Authentication - Joomla', 'joomla', 'authentication', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(2, 'Authentication - LDAP', 'ldap', 'authentication', 0, 2, 0, 1, 0, 0, '0000-00-00 00:00:00', 'host=\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),
(3, 'Authentication - GMail', 'gmail', 'authentication', 0, 4, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 'Authentication - OpenID', 'openid', 'authentication', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(5, 'User - Joomla!', 'joomla', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'autoregister=1\n\n'),
(6, 'Search - Content', 'content', 'search', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch_archived=1\n\n'),
(7, 'Search - Contacts', 'contacts', 'search', 0, 3, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(8, 'Search - Categories', 'categories', 'search', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(9, 'Search - Sections', 'sections', 'search', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(10, 'Search - Newsfeeds', 'newsfeeds', 'search', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(11, 'Search - Weblinks', 'weblinks', 'search', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(12, 'Content - Pagebreak', 'pagebreak', 'content', 0, 10000, 1, 1, 0, 0, '0000-00-00 00:00:00', 'enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),
(13, 'Content - Rating', 'vote', 'content', 0, 6, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(14, 'Content - Email Cloaking', 'emailcloak', 'content', 0, 7, 1, 0, 0, 0, '0000-00-00 00:00:00', 'mode=1\n\n'),
(15, 'Content - Code Hightlighter (GeSHi)', 'geshi', 'content', 0, 8, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(16, 'Content - Load Module', 'loadmodule', 'content', 0, 9, 1, 0, 0, 0, '0000-00-00 00:00:00', 'enabled=1\nstyle=0\n\n'),
(17, 'Content - Page Navigation', 'pagenavigation', 'content', 0, 4, 1, 1, 0, 0, '0000-00-00 00:00:00', 'position=1\n\n'),
(18, 'Editor - No Editor', 'none', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(19, 'Editor - TinyMCE 2.0', 'tinymce', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 'theme=advanced\ncleanup=1\ncleanup_startup=0\nautosave=0\ncompressed=0\nrelative_urls=1\ntext_direction=ltr\nlang_mode=0\nlang_code=en\ninvalid_elements=applet\ncontent_css=1\ncontent_css_custom=\nnewlines=0\ntoolbar=top\nhr=1\nsmilies=1\ntable=1\nstyle=1\nlayer=1\nxhtmlxtras=0\ntemplate=0\ndirectionality=1\nfullscreen=1\nhtml_height=550\nhtml_width=750\npreview=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:%M:%S\n\n'),
(20, 'Editor - XStandard Lite 2.0', 'xstandard', 'editors', 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(21, 'Editor Button - Image', 'image', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(22, 'Editor Button - Pagebreak', 'pagebreak', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(23, 'Editor Button - Readmore', 'readmore', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(24, 'XML-RPC - Joomla', 'joomla', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(25, 'XML-RPC - Blogger API', 'blogger', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', 'catid=1\nsectionid=0\n\n'),
(27, 'System - SEF', 'sef', 'system', 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(28, 'System - Debug', 'debug', 'system', 0, 2, 1, 0, 0, 0, '0000-00-00 00:00:00', 'queries=1\nmemory=1\nlangauge=1\n\n'),
(29, 'System - Legacy', 'legacy', 'system', 0, 3, 0, 1, 0, 0, '0000-00-00 00:00:00', 'route=0\n\n'),
(30, 'System - Cache', 'cache', 'system', 0, 4, 0, 1, 0, 0, '0000-00-00 00:00:00', 'browsercache=0\ncachetime=15\n\n'),
(31, 'System - Log', 'log', 'system', 0, 5, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(32, 'System - Remember Me', 'remember', 'system', 0, 6, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(33, 'System - Backlink', 'backlink', 'system', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(38, 'JAZin plugin', 'jazin', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'jazin_update=0\njazin_menutype=magazine\njazin_home=1\nshow_description=0\nshow_description_image=0\nshow_page_title=1\ns_num_leading_articles=1\ns_num_intro_articles=8\ns_num_columns=2\ns_num_links=0\nc_num_leading_articles=1\nc_num_intro_articles=4\nc_num_columns=2\nc_num_links=0\norderby_pri=\norderby_sec=\nshow_pagination=2\nshow_pagination_results=1\nshow_noauth=\nshow_title=\nlink_titles=1\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=0\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\n\n'),
(39, 'JA Tabs for Joomla! 1.5', 'ja_tabs', 'content', 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', 'style=default\n'),
(58, 'JA User Setting for joomla 1.5.x', 'plg_jausersetting', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'load_css_file=1\nlable_setting=Setting\n\n'),
(44, 'JA Widget Base', 'jawidget', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(50, 'JA Popup Plugin', 'plg_japopup', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(48, 'Thumbnail for Joomla content', 'plg_jathumbnail', 'content', 0, 2, 1, 0, 0, 0, '0000-00-00 00:00:00', 'content_mode=auto\ncontent_mode-auto-content_position=0\ncontent_mode-auto-manual-content_width=200\ncontent_mode-auto-manual-content_height=200\ncontent_mode-auto-manual-content_align=right\nblog_mode=1\nblog_mode-1-blog_frontpage=0\nblog_mode-1-blog_leading_width=200\nblog_mode-1-blog_leading_height=200\nblog_mode-1-blog_intro_width=100\nblog_mode-1-blog_intro_height=100\n\n'),
(57, 'JA Bookmark Plugin', 'plg_jabookmark', 'content', 0, 3, 1, 0, 0, 62, '2009-09-11 10:55:37', 'system=addthis\nclass_sfx=\nmode=auto\ncategory=\nlocation=onPrepareContent-Bottom\nused_custom_button=0\ncustom_button=\nsystem-addthis-brand_name=joomlart.com\nsystem-addthis-account=joomlart\nsystem-addthis-language_code=en\nsystem-addthis-custom_apps=\nsystem-addthis-button=lg-bookmark\nsystem-addtoany-language=en-US\nsystem-addtoany-button=share_save_106_16.gif\nsystem-addtoany-showpagetitle=1\nsystem-addtoany-orientation=automatic\nwidget_text=\nsystem-addthis-header_color=000000\nsystem-addthis-header_background=999999\nsystem-addthis-vertical_offset=\nsystem-addthis-horizontal_offset=\nsystem-addtoany-maincolor=D7E5ED\nsystem-addtoany-border=414c53\nsystem-addtoany-linktext=333333\nsystem-addtoany-linktext_hover=333333\nsystem-addtoany-background=FFFFFF\n\n'),
(60, 'System - Mootools Upgrade', 'mtupgrade', 'system', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(61, 'JA T3 Framework', 'jat3', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(62, 'Content - JA Disqus Debate Echo Plugin', 'plg_disqus_debate_echo', 'content', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'source=both\ndisplay_on_list=1\ndisplay_on_home=1\nmode=automatic\nmode-automatic-catsid=76|1|3|55|54|56|74|90|71|73|91|72|84|81|83|89|88|82|78|86|77|80|87|79\nmode-automatic-k2catsid=\nprovider=disqus\nprovider-intensdebate-account=\nprovider-disqus-subdomain=joomlart\nprovider-disqus-devmode=0\nprovider-jskit-domain=\ndisplay_comment_count=1\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_poll_data`
--

DROP TABLE IF EXISTS `ja_tel_poll_data`;
CREATE TABLE IF NOT EXISTS `ja_tel_poll_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollid` int(11) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pollid` (`pollid`,`text`(1))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `ja_tel_poll_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_poll_date`
--

DROP TABLE IF EXISTS `ja_tel_poll_date`;
CREATE TABLE IF NOT EXISTS `ja_tel_poll_date` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vote_id` int(11) NOT NULL DEFAULT '0',
  `poll_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ja_tel_poll_date`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_poll_menu`
--

DROP TABLE IF EXISTS `ja_tel_poll_menu`;
CREATE TABLE IF NOT EXISTS `ja_tel_poll_menu` (
  `pollid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pollid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_poll_menu`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_polls`
--

DROP TABLE IF EXISTS `ja_tel_polls`;
CREATE TABLE IF NOT EXISTS `ja_tel_polls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `voters` int(9) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `lag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ja_tel_polls`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_sections`
--

DROP TABLE IF EXISTS `ja_tel_sections`;
CREATE TABLE IF NOT EXISTS `ja_tel_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` text NOT NULL,
  `scope` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_scope` (`scope`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `ja_tel_sections`
--

INSERT INTO `ja_tel_sections` (`id`, `title`, `name`, `alias`, `image`, `scope`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `ordering`, `access`, `count`, `params`) VALUES
(15, '政策类', '', '2011-02-05-09-06-06', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 1, 0, 2, ''),
(16, '青春期', '', '2011-02-05-09-06-14', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 2, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_session`
--

DROP TABLE IF EXISTS `ja_tel_session`;
CREATE TABLE IF NOT EXISTS `ja_tel_session` (
  `username` varchar(150) DEFAULT '',
  `time` varchar(14) DEFAULT '',
  `session_id` varchar(200) NOT NULL DEFAULT '0',
  `guest` tinyint(4) DEFAULT '1',
  `userid` int(11) DEFAULT '0',
  `usertype` varchar(50) DEFAULT '',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext,
  PRIMARY KEY (`session_id`(64)),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_session`
--

INSERT INTO `ja_tel_session` (`username`, `time`, `session_id`, `guest`, `userid`, `usertype`, `gid`, `client_id`, `data`) VALUES
('', '1296898424', '77gd998kultsvtkbp67jm90tq4', 1, 0, '', 0, 0, '__default|a:7:{s:15:"session.counter";i:61;s:19:"session.timer.start";i:1296896482;s:18:"session.timer.last";i:1296898422;s:17:"session.timer.now";i:1296898424;s:22:"session.client.browser";s:119:"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.237 Safari/534.10";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:77:"C:\\project\\PHPnow-1.5.4\\htdocs\\ja_tel\\libraries\\joomla\\html\\parameter\\element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}}'),
('admin', '1296897819', '2c6e2a4175b1c272e2414beb64da626b', 0, 62, 'Super Administrator', 25, 1, '__default|a:8:{s:15:"session.counter";i:57;s:19:"session.timer.start";i:1296896664;s:18:"session.timer.last";i:1296897802;s:17:"session.timer.now";i:1296897819;s:22:"session.client.browser";s:119:"Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.552.237 Safari/534.10";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:6:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":7:{s:23:"com_modulesfilter_order";s:10:"m.position";s:27:"com_modulesfilter_order_Dir";s:0:"";s:23:"com_modulesfilter_state";s:0:"";s:26:"com_modulesfilter_position";s:1:"0";s:22:"com_modulesfilter_type";s:1:"0";s:26:"com_modulesfilter_assigned";s:1:"0";s:17:"com_modulessearch";s:0:"";}}s:11:"application";a:1:{s:4:"data";O:8:"stdClass":1:{s:4:"lang";s:0:"";}}s:10:"com_cpanel";a:1:{s:4:"data";O:8:"stdClass":1:{s:9:"mtupgrade";O:8:"stdClass":1:{s:7:"checked";b:1;}}}s:6:"global";a:1:{s:4:"data";O:8:"stdClass":1:{s:4:"list";O:8:"stdClass":1:{s:5:"limit";s:2:"20";}}}s:11:"com_modules";a:1:{s:4:"data";O:8:"stdClass":1:{s:10:"limitstart";i:0;}}s:9:"com_menus";a:1:{s:4:"data";O:8:"stdClass":1:{s:8:"menutype";s:8:"mainmenu";}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";s:2:"62";s:4:"name";s:13:"Administrator";s:8:"username";s:5:"admin";s:5:"email";s:23:"racke1983cn@hotmail.com";s:8:"password";s:65:"61e1b887826d68c2cb976d5593d484f7:NiwXApuXuVIQdh3uj1uKO8zoqBDJ4LCZ";s:14:"password_clear";s:0:"";s:8:"usertype";s:19:"Super Administrator";s:5:"block";s:1:"0";s:9:"sendEmail";s:1:"1";s:3:"gid";s:2:"25";s:12:"registerDate";s:19:"2011-02-05 09:01:01";s:13:"lastvisitDate";s:19:"0000-00-00 00:00:00";s:10:"activation";s:0:"";s:6:"params";s:0:"";s:3:"aid";i:2;s:5:"guest";i:0;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:77:"C:\\project\\PHPnow-1.5.4\\htdocs\\ja_tel\\libraries\\joomla\\html\\parameter\\element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:13:"session.token";s:32:"e2292384636c98b8c2bca6c235ac6e42";}');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_stats_agents`
--

DROP TABLE IF EXISTS `ja_tel_stats_agents`;
CREATE TABLE IF NOT EXISTS `ja_tel_stats_agents` (
  `agent` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_stats_agents`
--


-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_templates_menu`
--

DROP TABLE IF EXISTS `ja_tel_templates_menu`;
CREATE TABLE IF NOT EXISTS `ja_tel_templates_menu` (
  `template` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menuid`,`client_id`,`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ja_tel_templates_menu`
--

INSERT INTO `ja_tel_templates_menu` (`template`, `menuid`, `client_id`) VALUES
('ja_teline_iii_v2', 0, 0),
('khepri', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_users`
--

DROP TABLE IF EXISTS `ja_tel_users`;
CREATE TABLE IF NOT EXISTS `ja_tel_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `ja_tel_users`
--

INSERT INTO `ja_tel_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `gid`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES
(62, 'Administrator', 'admin', 'racke1983cn@hotmail.com', '61e1b887826d68c2cb976d5593d484f7:NiwXApuXuVIQdh3uj1uKO8zoqBDJ4LCZ', 'Super Administrator', 0, 1, 25, '2011-02-05 09:01:01', '2011-02-05 09:04:28', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `ja_tel_weblinks`
--

DROP TABLE IF EXISTS `ja_tel_weblinks`;
CREATE TABLE IF NOT EXISTS `ja_tel_weblinks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`,`archived`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ja_tel_weblinks`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_advancedmodules`
--

DROP TABLE IF EXISTS `jos_advancedmodules`;
CREATE TABLE IF NOT EXISTS `jos_advancedmodules` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`moduleid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_advancedmodules`
--

INSERT INTO `jos_advancedmodules` (`moduleid`, `params`) VALUES
(44, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=1\nassignto_menuitems_selection=1|2\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(1, 'assignto_menuitems=0\nassignto_menuitems_selection=0'),
(42, 'assignto_menuitems=0\nassignto_menuitems_selection=0'),
(43, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=1\nassignto_menuitems_selection=1|2\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(40, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_selection=1|2\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(39, 'assignto_menuitems=1\nassignto_menuitems_selection=1|2'),
(17, 'assignto_menuitems=1\nassignto_menuitems_selection=1|2'),
(51, 'assignto_menuitems=0\nassignto_menuitems_selection=0'),
(52, 'assignto_menuitems=0\nassignto_menuitems_selection=0'),
(53, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(54, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(55, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(56, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(57, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(58, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmirror_moduleid=19\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(59, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n'),
(60, 'hideempty=0\ntooltip=\ncolor=FFFFFF\nmirror_module=0\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs|inc_cats|inc_arts|x\nassignto_articles=0\nassignto_articles_selection=\nassignto_articles_keywords=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats|inc_items|x\nassignto_k2tags=0\nassignto_k2tags_selection=\nassignto_k2tags_inc=inc_tags|inc_items|x\nassignto_k2items=0\nassignto_k2items_selection=\nassignto_components=0\nassignto_components_selection=x\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=0000-00-00 00:00\nassignto_date_publish_down=0000-00-00 00:00\nassignto_seasons=0\nassignto_seasons_selection=x\nassignto_seasons_hemisphere=northern\nassignto_months=0\nassignto_months_selection=x\nassignto_days=0\nassignto_days_selection=x\nassignto_time=0\nassignto_time_publish_up=0:00\nassignto_time_publish_down=0:00\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\nassignto_php=0\nassignto_php_selection=\n');

-- --------------------------------------------------------

--
-- Table structure for table `jos_banner`
--

DROP TABLE IF EXISTS `jos_banner`;
CREATE TABLE IF NOT EXISTS `jos_banner` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) NOT NULL DEFAULT 'banner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(100) NOT NULL DEFAULT '',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `date` datetime DEFAULT NULL,
  `showBanner` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `viewbanner` (`showBanner`),
  KEY `idx_banner_catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_banner`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_bannerclient`
--

DROP TABLE IF EXISTS `jos_bannerclient`;
CREATE TABLE IF NOT EXISTS `jos_bannerclient` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` time DEFAULT NULL,
  `editor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_bannerclient`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_bannertrack`
--

DROP TABLE IF EXISTS `jos_bannertrack`;
CREATE TABLE IF NOT EXISTS `jos_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_bannertrack`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_categories`
--

DROP TABLE IF EXISTS `jos_categories`;
CREATE TABLE IF NOT EXISTS `jos_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jos_categories`
--

INSERT INTO `jos_categories` (`id`, `parent_id`, `title`, `name`, `alias`, `image`, `section`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `editor`, `ordering`, `access`, `count`, `params`) VALUES
(1, 0, '视频类', '', '2011-02-04-10-00-30', '', '1', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(2, 0, '漫画类', '', '2011-02-05-09-35-49', '', '1', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(3, 0, '地区政策', '', '2011-05-01-14-02-37', '', '3', 'left', '', 1, 62, '2011-05-15 05:46:42', NULL, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_components`
--

DROP TABLE IF EXISTS `jos_components`;
CREATE TABLE IF NOT EXISTS `jos_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_menu_link` varchar(255) NOT NULL DEFAULT '',
  `admin_menu_alt` varchar(255) NOT NULL DEFAULT '',
  `option` varchar(50) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `admin_menu_img` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_option` (`parent`,`option`(32))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `jos_components`
--

INSERT INTO `jos_components` (`id`, `name`, `link`, `menuid`, `parent`, `admin_menu_link`, `admin_menu_alt`, `option`, `ordering`, `admin_menu_img`, `iscore`, `params`, `enabled`) VALUES
(1, 'Banners', '', 0, 0, '', 'Banner Management', 'com_banners', 0, 'js/ThemeOffice/component.png', 0, 'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n', 1),
(2, 'Banners', '', 0, 1, 'option=com_banners', 'Active Banners', 'com_banners', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(3, 'Clients', '', 0, 1, 'option=com_banners&c=client', 'Manage Clients', 'com_banners', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(4, 'Web Links', 'option=com_weblinks', 0, 0, '', 'Manage Weblinks', 'com_weblinks', 0, 'js/ThemeOffice/component.png', 0, 'show_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 1),
(5, 'Links', '', 0, 4, 'option=com_weblinks', 'View existing weblinks', 'com_weblinks', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(6, 'Categories', '', 0, 4, 'option=com_categories&section=com_weblinks', 'Manage weblink categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(7, 'Contacts', 'option=com_contact', 0, 0, '', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/component.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(8, 'Contacts', '', 0, 7, 'option=com_contact', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/edit.png', 1, '', 1),
(9, 'Categories', '', 0, 7, 'option=com_categories&section=com_contact_details', 'Manage contact categories', '', 2, 'js/ThemeOffice/categories.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(10, 'Polls', 'option=com_poll', 0, 0, 'option=com_poll', 'Manage Polls', 'com_poll', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(11, 'News Feeds', 'option=com_newsfeeds', 0, 0, '', 'News Feeds Management', 'com_newsfeeds', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(12, 'Feeds', '', 0, 11, 'option=com_newsfeeds', 'Manage News Feeds', 'com_newsfeeds', 1, 'js/ThemeOffice/edit.png', 0, 'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 1),
(13, 'Categories', '', 0, 11, 'option=com_categories&section=com_newsfeeds', 'Manage Categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(14, 'User', 'option=com_user', 0, 0, '', '', 'com_user', 0, '', 1, '', 1),
(15, 'Search', 'option=com_search', 0, 0, 'option=com_search', 'Search Statistics', 'com_search', 0, 'js/ThemeOffice/component.png', 1, 'enabled=0\n\n', 1),
(16, 'Categories', '', 0, 1, 'option=com_categories&section=com_banner', 'Categories', '', 3, '', 1, '', 1),
(17, 'Wrapper', 'option=com_wrapper', 0, 0, '', 'Wrapper', 'com_wrapper', 0, '', 1, '', 1),
(18, 'Mail To', '', 0, 0, '', '', 'com_mailto', 0, '', 1, '', 1),
(19, 'Media Manager', '', 0, 0, 'option=com_media', 'Media Manager', 'com_media', 0, '', 1, 'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\nimage_path=images/stories\nrestrict_uploads=1\ncheck_mime=1\nimage_extensions=bmp,gif,jpg,png\nignore_extensions=\nupload_mime=image/jpeg,image/gif,image/png,image/bmp,application/x-shockwave-flash,application/msword,application/excel,application/pdf,application/powerpoint,text/plain,application/x-zip\nupload_mime_illegal=text/html', 1),
(20, 'Articles', 'option=com_content', 0, 0, '', '', 'com_content', 0, '', 1, 'show_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=0\n\n', 1),
(21, 'Configuration Manager', '', 0, 0, '', 'Configuration', 'com_config', 0, '', 1, '', 1),
(22, 'Installation Manager', '', 0, 0, '', 'Installer', 'com_installer', 0, '', 1, '', 1),
(23, 'Language Manager', '', 0, 0, '', 'Languages', 'com_languages', 0, '', 1, 'administrator=zh-CN\nsite=zh-CN', 1),
(24, 'Mass mail', '', 0, 0, '', 'Mass Mail', 'com_massmail', 0, '', 1, 'mailSubjectPrefix=\nmailBodySuffix=\n\n', 1),
(25, 'Menu Editor', '', 0, 0, '', 'Menu Editor', 'com_menus', 0, '', 1, '', 1),
(27, 'Messaging', '', 0, 0, '', 'Messages', 'com_messages', 0, '', 1, '', 1),
(28, 'Modules Manager', '', 0, 0, '', 'Modules', 'com_modules', 0, '', 1, '', 1),
(29, 'Plugin Manager', '', 0, 0, '', 'Plugins', 'com_plugins', 0, '', 1, '', 1),
(30, 'Template Manager', '', 0, 0, '', 'Templates', 'com_templates', 0, '', 1, '', 1),
(31, 'User Manager', '', 0, 0, '', 'Users', 'com_users', 0, '', 1, 'allowUserRegistration=1\nnew_usertype=Registered\nuseractivation=1\nfrontend_userparams=1\n\n', 1),
(32, 'Cache Manager', '', 0, 0, '', 'Cache', 'com_cache', 0, '', 1, '', 1),
(33, 'Control Panel', '', 0, 0, '', 'Control Panel', 'com_cpanel', 0, '', 1, '', 1),
(34, 'Gavick PhotoSlide GK2', 'option=com_gk2_photoslide', 0, 0, 'option=com_gk2_photoslide', 'Gavick PhotoSlide GK2', 'com_gk2_photoslide', 0, 'components/com_gk2_photoslide/interface/images/com_logo_gk2.png', 0, '', 1),
(44, 'JA Extensions Manager', 'option=com_jaextmanager', 0, 0, 'option=com_jaextmanager', 'JA Extensions Manager', 'com_jaextmanager', 0, 'components/com_jaextmanager/assets/images/jauc.png', 0, '', 1),
(45, 'K2', 'option=com_k2', 0, 0, 'option=com_k2', 'K2', 'com_k2', 0, 'components/com_k2/images/system/k2-icon.png', 0, 'enable_css=1\nimagesQuality=100\nitemImageXS=100\nitemImageS=200\nitemImageM=400\nitemImageL=600\nitemImageXL=900\nitemImageGeneric=300\ncatImageWidth=100\ncatImageDefault=1\nuserImageWidth=100\nuserImageDefault=1\ncommenterImgWidth=48\nuserName=1\nuserImage=0\nuserDescription=0\nuserURL=0\nuserEmail=0\nuserFeed=0\nuserItemCount=10\nuserItemTitle=1\nuserItemTitleLinked=1\nuserItemDateCreated=1\nuserItemImage=1\nuserItemIntroText=1\nuserItemCategory=1\nuserItemTags=1\nuserItemCommentsAnchor=0\nuserItemReadMore=1\nuserItemK2Plugins=1\ngenericItemCount=10\ngenericItemTitle=1\ngenericItemTitleLinked=1\ngenericItemDateCreated=1\ngenericItemImage=1\ngenericItemIntroText=1\ngenericItemCategory=1\ngenericItemReadMore=1\ngenericItemExtraFields=0\ntagOrdering=\ncomments=0\ncommentsOrdering=DESC\ncommentsLimit=10\ncommentsFormPosition=below\ncommentsPublishing=1\ngravatar=0\nrecaptcha=0\nrecaptcha_public_key=\nrecaptcha_private_key=\nrecaptcha_theme=clean\ncommentsFormNotes=0\ncommentsFormNotesText=Make sure you enter the (*) required information where indicated.\\nBasic HTML code is allowed.\nsocialButtonCode=\ntwitterUsername=\ntinyURL=0\nfeedLimit=10\nfeedItemImage=0\nfeedImgSize=S\nfeedItemIntroText=0\nfeedTextWordLimit=\nfeedItemFullText=1\nfeedItemTags=0\nfeedItemVideo=0\nfeedItemGallery=0\nfeedItemAttachments=0\nintroTextCleanup=0\nintroTextCleanupExcludeTags=\nintroTextCleanupTagAttr=\nfullTextCleanup=0\nfullTextCleanupExcludeTags=\nfullTextCleanupTagAttr=\nxssFiltering=0\nlinkPopupWidth=900\nlinkPopupHeight=600\nfrontendEditing=1\nshowImageTab=1\nshowImageGalleryTab=1\nshowVideoTab=1\nshowExtraFieldsTab=1\nshowAttachmentsTab=1\nshowK2Plugins=1\nsideBarDisplayFrontend=0\nmergeEditors=1\nsideBarDisplay=1\nattachmentsFolder=\nhideImportButton=0\ntaggingSystem=1\nlockTags=0\ngoogleSearch=0\ngoogleSearchContainer=k2Container\nK2UserProfile=1\nK2UserGroup=1\nredirect=\nadminSearch=simple\nshowItemsCounterAdmin=1\nshowChildCatItems=1\ndisableCompactOrdering=0\nSEFReplacements=Å \\|S, Å’\\|O, Å½\\|Z, Å¡\\|s, Å“\\|oe, Å¾\\|z, Å¸\\|Y, Â¥\\|Y, Âµ\\|u, Ã€\\|A, Ã�\\|A, Ã‚\\|A, Ãƒ\\|A, Ã„\\|A, Ã…\\|A, Ã†\\|A, Ã‡\\|C, Ãˆ\\|E, Ã‰\\|E, ÃŠ\\|E, Ã‹\\|E, ÃŒ\\|I, Ã�\\|I, ÃŽ\\|I, Ã�\\|I, Ã�\\|D, Ã‘\\|N, Ã’\\|O, Ã“\\|O, Ã”\\|O, Ã•\\|O, Ã–\\|O, Ã˜\\|O, Ã™\\|U, Ãš\\|U, Ã›\\|U, Ãœ\\|U, Ã�\\|Y, ÃŸ\\|s, Ã \\|a, Ã¡\\|a, Ã¢\\|a, Ã£\\|a, Ã¤\\|a, Ã¥\\|a, Ã¦\\|a, Ã§\\|c, Ã¨\\|e, Ã©\\|e, Ãª\\|e, Ã«\\|e, Ã¬\\|i, Ã­\\|i, Ã®\\|i, Ã¯\\|i, Ã°\\|o, Ã±\\|n, Ã²\\|o, Ã³\\|o, Ã´\\|o, Ãµ\\|o, Ã¶\\|o, Ã¸\\|o, Ã¹\\|u, Ãº\\|u, Ã»\\|u, Ã¼\\|u, Ã½\\|y, Ã¿\\|y, ÃŸ\\|ss\nmetaDescLimit=150\nsh404SefLabelCat=\nsh404SefLabelUser=blog\nsh404SefLabelItem=2\n\n', 1),
(47, 'Advanced Module Manager', '', 0, 0, '', 'Advanced Module Manager', 'com_advancedmodules', 0, '', 0, '\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_contact_details`
--

DROP TABLE IF EXISTS `jos_contact_details`;
CREATE TABLE IF NOT EXISTS `jos_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_contact_details`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_content`
--

DROP TABLE IF EXISTS `jos_content`;
CREATE TABLE IF NOT EXISTS `jos_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `title_alias` varchar(255) NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(11) unsigned NOT NULL DEFAULT '0',
  `mask` int(11) unsigned NOT NULL DEFAULT '0',
  `catid` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_section` (`sectionid`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `jos_content`
--

INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(1, '视频1', '1', '', '<p>青春期视频1</p>\r\n<p> </p>\r\n', '\r\n<p> </p>\r\n<p>{flv}video1{/flv}</p>\r\n<p>主创人员</p>\r\n<p>相关介绍</p>', -2, 1, 0, 1, '2011-02-04 10:01:14', 62, '', '2011-02-05 06:49:37', 62, 0, '0000-00-00 00:00:00', '2011-02-04 10:01:14', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 0, '', '', 0, 6, 'robots=\nauthor='),
(2, '视频2', '2', '', '<p><span style="font-family: Helvetica, Arial, sans-serif; line-height: 19px; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; "><img src="images/stories/clock.jpg" border="0" />青春期视频2</span></p>\r\n<p><span style="font-family: Helvetica, Arial, sans-serif; line-height: 19px; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; "> </span></p>\r\n', '\r\n<p> </p>\r\n<p> </p>\r\n<p>{flv}video1|850|400|1{/flv}</p>', -2, 1, 0, 1, '2011-02-04 10:07:19', 62, '', '2011-02-05 06:15:21', 62, 0, '0000-00-00 00:00:00', '2011-02-04 10:07:19', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 0, '', '', 0, 20, 'robots=\nauthor='),
(3, '视频3', '2', '', '<p><span style="font-family: Helvetica, Arial, sans-serif; line-height: 19px; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; "><img src="images/stories/articles.jpg" border="0" /></span><span style="font-family: Helvetica, Arial, sans-serif; line-height: 19px; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">青春期视频2</span></p>\r\n<p><span style="font-family: Helvetica, Arial, sans-serif; line-height: 19px; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; "> </span></p>\r\n', '\r\n<p> </p>\r\n<p> </p>\r\n<p>{flv}video1{/flv}</p>', -2, 1, 0, 1, '2011-02-04 10:07:19', 62, '', '2011-02-05 04:49:09', 62, 0, '0000-00-00 00:00:00', '2011-02-04 10:07:19', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 0, '', '', 0, 10, 'robots=\nauthor='),
(4, '视频4', '2', '', '<p><span style="font-family: Helvetica, Arial, sans-serif; line-height: 19px; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; ">青春期视频2</span></p>\r\n<p><span style="font-family: Helvetica, Arial, sans-serif; line-height: 19px; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; ">\r\n', '\r\n<br /></span></p>\r\n<p>{flv}video1{/flv}</p>', -2, 1, 0, 1, '2011-02-04 10:07:19', 62, '', '2011-02-04 11:07:48', 62, 0, '0000-00-00 00:00:00', '2011-02-04 10:07:19', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 0, '', '', 0, 3, 'robots=\nauthor='),
(5, '漫画1', '1', '', '<p>漫画1</p>', '', -2, 1, 0, 2, '2011-02-05 09:34:47', 62, '', '2011-02-05 09:36:12', 62, 0, '0000-00-00 00:00:00', '2011-02-05 09:34:47', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 0, '', '', 0, 2, 'robots=\nauthor='),
(6, '漫画2', '2', '', '<p>漫画2</p>', '', -2, 1, 0, 2, '2011-02-05 09:35:11', 62, '', '2011-02-05 09:36:05', 62, 0, '0000-00-00 00:00:00', '2011-02-05 09:35:11', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 0, '', '', 0, 1, 'robots=\nauthor='),
(7, '地区政策1', '1', '', '<p><span style="font-family: Tahoma, Verdana; line-height: normal;">如只是找test, 不可能是"bbb"的,因為Object.test不在test的scope chain里面<br /><br />全個過程大概是<br />找 "原地" 作用域的 var <br />&gt; "上層"作用域 的 var <br />&gt; "上層"作用域 的 var.....<br />&gt;到 當前的作用域是對象時( 如用with(obj){} ) <br />&gt;找"obj"對象的屬性 , 這層的var, JS 會把 var x 轉為 obj.x, <br />&gt;找obj.constructor.prototype對象的屬性<br />&gt;再向上找 "上層"作用域 的 var <br />&gt; .....到 window 對象作用域, 這層的 var test="aaa" 自動轉為 window.test="aaa" <br />&gt;找window對象的屬性 <br />&gt;找window.constructor.prototype的屬性<br />&gt;在IE8,window instanceof Object 為 false, 因此找不到ccc<br />&gt;在FF4, Chrome, (由於window是Object()的一個實體) 再找Object.prototype下的屬性, 找到ccc<br /><br />[<em> 本帖最后由 hksar 于 2011-5-1 17:32 编辑 </em>]</span></p>', '', -2, 3, 0, 3, '2011-05-01 14:02:47', 62, '', '2011-05-01 14:05:23', 62, 0, '0000-00-00 00:00:00', '2011-05-01 14:02:47', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 0, '', '', 0, 1, 'robots=\nauthor='),
(8, '地区政策2', '2', '', '<p><span style="font-family: Tahoma, Verdana; line-height: normal;">我觉得查找顺序应该是<br />1.找func局部变量，test根本没有在局部范围内没有定义，就根本不存在要沿着prototype链找<br />2.沿着scope链找到window.test，存在显示出来了<br />若上例中window.test注释掉，IE则找不到test，而FF和Chrome则<br />3.沿着window对象的长长的prototype链找一直找到Object.prototype.test<br />注：通过chrome可以发现window.__proto__.__proto__.__proto__ === Object.prototype<br /><br />至于在ff控制台里面为啥显示ccc，真让人抓狂，注释掉Object.prototype.test居然就显示了aaa，猜测如下：<br />可能ff控制台是在另一个未知的的context下运行的，基于这个假设，模拟控制台环境如下<br />var _console = {};<br />with(_console){<br />alert(test);<br />}<br />也许这就是谜底</span></p>', '', -2, 3, 0, 3, '2011-05-01 14:04:17', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-05-01 14:04:17', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 0, '', '', 0, 1, 'robots=\nauthor=');

-- --------------------------------------------------------

--
-- Table structure for table `jos_content_frontpage`
--

DROP TABLE IF EXISTS `jos_content_frontpage`;
CREATE TABLE IF NOT EXISTS `jos_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_content_frontpage`
--

INSERT INTO `jos_content_frontpage` (`content_id`, `ordering`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_content_rating`
--

DROP TABLE IF EXISTS `jos_content_rating`;
CREATE TABLE IF NOT EXISTS `jos_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_content_rating`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro`
--

DROP TABLE IF EXISTS `jos_core_acl_aro`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_aro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_value` varchar(240) NOT NULL DEFAULT '0',
  `value` varchar(240) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_section_value_value_aro` (`section_value`(100),`value`(100)),
  KEY `jos_gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jos_core_acl_aro`
--

INSERT INTO `jos_core_acl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', '62', 0, 'Administrator', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_groups`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_groups`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `jos_gacl_parent_id_aro_groups` (`parent_id`),
  KEY `jos_gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `jos_core_acl_aro_groups`
--

INSERT INTO `jos_core_acl_aro_groups` (`id`, `parent_id`, `name`, `lft`, `rgt`, `value`) VALUES
(17, 0, 'ROOT', 1, 22, 'ROOT'),
(28, 17, 'USERS', 2, 21, 'USERS'),
(29, 28, 'Public Frontend', 3, 12, 'Public Frontend'),
(18, 29, 'Registered', 4, 11, 'Registered'),
(19, 18, 'Author', 5, 10, 'Author'),
(20, 19, 'Editor', 6, 9, 'Editor'),
(21, 20, 'Publisher', 7, 8, 'Publisher'),
(30, 28, 'Public Backend', 13, 20, 'Public Backend'),
(23, 30, 'Manager', 14, 19, 'Manager'),
(24, 23, 'Administrator', 15, 18, 'Administrator'),
(25, 24, 'Super Administrator', 16, 17, 'Super Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_map`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_map`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_map` (
  `acl_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(230) NOT NULL DEFAULT '0',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_acl_aro_map`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_sections`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_sections`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(230) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(230) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_gacl_value_aro_sections` (`value`),
  KEY `jos_gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jos_core_acl_aro_sections`
--

INSERT INTO `jos_core_acl_aro_sections` (`id`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', 1, 'Users', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_groups_aro_map`
--

DROP TABLE IF EXISTS `jos_core_acl_groups_aro_map`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_groups_aro_map` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(240) NOT NULL DEFAULT '',
  `aro_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `group_id_aro_id_groups_aro_map` (`group_id`,`section_value`,`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_acl_groups_aro_map`
--

INSERT INTO `jos_core_acl_groups_aro_map` (`group_id`, `section_value`, `aro_id`) VALUES
(25, '', 10);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_log_items`
--

DROP TABLE IF EXISTS `jos_core_log_items`;
CREATE TABLE IF NOT EXISTS `jos_core_log_items` (
  `time_stamp` date NOT NULL DEFAULT '0000-00-00',
  `item_table` varchar(50) NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_log_items`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_core_log_searches`
--

DROP TABLE IF EXISTS `jos_core_log_searches`;
CREATE TABLE IF NOT EXISTS `jos_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_log_searches`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_gk2_photoslide_extensions`
--

DROP TABLE IF EXISTS `jos_gk2_photoslide_extensions`;
CREATE TABLE IF NOT EXISTS `jos_gk2_photoslide_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `type` varchar(128) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `phpclassfile` varchar(255) NOT NULL,
  `version` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `desc` mediumtext NOT NULL,
  `storage` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_gk2_photoslide_extensions`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_gk2_photoslide_groups`
--

DROP TABLE IF EXISTS `jos_gk2_photoslide_groups`;
CREATE TABLE IF NOT EXISTS `jos_gk2_photoslide_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `plugin` varchar(255) NOT NULL,
  `mediumThumbX` int(11) NOT NULL DEFAULT '0',
  `mediumThumbY` int(11) NOT NULL DEFAULT '0',
  `smallThumbX` int(11) NOT NULL DEFAULT '0',
  `smallThumbY` int(11) NOT NULL DEFAULT '0',
  `bgcolor` varchar(7) NOT NULL DEFAULT '0',
  `titlecolor` varchar(7) NOT NULL DEFAULT '0',
  `textcolor` varchar(7) NOT NULL DEFAULT '0',
  `linkcolor` varchar(7) NOT NULL DEFAULT '0',
  `hlinkcolor` varchar(7) NOT NULL DEFAULT '0',
  `quality` int(3) NOT NULL DEFAULT '75',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_gk2_photoslide_groups`
--

INSERT INTO `jos_gk2_photoslide_groups` (`id`, `name`, `plugin`, `mediumThumbX`, `mediumThumbY`, `smallThumbX`, `smallThumbY`, `bgcolor`, `titlecolor`, `textcolor`, `linkcolor`, `hlinkcolor`, `quality`) VALUES
(1, '首页幻灯', 'GK News Image I', 750, 350, 80, 80, '#000000', '#FFFFFF', '#AAAAAA', '#CCCCCC', '#EEEEEE', 100);

-- --------------------------------------------------------

--
-- Table structure for table `jos_gk2_photoslide_plugins`
--

DROP TABLE IF EXISTS `jos_gk2_photoslide_plugins`;
CREATE TABLE IF NOT EXISTS `jos_gk2_photoslide_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `status` int(11) NOT NULL,
  `type` varchar(128) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `phpclassfile` varchar(255) NOT NULL,
  `version` varchar(128) NOT NULL,
  `author` varchar(128) NOT NULL,
  `desc` mediumtext NOT NULL,
  `gfields` mediumtext NOT NULL,
  `sfields` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_gk2_photoslide_plugins`
--

INSERT INTO `jos_gk2_photoslide_plugins` (`id`, `name`, `status`, `type`, `filename`, `phpclassfile`, `version`, `author`, `desc`, `gfields`, `sfields`) VALUES
(1, 'GK News Image I', 1, 'module', 'plg_gk_news_image_1.xml', 'plg_gk_news_image_1.php', '2.0', 'GavickPro', 'XML file for module Gavick News Image I', 'mediumThumbX,mediumThumbY,smallThumbX,smallThumbY,bgcolor,titlecolor,textcolor,linkcolor,hlinkcolor,quality', 'title,text,linktype,linkvalue,article,wordcount,stretch');

-- --------------------------------------------------------

--
-- Table structure for table `jos_gk2_photoslide_slides`
--

DROP TABLE IF EXISTS `jos_gk2_photoslide_slides`;
CREATE TABLE IF NOT EXISTS `jos_gk2_photoslide_slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `published` int(1) NOT NULL,
  `access` int(1) NOT NULL,
  `file` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` varchar(255) NOT NULL DEFAULT '0',
  `linktype` int(1) NOT NULL DEFAULT '0',
  `linkvalue` varchar(255) NOT NULL DEFAULT '0',
  `article` int(11) NOT NULL DEFAULT '0',
  `wordcount` int(4) NOT NULL DEFAULT '0',
  `stretch` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jos_gk2_photoslide_slides`
--

INSERT INTO `jos_gk2_photoslide_slides` (`id`, `group_id`, `name`, `published`, `access`, `file`, `order`, `title`, `text`, `linktype`, `linkvalue`, `article`, `wordcount`, `stretch`) VALUES
(1, 1, '途锐', 1, 0, '232385slide_01.jpg', 1, '', '', 1, '', 1, 30, 0),
(2, 1, '甲壳虫', 1, 0, '215521slide_02.jpg', 2, '', '', 1, '', 1, 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_groups`
--

DROP TABLE IF EXISTS `jos_groups`;
CREATE TABLE IF NOT EXISTS `jos_groups` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_groups`
--

INSERT INTO `jos_groups` (`id`, `name`) VALUES
(0, 'Public'),
(1, 'Registered'),
(2, 'Special');

-- --------------------------------------------------------

--
-- Table structure for table `jos_jaem_log`
--

DROP TABLE IF EXISTS `jos_jaem_log`;
CREATE TABLE IF NOT EXISTS `jos_jaem_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_id` varchar(50) DEFAULT NULL,
  `check_date` datetime DEFAULT NULL,
  `check_info` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ext_id` (`ext_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_jaem_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_jaem_services`
--

DROP TABLE IF EXISTS `jos_jaem_services`;
CREATE TABLE IF NOT EXISTS `jos_jaem_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ws_name` varchar(255) NOT NULL,
  `ws_mode` varchar(50) NOT NULL DEFAULT 'local',
  `ws_uri` varchar(255) NOT NULL,
  `ws_user` varchar(100) NOT NULL,
  `ws_pass` varchar(100) NOT NULL,
  `ws_default` tinyint(1) NOT NULL DEFAULT '0',
  `ws_core` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jos_jaem_services`
--

INSERT INTO `jos_jaem_services` (`id`, `ws_name`, `ws_mode`, `ws_uri`, `ws_user`, `ws_pass`, `ws_default`, `ws_core`) VALUES
(1, 'Local Service', 'local', '', '', '', 1, 1),
(2, 'JoomlArt Updates', 'remote', 'http://update.joomlart.com/service/', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_attachments`
--

DROP TABLE IF EXISTS `jos_k2_attachments`;
CREATE TABLE IF NOT EXISTS `jos_k2_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `titleAttribute` text NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `itemID` (`itemID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_k2_attachments`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_categories`
--

DROP TABLE IF EXISTS `jos_k2_categories`;
CREATE TABLE IF NOT EXISTS `jos_k2_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) DEFAULT '0',
  `extraFieldsGroup` int(11) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `trash` smallint(6) NOT NULL DEFAULT '0',
  `plugins` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`published`,`access`,`trash`),
  KEY `parent` (`parent`),
  KEY `ordering` (`ordering`),
  KEY `published` (`published`),
  KEY `access` (`access`),
  KEY `trash` (`trash`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `jos_k2_categories`
--

INSERT INTO `jos_k2_categories` (`id`, `name`, `alias`, `description`, `parent`, `extraFieldsGroup`, `published`, `access`, `ordering`, `image`, `params`, `trash`, `plugins`) VALUES
(1, '青春期', '青春期', '', 0, 0, 1, 0, 1, '', 'inheritFrom=0\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=0\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=0\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(2, '政策', '政策', '', 0, 0, 1, 0, 2, '', 'inheritFrom=0\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 1, ''),
(3, '青春期视频', '青春期视频', '', 1, 0, 1, 0, 1, '', 'inheritFrom=0\nsubType=Video\ntheme=\nnum_leading_items=0\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=8\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=0\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=0\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=0\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=0\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=0\nitemAuthorImage=0\nitemAuthorDescription=0\nitemAuthorURL=0\nitemAuthorEmail=0\nitemAuthorLatest=0\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(4, '青春期动漫', '青春期动漫', '', 1, 0, 1, 0, 2, '', 'inheritFrom=0\nsubType=Video\ntheme=\nnum_leading_items=0\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=8\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=0\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=0\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(5, '地区政策', '地区政策', '', 0, 0, 1, 0, 2, '', 'inheritFrom=0\nsubType=Article\ntheme=\nnum_leading_items=0\nnum_leading_columns=1\nleadingImgSize=none\nnum_primary_items=20\nnum_primary_columns=0\nprimaryImgSize=none\nnum_secondary_items=0\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=0\nnum_links_columns=1\nlinksImgSize=none\ncatCatalogMode=0\ncatFeaturedItems=0\ncatOrdering=rdate\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(6, '广播', '广播', '', 0, 0, 1, 0, 3, '', 'inheritFrom=0\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(7, '墙报', '墙报', '', 0, 0, 1, 0, 4, '', 'inheritFrom=0\nsubType=Qiangbao\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(8, '文图类', '文图类', '', 1, 0, 1, 0, 3, '', 'inheritFrom=0\nsubType=Picture\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(9, '视频大类', '视频大类', '', 0, 0, 1, 0, 5, '', 'inheritFrom=0\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(10, '中心动态', '中心动态', '', 0, 0, 1, 0, 6, '', 'inheritFrom=0\nsubType=Article\nextraClass=zhongxindongtai\ntheme=\nnum_leading_items=0\nnum_leading_columns=1\nleadingImgSize=none\nnum_primary_items=20\nnum_primary_columns=0\nprimaryImgSize=none\nnum_secondary_items=0\nnum_secondary_columns=0\nsecondaryImgSize=none\nnum_links=0\nnum_links_columns=0\nlinksImgSize=none\ncatCatalogMode=0\ncatFeaturedItems=0\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(11, '首页视频', '首页视频', '', 0, 0, 1, 0, 7, '', 'inheritFrom=0\nsubType=Video\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(12, '新 婚', '新-婚', '', 0, 0, 1, 0, 8, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(13, '孕 期', '孕-期', '', 0, 0, 1, 0, 9, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(14, '0-3岁婴幼儿', '0-3岁婴幼儿', '', 0, 0, 1, 0, 10, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(15, '中老年', '中老年', '', 0, 0, 1, 0, 11, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(16, '男 性', '男-性', '', 0, 0, 1, 0, 12, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(17, '女 性', '女-性', '', 0, 0, 1, 0, 13, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(18, '避孕节育', '避孕节育', '', 0, 0, 1, 0, 14, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(19, '性病艾滋', '性病艾滋', '', 0, 0, 1, 0, 15, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(20, '流动人口', '流动人口', '', 0, 0, 1, 0, 16, '', 'inheritFrom=0\nsubType=Video\nextraClass=\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 0, ''),
(21, '中心动态', '中心动态', '', 0, 0, 1, 0, 17, '', 'inheritFrom=0\nsubType=Article\nextraClass=dongtai\ntheme=\nnum_leading_items=2\nnum_leading_columns=1\nleadingImgSize=Large\nnum_primary_items=4\nnum_primary_columns=2\nprimaryImgSize=Medium\nnum_secondary_items=4\nnum_secondary_columns=1\nsecondaryImgSize=Small\nnum_links=4\nnum_links_columns=1\nlinksImgSize=XSmall\ncatCatalogMode=0\ncatFeaturedItems=1\ncatOrdering=\ncatPagination=2\ncatPaginationResults=1\ncatTitle=1\ncatTitleItemCounter=1\ncatDescription=1\ncatImage=1\ncatFeedLink=1\nfeedLink=1\nsubCategories=1\nsubCatColumns=2\nsubCatOrdering=\nsubCatTitle=1\nsubCatTitleItemCounter=1\nsubCatDescription=1\nsubCatImage=1\nitemImageXS=\nitemImageS=\nitemImageM=\nitemImageL=\nitemImageXL=\ncatItemTitle=1\ncatItemTitleLinked=1\ncatItemFeaturedNotice=0\ncatItemAuthor=1\ncatItemDateCreated=1\ncatItemRating=0\ncatItemImage=1\ncatItemIntroText=1\ncatItemIntroTextWordLimit=\ncatItemExtraFields=0\ncatItemHits=0\ncatItemCategory=1\ncatItemTags=1\ncatItemAttachments=0\ncatItemAttachmentsCounter=0\ncatItemVideo=0\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=0\ncatItemImageGallery=0\ncatItemDateModified=0\ncatItemReadMore=1\ncatItemCommentsAnchor=1\ncatItemK2Plugins=1\nitemDateCreated=1\nitemTitle=1\nitemFeaturedNotice=1\nitemAuthor=1\nitemFontResizer=1\nitemPrintButton=1\nitemEmailButton=1\nitemSocialButton=1\nitemVideoAnchor=1\nitemImageGalleryAnchor=1\nitemCommentsAnchor=1\nitemRating=1\nitemImage=1\nitemImgSize=Large\nitemImageMainCaption=1\nitemImageMainCredits=1\nitemIntroText=1\nitemFullText=1\nitemExtraFields=1\nitemDateModified=1\nitemHits=1\nitemTwitterLink=1\nitemCategory=1\nitemTags=1\nitemShareLinks=1\nitemAttachments=1\nitemAttachmentsCounter=1\nitemRelated=1\nitemRelatedLimit=5\nitemVideo=1\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemImageGallery=1\nitemNavigation=1\nitemComments=1\nitemAuthorBlock=1\nitemAuthorImage=1\nitemAuthorDescription=1\nitemAuthorURL=1\nitemAuthorEmail=0\nitemAuthorLatest=1\nitemAuthorLatestLimit=5\nitemK2Plugins=1\n\n', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_comments`
--

DROP TABLE IF EXISTS `jos_k2_comments`;
CREATE TABLE IF NOT EXISTS `jos_k2_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `commentDate` datetime NOT NULL,
  `commentText` text NOT NULL,
  `commentEmail` varchar(255) NOT NULL,
  `commentURL` varchar(255) NOT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `itemID` (`itemID`),
  KEY `userID` (`userID`),
  KEY `published` (`published`),
  KEY `latestComments` (`published`,`commentDate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_k2_comments`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_extra_fields`
--

DROP TABLE IF EXISTS `jos_k2_extra_fields`;
CREATE TABLE IF NOT EXISTS `jos_k2_extra_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `group` int(11) NOT NULL,
  `published` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group`),
  KEY `published` (`published`),
  KEY `ordering` (`ordering`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_k2_extra_fields`
--

INSERT INTO `jos_k2_extra_fields` (`id`, `name`, `value`, `type`, `group`, `published`, `ordering`) VALUES
(1, 'SubType', '[{"name":"Video","value":1,"target":null},{"name":"Picture","value":2,"target":null},{"name":"Article","value":3,"target":null}]', 'radio', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_extra_fields_groups`
--

DROP TABLE IF EXISTS `jos_k2_extra_fields_groups`;
CREATE TABLE IF NOT EXISTS `jos_k2_extra_fields_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_k2_extra_fields_groups`
--

INSERT INTO `jos_k2_extra_fields_groups` (`id`, `name`) VALUES
(1, 'sub type');

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_items`
--

DROP TABLE IF EXISTS `jos_k2_items`;
CREATE TABLE IF NOT EXISTS `jos_k2_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `catid` int(11) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `video` text,
  `gallery` varchar(255) DEFAULT NULL,
  `extra_fields` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `extra_fields_search` text NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL,
  `checked_out` int(10) unsigned NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `trash` smallint(6) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `featured` smallint(6) NOT NULL DEFAULT '0',
  `featured_ordering` int(11) NOT NULL DEFAULT '0',
  `image_caption` text NOT NULL,
  `image_credits` varchar(255) NOT NULL,
  `video_caption` text NOT NULL,
  `video_credits` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL,
  `params` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `metakey` text NOT NULL,
  `plugins` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item` (`published`,`publish_up`,`publish_down`,`trash`,`access`),
  KEY `catid` (`catid`),
  KEY `created_by` (`created_by`),
  KEY `ordering` (`ordering`),
  KEY `featured` (`featured`),
  KEY `featured_ordering` (`featured_ordering`),
  KEY `hits` (`hits`),
  KEY `created` (`created`),
  FULLTEXT KEY `search` (`title`,`introtext`,`fulltext`,`extra_fields_search`,`image_caption`,`image_credits`,`video_caption`,`video_credits`,`metadesc`,`metakey`),
  FULLTEXT KEY `title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `jos_k2_items`
--

INSERT INTO `jos_k2_items` (`id`, `title`, `alias`, `catid`, `published`, `introtext`, `fulltext`, `video`, `gallery`, `extra_fields`, `extra_fields_search`, `created`, `created_by`, `created_by_alias`, `checked_out`, `checked_out_time`, `modified`, `modified_by`, `publish_up`, `publish_down`, `trash`, `access`, `ordering`, `featured`, `featured_ordering`, `image_caption`, `image_credits`, `video_caption`, `video_credits`, `hits`, `params`, `metadesc`, `metadata`, `metakey`, `plugins`) VALUES
(1, '视频1', '视频1', 3, 1, '<p><span style="color: #808080; font-family: Tahoma, sans-serif; line-height: 18px;">去年春晚上牛莉的大衣、手机链引领时尚热潮，而今年“兔子情侣衫”等“春晚</span><span style="color: #808080; font-family: Tahoma, sans-serif; line-height: 18px;">行头”再次火爆网络。</span></p>', '', '{flv}1{/flv}', NULL, '[]', '', '2011-02-05 10:40:18', 62, '', 0, '0000-00-00 00:00:00', '2011-08-28 09:15:33', 62, '2011-02-05 10:40:18', '0000-00-00 00:00:00', 0, 0, 1, 0, 0, '', '', '', '阿斯大法师父:宇宙应\r\n大法师\r\n立刻\r\n波波:阿斯顿噶是个\r\n', 150, 'mediaType=专题片\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(2, '视频2', '视频2', 3, 1, '<p>视频2</p>', '', NULL, NULL, '[]', '', '2011-02-05 10:40:36', 62, '', 0, '0000-00-00 00:00:00', '2011-08-28 09:39:41', 62, '2011-02-05 10:40:36', '0000-00-00 00:00:00', 0, 0, 2, 0, 0, '', '', '', '', 17, 'mediaType=专题片\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(3, '动漫1', '动漫1', 4, 1, '<p>动漫1</p>', '', NULL, NULL, '[]', '', '2011-02-05 10:40:52', 62, '', 0, '0000-00-00 00:00:00', '2011-08-28 09:43:49', 62, '2011-02-05 10:40:52', '0000-00-00 00:00:00', 0, 0, 1, 0, 0, '', '', '', '', 5, 'mediaType=专题片\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(4, '动漫2', '动漫2', 4, 1, '<p>动漫2</p>', '', NULL, NULL, '[]', '', '2011-02-05 10:41:11', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-02-05 10:41:11', '0000-00-00 00:00:00', 0, 0, 2, 0, 0, '', '', '', '', 21, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(5, 'TestMp3', 'testmp3', 4, 1, '<p>Test mp3</p>', '', '{mp3}5{/mp3}', '{gallery}5{/gallery}', '[]', '', '2011-05-01 07:31:32', 62, '', 0, '0000-00-00 00:00:00', '2011-05-02 08:31:12', 62, '2011-05-01 07:31:32', '0000-00-00 00:00:00', 0, 0, 3, 0, 0, '', '', '', '', 22, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(6, '出生缺陷三级预防', '出生缺陷三级预防', 8, 1, '', '', NULL, '{gallery}6{/gallery}', '[]', '', '2011-05-02 11:33:59', 62, '', 0, '0000-00-00 00:00:00', '2011-08-07 06:20:47', 62, '2011-05-02 11:33:59', '0000-00-00 00:00:00', 0, 0, 1, 0, 0, '', '', '', '', 116, 'mediaType=图书\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(7, '测试动漫青春期标题要长', '测试动漫青春期标题要长', 4, 1, '<p>测试动漫青春期标题要长</p>', '', NULL, NULL, '[]', '', '2011-05-12 14:13:59', 62, '', 62, '2011-08-06 01:32:04', '0000-00-00 00:00:00', 0, '2011-05-12 14:13:59', '0000-00-00 00:00:00', 0, 0, 4, 0, 0, '', '', '', '', 0, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(8, '23动漫1123', '23动漫1123', 4, 1, '<p>23动漫1123</p>', '', NULL, NULL, '[]', '', '2011-05-12 14:40:58', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-05-12 14:40:58', '0000-00-00 00:00:00', 0, 0, 5, 0, 0, '', '', '', '', 1, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(9, '12阿伽动漫', '12阿伽动漫', 4, 1, '<p>12阿伽动漫</p>', '', NULL, NULL, '[]', '', '2011-05-12 14:41:43', 62, '', 0, '0000-00-00 00:00:00', '2011-08-28 09:45:14', 62, '2011-05-12 14:41:43', '0000-00-00 00:00:00', 0, 0, 6, 0, 0, '', '', '', '', 8, 'mediaType=专题片\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(10, '地区政策1', '地区政策1', 5, 1, '<p>After eleven alphas and four betas Firebug Working Group (FWG) is proud  to declare victory and release the final Firebug 1.7.0! Of course also  Chromebug 1.7.0 is available. This version fixes three bugs reported  since the last beta. The main goal of Firebug 1.7 has been full  compatibility with Firefox 4 (released tomorrow!). However there [...]</p>', '', NULL, NULL, '[]', '', '2011-05-15 05:47:50', 62, '', 62, '2011-05-15 05:50:40', '0000-00-00 00:00:00', 0, '2011-05-15 05:47:50', '0000-00-00 00:00:00', 0, 0, 1, 0, 0, '', '', '', '', 2, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(11, '地区政策2', '地区政策2', 5, 1, '<div class="entry">getfirebug.com has Firebug 1.7b3 + Chromebug 1.7b3. We have got couple  of bug reports since yesterday and as we managed to quickly fix them we  also decided to yet release b3 for our awesome beta testers! CSS  Properties in the Style side-panel can be disabled/enabled again (issue  4227) JavaScript object are properly logged in the [...]</div>\r\n<div class="post">\r\n<h2><a name="Firebug 1.7b2" href="http://blog.getfirebug.com/2011/03/16/firebug-1-7b2/" rel="bookmark">Firebug 1.7b2</a></h2>\r\n<div class="postDate">16 March 2011 | 8:28 pm</div>\r\n<div class="entry">getfirebug.com has Firebug 1.7b2 + Chromebug 1.7b2! We have fixed 10  issues in this release. The most noticeable change is probably break  notification UI (issue 4144). Not only the message doesn’t take so much  space and so, more actual source code is visible, but it also nicely  teaches the user about how to get it [...]</div>\r\n</div>\r\n<div id="links">\r\n<h2 class="heading">Firebug Sponsors</h2>\r\n<div id="sidenav" class="sponsors">\r\n<ul>\r\n<li> <a href="http://www.mozilla.org/"><img src="http://getfirebug.com/img/mozilla-logo.jpg" border="0" /></a> <br /> <a href="http://www.mozilla.org/">mozilla.org</a> </li>\r\n<li> <a href="http://www.ibm.com/"><img src="http://getfirebug.com/img/ibm-logo.png" border="0" /></a> <br /> <a href="http://www.ibm.com/">ibm.com</a> </li>\r\n<li>\r\n<h3>Your Logo Here</h3>\r\n<a href="http://getfirebug.com/getinvolved">Join us</a> </li>\r\n</ul>\r\n</div>\r\n<div id="sidenav">\r\n<h3>What is Firebug?</h3>\r\n<ul>\r\n<li><a href="http://getfirebug.com/wiki/index.php/FAQ">Firebug FAQ</a></li>\r\n<li><a href="http://getfirebug.com/enable">Enabling Firebug</a></li>\r\n<li><a href="http://getfirebug.com/html">HTML Development</a></li>\r\n<li><a href="http://getfirebug.com/css">CSS Development</a></li>\r\n<li><a href="http://getfirebug.com/layout">CSS Layout</a></li>\r\n<li><a href="http://getfirebug.com/network">Network Monitoring</a></li>\r\n<li><a href="http://getfirebug.com/javascript">Javascript Debugging</a></li>\r\n<li><a href="http://getfirebug.com/errors">Finding Errors</a></li>\r\n<li><a href="http://getfirebug.com/dom">DOM Exploration</a></li>\r\n<li><a href="http://getfirebug.com/commandline">Javascript Command Line</a></li>\r\n<li><a href="http://getfirebug.com/logging">Javascript Logging</a></li>\r\n</ul>\r\n</div>\r\n</div>', '', NULL, NULL, '[]', '', '2011-05-15 05:48:35', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-05-15 05:48:35', '0000-00-00 00:00:00', 0, 0, 2, 0, 0, '', '', '', '', 1, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(12, '地区政策3', '地区政策3', 5, 1, '<p><a href="administrator/index.php?option=com_k2&amp;view=item&amp;cid=11">地区政策3 </a></p>', '', NULL, NULL, '[]', '', '2011-05-15 07:11:34', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-05-15 07:11:34', '0000-00-00 00:00:00', 0, 0, 3, 0, 0, '', '', '', '', 1, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(13, '地区政策4', '地区政策4', 5, 1, '<p>地区政策4</p>', '', NULL, NULL, '[]', '', '2011-05-15 07:11:55', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-05-15 07:11:55', '0000-00-00 00:00:00', 0, 0, 4, 0, 0, '', '', '', '', 0, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(14, '地区政策5', '地区政策5', 5, 1, '<p>地区政策5</p>', '', NULL, NULL, '[]', '', '2011-05-15 07:12:09', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-05-15 07:12:09', '0000-00-00 00:00:00', 0, 0, 5, 0, 0, '', '', '', '', 1, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(15, '地区政策6', '地区政策6', 5, 1, '<p>地区政策6</p>', '', NULL, NULL, '[]', '', '2011-05-15 07:12:20', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-05-15 07:12:20', '0000-00-00 00:00:00', 0, 0, 6, 0, 0, '', '', '', '', 1, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(16, '地区政策7', '地区政策7', 5, 1, '<p>地区政策7</p>', '', NULL, NULL, '[]', '', '2011-05-15 07:12:30', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-05-15 07:12:30', '0000-00-00 00:00:00', 0, 0, 7, 0, 0, '', '', '', '', 1, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(17, '地区政策8', '地区政策8', 5, 1, '<p>地区政策8</p>', '', NULL, NULL, '[]', '', '2011-05-15 07:12:40', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-05-15 07:12:40', '0000-00-00 00:00:00', 0, 0, 8, 0, 0, '', '', '', '', 0, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(18, '地区政策8', '地区政策8', 5, 1, '<p>地区政策8</p>', '', NULL, NULL, '[]', '', '2011-05-15 07:12:51', 62, '', 0, '0000-00-00 00:00:00', '2011-07-24 08:09:58', 62, '2011-05-15 07:12:51', '0000-00-00 00:00:00', 0, 0, 9, 0, 0, '', '', '', '', 35, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(19, '广播1', '广播1', 6, 1, '<p>广播1</p>', '', NULL, NULL, '[]', '', '2011-06-11 07:26:50', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-06-11 07:26:50', '0000-00-00 00:00:00', 0, 0, 1, 0, 0, '', '', '', '', 1, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(20, '广播2', '广播2', 6, 1, '<p>广播2</p>', '', NULL, NULL, '[]', '', '2011-06-11 07:27:32', 62, '', 62, '2011-08-07 05:47:33', '0000-00-00 00:00:00', 0, '2011-06-11 07:27:32', '0000-00-00 00:00:00', 0, 0, 2, 0, 0, '', '', '', '', 1, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(21, '中心动态1', '中心动态1', 10, 1, '<p><span style="font-family: 宋体; color: #000000; line-height: normal;"><span style="font-size: small;"><span> 为切实加强人口和计划生育宣传队伍的能力建设，为区县提供相应的专业指导服务，八月上旬，上海人口计生宣教中心开展了</span><span lang="EN-US"><span style="font-family: ''\\\\\\''\\\\\\\\\\\\&quot;Times\\\\\\'''';">2011</span></span><span>年第三期宣教技能高级培训班。应区县要求，本次培训就新闻摄影专业技能进行了深入探讨和重点学习，并赴新疆实践采风，共计</span><span lang="EN-US"><span style="font-family: ''\\\\\\''\\\\\\\\\\\\&quot;Times\\\\\\'''';">22</span></span></span><span style="font-size: small;"><span>名计生干部参加了培训活动。<br /> </span><span>采风学习过程中，学员们相互观摩、交流，把摄影理论知识和实践操作紧密结合起来，对照片拍摄技巧、创作与构图有了更直观的理解和感悟，对如何选择和收集高质量的人口计生新闻图片有了更深切的体会。培训活动同时为宣教中心和各区县提供了一个充分交流沟通的平台。</span></span></span></p>\r\n<p><span style="font-family: 宋体; color: #000000; line-height: normal;"><span style="font-size: small;"><span>dasfadf</span></span></span></p>', '', NULL, NULL, '[]', '', '2011-06-11 07:30:22', 62, '', 0, '0000-00-00 00:00:00', '2011-08-28 14:09:31', 62, '2011-06-11 07:30:22', '0000-00-00 00:00:00', 0, 0, 1, 0, 0, '', '', '', '', 26, 'mediaType=专题片\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(22, '中心动态2', '中心动态2', 10, 1, '<p>中心动态2</p>', '', NULL, NULL, '[]', '', '2011-06-11 07:30:50', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-06-11 07:30:50', '0000-00-00 00:00:00', 0, 0, 2, 0, 0, '', '', '', '', 2, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(23, '婚检与孕检', '婚检与孕检', 11, 1, '', '', '{flv}23{/flv}', NULL, '[]', '', '2011-06-11 08:04:23', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-06-11 08:04:23', '0000-00-00 00:00:00', 0, 0, 1, 0, 0, '', '', '', '', 0, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(24, '人口调控', '人口调控', 11, 1, '', '', '{flv}24{/flv}', NULL, '[]', '', '2011-06-11 08:05:39', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-06-11 08:05:39', '0000-00-00 00:00:00', 0, 0, 2, 0, 0, '', '', '', '', 0, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(25, '同在蓝天下', '同在蓝天下', 11, 1, '', '', '{flv}25{/flv}', NULL, '[]', '', '2011-06-11 11:10:34', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-06-11 11:10:34', '0000-00-00 00:00:00', 0, 0, 3, 0, 0, '', '', '', '', 0, 'catItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(26, '责任与义务', '责任与义务', 11, 1, '', '', '{flv}26{/flv}', NULL, '[]', '', '2011-06-11 11:11:11', 62, '', 0, '0000-00-00 00:00:00', '2011-08-07 06:17:40', 62, '2011-06-11 11:11:11', '0000-00-00 00:00:00', 0, 0, 4, 0, 0, '', '', '', '', 0, 'mediaType=专题片\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(27, '2011年第一期墙报', '2011年第一期墙报', 7, 1, '<p><span style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 20px;">2011年第一期墙报<br />一、“新上海人”生育政策使用信息指南<br />二、关爱女性健康从生殖健康开始<br />三、生殖健康Q&amp;A</span></p>', '', NULL, NULL, '[]', '', '2011-09-04 14:41:25', 62, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2011-09-04 14:41:25', '0000-00-00 00:00:00', 0, 0, 1, 0, 0, '', '', '', '', 3, 'mediaType=专题片\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', ''),
(28, '2011年第二期墙报', '2011年第二期墙报', 7, 1, '<p><span style="font-family: Arial, Helvetica, sans-serif; font-weight: bold; line-height: 20px;">一、“新上海人”生育政策使用信息指南<br />二、关爱女性健康从生殖健康开始<br />三、生殖健康Q&amp;A</span></p>', '', NULL, NULL, '[]', '', '2011-09-04 14:43:00', 62, '', 0, '0000-00-00 00:00:00', '2011-09-06 14:03:04', 62, '2011-09-04 14:43:00', '0000-00-00 00:00:00', 0, 0, 2, 0, 0, '', '', '', '', 9, 'mediaType=专题片\ncatItemTitle=\ncatItemTitleLinked=\ncatItemFeaturedNotice=\ncatItemAuthor=\ncatItemDateCreated=\ncatItemRating=\ncatItemImage=\ncatItemIntroText=\ncatItemExtraFields=\ncatItemHits=\ncatItemCategory=\ncatItemTags=\ncatItemAttachments=\ncatItemAttachmentsCounter=\ncatItemVideo=\ncatItemVideoWidth=\ncatItemVideoHeight=\ncatItemVideoAutoPlay=\ncatItemImageGallery=\ncatItemDateModified=\ncatItemReadMore=\ncatItemCommentsAnchor=\ncatItemK2Plugins=\nitemDateCreated=\nitemTitle=\nitemFeaturedNotice=\nitemAuthor=\nitemFontResizer=\nitemPrintButton=\nitemEmailButton=\nitemSocialButton=\nitemVideoAnchor=\nitemImageGalleryAnchor=\nitemCommentsAnchor=\nitemRating=\nitemImage=\nitemImgSize=\nitemImageMainCaption=\nitemImageMainCredits=\nitemIntroText=\nitemFullText=\nitemExtraFields=\nitemDateModified=\nitemHits=\nitemTwitterLink=\nitemCategory=\nitemTags=\nitemShareLinks=\nitemAttachments=\nitemAttachmentsCounter=\nitemRelated=\nitemRelatedLimit=\nitemVideo=\nitemVideoWidth=\nitemVideoHeight=\nitemVideoAutoPlay=\nitemVideoCaption=\nitemVideoCredits=\nitemImageGallery=\nitemNavigation=\nitemComments=\nitemAuthorBlock=\nitemAuthorImage=\nitemAuthorDescription=\nitemAuthorURL=\nitemAuthorEmail=\nitemAuthorLatest=\nitemAuthorLatestLimit=\nitemK2Plugins=\n\n', '', 'robots=\nauthor=', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_rating`
--

DROP TABLE IF EXISTS `jos_k2_rating`;
CREATE TABLE IF NOT EXISTS `jos_k2_rating` (
  `itemID` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_k2_rating`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_tags`
--

DROP TABLE IF EXISTS `jos_k2_tags`;
CREATE TABLE IF NOT EXISTS `jos_k2_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_k2_tags`
--

INSERT INTO `jos_k2_tags` (`id`, `name`, `published`) VALUES
(1, '视频系列', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_tags_xref`
--

DROP TABLE IF EXISTS `jos_k2_tags_xref`;
CREATE TABLE IF NOT EXISTS `jos_k2_tags_xref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tagID` (`tagID`),
  KEY `itemID` (`itemID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `jos_k2_tags_xref`
--

INSERT INTO `jos_k2_tags_xref` (`id`, `tagID`, `itemID`) VALUES
(10, 1, 1),
(11, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_user_groups`
--

DROP TABLE IF EXISTS `jos_k2_user_groups`;
CREATE TABLE IF NOT EXISTS `jos_k2_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jos_k2_user_groups`
--

INSERT INTO `jos_k2_user_groups` (`id`, `name`, `permissions`) VALUES
(1, 'Registered', 'frontEdit=0\nadd=0\neditOwn=0\neditAll=0\npublish=0\ncomment=1\ninheritance=0\ncategories=all\n\n'),
(2, 'Site Owner', 'frontEdit=1\nadd=1\neditOwn=1\neditAll=1\npublish=1\ncomment=1\ninheritance=1\ncategories=all\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_users`
--

DROP TABLE IF EXISTS `jos_k2_users`;
CREATE TABLE IF NOT EXISTS `jos_k2_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `gender` enum('m','f') NOT NULL DEFAULT 'm',
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `group` int(11) NOT NULL DEFAULT '0',
  `plugins` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  KEY `group` (`group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_k2_users`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_menu`
--

DROP TABLE IF EXISTS `jos_menu`;
CREATE TABLE IF NOT EXISTS `jos_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text,
  `type` varchar(50) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `componentid` int(11) unsigned NOT NULL DEFAULT '0',
  `sublevel` int(11) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pollid` int(11) NOT NULL DEFAULT '0',
  `browserNav` tinyint(4) DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `utaccess` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `lft` int(11) unsigned NOT NULL DEFAULT '0',
  `rgt` int(11) unsigned NOT NULL DEFAULT '0',
  `home` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `componentid` (`componentid`,`menutype`,`published`,`access`),
  KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `jos_menu`
--

INSERT INTO `jos_menu` (`id`, `menutype`, `name`, `alias`, `link`, `type`, `published`, `parent`, `componentid`, `sublevel`, `ordering`, `checked_out`, `checked_out_time`, `pollid`, `browserNav`, `access`, `utaccess`, `params`, `lft`, `rgt`, `home`) VALUES
(1, 'mainmenu', '首页', 'home', 'index.php?option=com_home', 'component', 1, 0, 45, 0, 3, 62, '2011-08-14 08:18:40', 0, 0, 0, 3, 'source=1\nlatestItemsCols=1\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryIDs=5|6|7\ncategoryTitle=1\ncategoryDescription=0\ncategoryImage=0\ncategoryFeed=0\nlatestItemTitle=0\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=0\nlatestItemImageSize=Small\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 1),
(2, 'mainmenu', '青春期', '2011-02-04-10-20-17', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=1\ntitleImage=templates/jisheng_video/css/images/subPageTitle.jpg\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=4\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=0\nuserDescription=0\nuserURL=0\nuserEmail=0\nuserFeed=0\ncategoryIDs=4|3|8\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=0\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=0\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=0\nlatestItemCategory=0\nlatestItemTags=0\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=0\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=articles.jpg\nsecure=0\n\n', 0, 0, 0),
(3, 'mainmenu', '本地政策', '2011-05-01-13-58-20', 'index.php?option=com_content&view=category&id=3', 'component', -2, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'display_num=20\nshow_headings=1\nshow_date=0\ndate_format=\nfilter=1\nfilter_type=title\norderby_sec=\nshow_pagination=1\nshow_pagination_limit=1\nshow_feed_link=0\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=0\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=-testSurffix\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(4, 'mainmenu', '政策类', '2011-05-15-05-49-59', 'index.php?option=com_k2&view=itemlist&layout=category&task=category&id=5', 'component', 1, 0, 45, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'titleImage=地区政策<span>Regional Policy</span>\nitemType=Article\ncategories=5\nfeedLink=1\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(5, 'mainmenu', '新婚', '2011-08-14-07-06-26', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryIDs=12\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(6, 'mainmenu', '孕期', '2011-08-14-07-07-56', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryIDs=13\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(7, 'mainmenu', '0-3岁婴幼儿', '0-3', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 8, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(8, 'mainmenu', '中老年', '2011-08-14-07-08-59', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 9, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(9, 'mainmenu', '男性', '2011-08-14-07-09-24', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 10, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(10, 'mainmenu', '女性', '2011-08-14-07-09-44', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 11, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(11, 'mainmenu', '避孕节目', '2011-08-14-07-10-06', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 12, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(12, 'mainmenu', '性病艾滋', '2011-08-14-07-10-33', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 13, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(13, 'mainmenu', '流动人口', '2011-08-14-07-11-00', 'index.php?option=com_k2&view=latest&layout=latest', 'component', 1, 0, 45, 0, 14, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'source=0\ntitleImage=\nitemType=Video\nlatestItemsCols=2\nlatestItemsLimit=3\nlatestItemsDisplayEffect=all\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\ncategoryTitle=1\ncategoryDescription=1\ncategoryImage=1\ncategoryFeed=1\nlatestItemTitle=1\nlatestItemTitleLinked=1\nlatestItemDateCreated=1\nlatestItemImage=1\nlatestItemImageSize=Medium\nlatestItemVideo=1\nlatestItemVideoWidth=\nlatestItemVideoHeight=\nlatestItemVideoAutoPlay=0\nlatestItemIntroText=1\nlatestItemCategory=1\nlatestItemTags=1\nlatestItemReadMore=1\nlatestItemCommentsAnchor=0\nfeedLink=1\nlatestItemK2Plugins=0\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(14, 'mainmenu', 'zhongxindongtai', 'zhongxindongtai', 'index.php?option=com_k2&view=itemlist&layout=category&task=category&id=10', 'component', -2, 0, 45, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'titleImage=中心动态<span>Latest News</span>\nitemType=Article\ncategories=10\nfeedLink=1\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(15, 'mainmenu', 'centerNews', 'centernews', 'index.php?option=com_k2&view=itemlist&layout=category&task=category&id=10', 'component', 1, 0, 45, 0, 15, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'titleImage=中心动态<span>Center News</span>\nitemType=Article\ncategories=10\nfeedLink=1\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_menu_types`
--

DROP TABLE IF EXISTS `jos_menu_types`;
CREATE TABLE IF NOT EXISTS `jos_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_menu_types`
--

INSERT INTO `jos_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site');

-- --------------------------------------------------------

--
-- Table structure for table `jos_messages`
--

DROP TABLE IF EXISTS `jos_messages`;
CREATE TABLE IF NOT EXISTS `jos_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(11) NOT NULL DEFAULT '0',
  `priority` int(1) unsigned NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_messages`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_messages_cfg`
--

DROP TABLE IF EXISTS `jos_messages_cfg`;
CREATE TABLE IF NOT EXISTS `jos_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_messages_cfg`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_migration_backlinks`
--

DROP TABLE IF EXISTS `jos_migration_backlinks`;
CREATE TABLE IF NOT EXISTS `jos_migration_backlinks` (
  `itemid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `sefurl` text NOT NULL,
  `newurl` text NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_migration_backlinks`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_modules`
--

DROP TABLE IF EXISTS `jos_modules`;
CREATE TABLE IF NOT EXISTS `jos_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) DEFAULT NULL,
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `numnews` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `control` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `jos_modules`
--

INSERT INTO `jos_modules` (`id`, `title`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `published`, `module`, `numnews`, `access`, `showtitle`, `params`, `iscore`, `client_id`, `control`) VALUES
(1, 'Main Menu', '', 0, 'hornav', 62, '2011-05-01 14:20:56', 1, 'mod_mainmenu', 0, 0, 1, 'menutype=mainmenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(2, 'Login', '', 1, 'login', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, '', 1, 1, ''),
(3, 'Popular', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_popular', 0, 2, 1, '', 0, 1, ''),
(4, 'Recent added Articles', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_latest', 0, 2, 1, 'ordering=c_dsc\nuser_id=0\ncache=0\n\n', 0, 1, ''),
(5, 'Menu Stats', '', 5, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_stats', 0, 2, 1, '', 0, 1, ''),
(6, 'Unread Messages', '', 1, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_unread', 0, 2, 1, '', 1, 1, ''),
(7, 'Online Users', '', 2, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_online', 0, 2, 1, '', 1, 1, ''),
(8, 'Toolbar', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', 1, 'mod_toolbar', 0, 2, 1, '', 1, 1, ''),
(9, 'Quick Icons', '', 1, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_quickicon', 0, 2, 1, '', 1, 1, ''),
(10, 'Logged in Users', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_logged', 0, 2, 1, '', 0, 1, ''),
(11, 'Footer', '', 0, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_footer', 0, 0, 1, '', 1, 1, ''),
(12, 'Admin Menu', '', 1, 'menu', 0, '0000-00-00 00:00:00', 1, 'mod_menu', 0, 2, 1, '', 0, 1, ''),
(13, 'Admin SubMenu', '', 1, 'submenu', 0, '0000-00-00 00:00:00', 1, 'mod_submenu', 0, 2, 1, '', 0, 1, ''),
(14, 'User Status', '', 1, 'status', 0, '0000-00-00 00:00:00', 1, 'mod_status', 0, 2, 1, '', 0, 1, ''),
(15, 'Title', '', 1, 'title', 0, '0000-00-00 00:00:00', 1, 'mod_title', 0, 2, 1, '', 0, 1, ''),
(16, '顶部菜单', '', 1, 'user3', 62, '2011-05-01 14:21:37', 0, 'mod_mainmenu', 0, 0, 1, 'menutype=\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 0, 0, ''),
(17, '计生调查', '', 6, 'left', 62, '2011-07-17 09:00:31', 1, 'mod_poll', 0, 0, 1, 'id=1\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(18, 'Gavick News Highlighter GK1', '', 3, 'user2', 0, '0000-00-00 00:00:00', 0, 'mod_gk_news_highlighter', 0, 0, 1, 'moduleclass_sfx=\nmodule_id=news-highlight-1\nmoduleHeight=24\nmoduleWidth=780\ninterfaceWidth=120\nbgcolor=#FFFFEE\nbordercolor=#E9E9A1\nlinkcolor=\nhlinkcolor=\ntextleft_color=\ntextleft_style=normal\nintrotext=HOT NEWS\nextra_divs=1\narrows=1\nsection=0\ncategory=0\nsections=\ncategories=\nids=\nnews_amount=\nnews_sort_value=created\nnews_sort_order=DESC\nnews_frontpage=1\nunauthorized=0\nonly_frontpage=0\nstartposition=0\nlinks=1\nshow_title=1\nshow_description=1\ntitle_length=50\ndesc_length=50\nanimationType=1\nanimationSpeed=250\nanimationInterval=5000\nanimationFun=Fx.Transitions.linear\nmouseover=1\nclean_code=1\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(19, 'Gavick News Image I', '', 0, 'banner', 0, '0000-00-00 00:00:00', 0, 'mod_gk_news_image_1', 0, 0, 1, 'moduleclass_sfx=\nmodule_id=newsimage1\ngroup_id=1\nmodule_width=0\nmodule_height=0\nimage_x=0\nimage_y=0\nshow_text_block=1\ntext_block_width=200\ntext_block_margin=50\ntext_block_opacity=0.45\ntext_block_background=1\ntext_block_bgcolor=#000000\nclean_xhtml=1\nreadmore_button=1\nreadmore_text=Read more\ntitle_link=1\nprev_button=1\nnext_button=1\nplay_button=1\ninterface_x=20\ninterface_y=20\nslidelinks=1\nthumbnail_bar=1\nthumbnail_margin=4\nthumbnail_border=1\nthumbnail_border_color_inactive=#000000\nthumbnail_border_color=#FFFFFF\nthumbnail_bar_position=1\nshow_ticks=1\ntick_x=20\ntick_y=20\npreloading=1\nanimation_slide_speed=1000\nanimation_interval=5000\nautoanimation=1\nanimation_slide_type=0\nanimation_text_type=0\nclean_code=1\nuseMoo=2\nuseScript=2\ncompress_js=1\n\n', 0, 0, ''),
(20, '热门视频', '', 5, 'left', 62, '2011-02-05 13:35:08', 0, 'mod_mostread', 0, 0, 1, 'moduleclass_sfx=\nshow_front=1\ncount=5\ncatid=\nsecid=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(42, 'k2搜索', '', 0, 'banner', 62, '2011-08-07 06:58:10', 1, 'mod_k2_tools', 0, 0, 1, 'moduleclass_sfx=\nmodule_usage=6\narchiveItemsCounter=0\narchiveCategory=0\nauthors_module_category=0\nauthorItemsCounter=0\nauthorAvatar=0\nauthorAvatarWidthSelect=custom\nauthorAvatarWidth=50\nauthorLatestItem=1\ncalendarCategory=0\nhome=\nseperator=\nroot_id=0\nend_level=\ncategoriesListOrdering=\ncategoriesListItemsCounter=1\nroot_id2=0\nwidth=20\ntext=\nbutton=1\nimagebutton=\nbutton_text=\nmin_size=75\nmax_size=300\ncloud_limit=30\ncloud_category=0\ncloud_category_recursive=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(28, 'JA News Pro Module', '', 7, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_janewspro', 0, 0, 1, 'profile=default\nsource=JANewsHelper\ngetFlexiChildren=1\ngetChildren=1\nordering=ordering\nmaxSubCats=-1\n', 0, 0, ''),
(29, 'JA News Ticker Module', '', 8, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_janewsticker', 0, 0, 1, '', 0, 0, ''),
(30, 'JA Facebook Social Like Box Module', '', 9, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_jafacebooklikebox', 0, 0, 1, 'id=123144964369587\nwidth=300\nheight=587\nconnections=10\nstream=1\nheader=1\ncolorscheme=1\n', 0, 0, ''),
(31, 'JA Bulletin', '', 10, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_jabulletin', 0, 0, 1, 'type=latest\nmoduleclass_sfx=\ncategory=\ncount=5\nshow_front=1\nshow_date=1\nshow_image=1\nthumbnail_mode=crop\nthumbnail_mode-resize-use_ratio=1\nwidth=0\nheight=0\ncache=1\ncache_time=30\n\n', 0, 0, ''),
(32, 'K2 Comments', '', 11, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_k2_comments', 0, 0, 1, 'comments_limit=5\ncomments_word_limit=10\ncommenterName=1\ncommentAvatar=1\ncommentAvatarWidthSelect=custom\ncommentAvatarWidth=50\ncommentDate=1\ncommentDateFormat=absolute\ncommentLink=1\nitemTitle=1\nitemCategory=1\nfeed=1\ncommenters_limit=5\ncommenterAvatar=1\ncommenterAvatarWidthSelect=custom\ncommenterAvatarWidth=50\ncommenterLink=1\ncommenterCommentsCounter=1\ncommenterLatestComment=1\n', 0, 0, ''),
(34, 'K2 Login', '', 12, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_k2_login', 0, 0, 1, 'name=1\nuserAvatar=1\nuserAvatarWidthSelect=custom\nuserAvatarWidth=50\n', 0, 0, ''),
(35, 'K2 Tools', '', 13, 'left', 62, '2011-02-05 11:15:39', 0, 'mod_k2_tools', 0, 0, 1, 'archiveItemsCounter=1\nauthorItemsCounter=1\nauthorAvatar=1\nauthorAvatarWidthSelect=custom\nauthorAvatarWidth=50\nauthorLatestItem=1\ncategoriesListItemsCounter=1\nwidth=20\nmin_size=75\nmax_size=300\ncloud_limit=30\n', 0, 0, ''),
(36, 'K2 Users', '', 14, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_k2_users', 0, 0, 1, 'getTemplate=Default\nfilter=1\nordering=1\nlimit=4\nuserName=1\nuserAvatar=1\nuserAvatarWidthSelect=custom\nuserAvatarWidth=50\nuserDescription=1\nuserURL=1\nuserFeed=1\nuserItemCount=1\n', 0, 0, ''),
(37, 'K2 QuickIcons (admin)', '', 99, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_k2_quickicons', 0, 2, 1, 'modCSSStyling=1\nmodLogo=1\n', 0, 1, ''),
(39, '最新视频-k2', '', 4, 'left', 62, '2011-06-05 08:21:09', 1, 'mod_k2_content', 0, 0, 1, 'moduleclass_sfx=\ngetTemplate=Default\nsource=filter\ncatfilter=0\ngetChildren=0\nitemCount=5\nitemsOrdering=rdate\nFeaturedItems=1\npopularityRange=\nvideosOnly=0\nitem=0\nitemTitle=1\nitemAuthor=0\nitemAuthorAvatar=0\nitemAuthorAvatarWidthSelect=custom\nitemAuthorAvatarWidth=50\nuserDescription=0\nitemIntroText=0\nitemIntroTextWordLimit=\nitemImage=0\nitemImgSize=Small\nitemVideo=0\nitemVideoCaption=0\nitemVideoCredits=0\nitemAttachments=0\nitemTags=0\nitemCategory=0\nitemDateCreated=0\nitemHits=0\nitemReadMore=0\nitemExtraFields=0\nitemCommentsCounter=0\nfeed=0\nitemPreText=\nitemCustomLink=0\nitemCustomLinkURL=http://\nitemCustomLinkTitle=\nK2Plugins=1\nJPlugins=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(40, 'content_most_popular', '', 3, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_k2_content', 0, 0, 1, 'moduleclass_sfx=\nnew_class_sfx=\ngetTemplate=ContentPageLeft\nsource=filter\ncatfilter=0\ngetChildren=0\nitemCount=5\nitemsOrdering=hits\nFeaturedItems=1\npopularityRange=\nvideosOnly=0\nitem=0\nitemTitle=1\nitemAuthor=1\nitemAuthorAvatar=1\nitemAuthorAvatarWidthSelect=custom\nitemAuthorAvatarWidth=50\nuserDescription=1\nitemIntroText=1\nitemIntroTextWordLimit=\nitemImage=1\nitemImgSize=Small\nitemVideo=0\nitemVideoCaption=1\nitemVideoCredits=1\nitemAttachments=1\nitemTags=1\nitemCategory=1\nitemDateCreated=1\nitemHits=1\nitemReadMore=1\nitemExtraFields=0\nitemCommentsCounter=1\nfeed=1\nitemPreText=\nitemCustomLink=0\nitemCustomLinkURL=http://\nitemCustomLinkTitle=\nK2Plugins=1\nJPlugins=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(43, 'latest_videos_main', '', 2, 'left', 62, '2011-09-04 14:36:25', 1, 'mod_k2_content', 0, 0, 0, 'moduleclass_sfx=greenNumberList\nnew_class_sfx=greenNumberList\ngetTemplate=MainPageLeft\nsource=filter\ncatfilter=0\ngetChildren=0\nitemCount=12\nitemsOrdering=rdate\nFeaturedItems=1\npopularityRange=\nvideosOnly=0\nitem=0\nitemTitle=1\nitemAuthor=0\nitemAuthorAvatar=0\nitemAuthorAvatarWidthSelect=custom\nitemAuthorAvatarWidth=50\nuserDescription=0\nitemIntroText=0\nitemIntroTextWordLimit=\nitemImage=0\nitemImgSize=Small\nitemVideo=0\nitemVideoCaption=0\nitemVideoCredits=0\nitemAttachments=0\nitemTags=0\nitemCategory=0\nitemDateCreated=0\nitemHits=0\nitemReadMore=0\nitemExtraFields=0\nitemCommentsCounter=0\nfeed=0\nitemPreText=\nitemCustomLink=0\nitemCustomLinkURL=http://\nitemCustomLinkTitle=\nK2Plugins=1\nJPlugins=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(44, 'most_popular_main', '', 1, 'left', 62, '2011-06-12 10:30:13', 1, 'mod_k2_content', 0, 0, 0, 'moduleclass_sfx=test\r\nnew_class_sfx=test\r\ngetTemplate=MainPageLeft\r\nsource=filter\r\ncatfilter=0\r\ngetChildren=0\r\nitemCount=12\r\nitemsOrdering=hits\r\nFeaturedItems=1\r\npopularityRange=\r\nvideosOnly=0\r\nitem=0\r\nitemTitle=1\r\nitemAuthor=0\r\nitemAuthorAvatar=0\r\nitemAuthorAvatarWidthSelect=custom\r\nitemAuthorAvatarWidth=50\r\nuserDescription=0\r\nitemIntroText=0\r\nitemIntroTextWordLimit=\r\nitemImage=0\r\nitemImgSize=Small\r\nitemVideo=0\r\nitemVideoCaption=0\r\nitemVideoCredits=0\r\nitemAttachments=0\r\nitemTags=0\r\nitemCategory=0\r\nitemDateCreated=0\r\nitemHits=0\r\nitemReadMore=0\r\nitemExtraFields=0\r\nitemCommentsCounter=0\r\nfeed=0\r\nitemPreText=\r\nitemCustomLink=0\r\nitemCustomLinkURL=http://\r\nitemCustomLinkTitle=\r\nK2Plugins=1\r\nJPlugins=1\r\ncache=1\r\ncache_time=900\r\n\r\n', 0, 0, ''),
(51, 'renkou_clock', '', 15, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_renkou_clock', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(52, 'video_main', '', 16, 'left', 62, '2011-06-12 10:27:20', 1, 'mod_video_main', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(53, 'main_region_policy', '', 17, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_k2_content', 0, 0, 1, 'moduleclass_sfx=\nnew_class_sfx=\ngetTemplate=MainPageArticles\nsource=filter\ncatfilter=1\ncategory_id=5\ngetChildren=0\nitemCount=8\nitemsOrdering=rdate\nFeaturedItems=1\npopularityRange=\nvideosOnly=0\nitem=0\nitemTitle=1\nitemAuthor=1\nitemAuthorAvatar=1\nitemAuthorAvatarWidthSelect=custom\nitemAuthorAvatarWidth=50\nuserDescription=1\nitemIntroText=1\nitemIntroTextWordLimit=\nitemImage=1\nitemImgSize=Small\nitemVideo=1\nitemVideoCaption=1\nitemVideoCredits=1\nitemAttachments=1\nitemTags=1\nitemCategory=1\nitemDateCreated=1\nitemHits=1\nitemReadMore=1\nitemExtraFields=0\nitemCommentsCounter=1\nfeed=1\nitemPreText=地区政策<span>Regional Policy</span>\nitemCustomLink=0\nitemCustomLinkURL=http://\nitemCustomLinkTitle=\nK2Plugins=1\nJPlugins=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(54, 'main_broadcast', '', 18, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_k2_content', 0, 0, 1, 'moduleclass_sfx=\nnew_class_sfx=\ngetTemplate=MainPageArticles\nsource=filter\ncatfilter=1\ncategory_id=6\ngetChildren=0\nitemCount=8\nitemsOrdering=rdate\nFeaturedItems=1\npopularityRange=\nvideosOnly=0\nitem=0\nitemTitle=1\nitemAuthor=1\nitemAuthorAvatar=1\nitemAuthorAvatarWidthSelect=custom\nitemAuthorAvatarWidth=50\nuserDescription=1\nitemIntroText=1\nitemIntroTextWordLimit=\nitemImage=1\nitemImgSize=Small\nitemVideo=1\nitemVideoCaption=1\nitemVideoCredits=1\nitemAttachments=1\nitemTags=1\nitemCategory=1\nitemDateCreated=1\nitemHits=1\nitemReadMore=1\nitemExtraFields=0\nitemCommentsCounter=1\nfeed=1\nitemPreText=广播栏目<span>Broadcast</span>\nitemCustomLink=0\nitemCustomLinkURL=http://\nitemCustomLinkTitle=\nK2Plugins=1\nJPlugins=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(55, 'main_center', '', 19, 'left', 62, '2011-06-25 10:45:47', 1, 'mod_k2_content', 0, 0, 1, 'moduleclass_sfx=centerNews\nnew_class_sfx=\ngetTemplate=MainPageArticles\nsource=filter\ncatfilter=1\ncategory_id=10\ngetChildren=0\nitemCount=8\nitemsOrdering=rdate\nFeaturedItems=1\npopularityRange=\nvideosOnly=0\nitem=0\nitemTitle=1\nitemAuthor=1\nitemAuthorAvatar=1\nitemAuthorAvatarWidthSelect=custom\nitemAuthorAvatarWidth=50\nuserDescription=1\nitemIntroText=1\nitemIntroTextWordLimit=\nitemImage=1\nitemImgSize=Small\nitemVideo=1\nitemVideoCaption=1\nitemVideoCredits=1\nitemAttachments=1\nitemTags=1\nitemCategory=1\nitemDateCreated=1\nitemHits=1\nitemReadMore=1\nitemExtraFields=0\nitemCommentsCounter=1\nfeed=1\nitemPreText=中心动态<span>Center News</span>\nitemCustomLink=0\nitemCustomLinkURL=http://\nitemCustomLinkTitle=\nK2Plugins=1\nJPlugins=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(58, 'renkou', '', 20, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_jisheng_poll', 0, 0, 1, 'id=1\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(59, 'qiangbao_list', '', 21, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_k2_content', 0, 0, 1, 'moduleclass_sfx=\nnew_class_sfx=\ngetTemplate=Qiangbao\nsource=filter\ncatfilter=1\ncategory_id=7\ngetChildren=0\nitemCount=6\nitemsOrdering=rdate\nFeaturedItems=1\npopularityRange=\nvideosOnly=0\nitem=0\nitemTitle=1\nitemAuthor=1\nitemAuthorAvatar=1\nitemAuthorAvatarWidthSelect=custom\nitemAuthorAvatarWidth=50\nuserDescription=1\nitemIntroText=1\nitemIntroTextWordLimit=\nitemImage=1\nitemImgSize=Small\nitemVideo=1\nitemVideoCaption=1\nitemVideoCredits=1\nitemAttachments=1\nitemTags=1\nitemCategory=1\nitemDateCreated=1\nitemHits=1\nitemReadMore=1\nitemExtraFields=0\nitemCommentsCounter=1\nfeed=1\nitemPreText=\nitemCustomLink=0\nitemCustomLinkURL=http://\nitemCustomLinkTitle=\nK2Plugins=1\nJPlugins=1\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(60, 'qiangbao_home', '', 22, 'left', 0, '0000-00-00 00:00:00', 1, 'mod_k2_content', 0, 0, 1, 'moduleclass_sfx=\nnew_class_sfx=\ngetTemplate=QiangbaoHome\nsource=filter\ncatfilter=1\ncategory_id=7\ngetChildren=0\nitemCount=1\nitemsOrdering=rdate\nFeaturedItems=1\npopularityRange=\nvideosOnly=0\nitem=0\nitemTitle=1\nitemAuthor=1\nitemAuthorAvatar=1\nitemAuthorAvatarWidthSelect=custom\nitemAuthorAvatarWidth=50\nuserDescription=1\nitemIntroText=1\nitemIntroTextWordLimit=\nitemImage=1\nitemImgSize=Small\nitemVideo=1\nitemVideoCaption=1\nitemVideoCredits=1\nitemAttachments=1\nitemTags=1\nitemCategory=1\nitemDateCreated=1\nitemHits=1\nitemReadMore=1\nitemExtraFields=0\nitemCommentsCounter=1\nfeed=1\nitemPreText=\nitemCustomLink=0\nitemCustomLinkURL=http://\nitemCustomLinkTitle=\nK2Plugins=1\nJPlugins=1\ncache=1\ncache_time=900\n\n', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_modules_menu`
--

DROP TABLE IF EXISTS `jos_modules_menu`;
CREATE TABLE IF NOT EXISTS `jos_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_modules_menu`
--

INSERT INTO `jos_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(16, 0),
(17, 1),
(17, 2),
(18, 0),
(19, 1),
(20, 1),
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(34, 0),
(35, 0),
(36, 0),
(37, 0),
(39, 1),
(39, 2),
(40, 0),
(42, 0),
(43, 1),
(43, 2),
(44, 1),
(44, 2),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(58, 0),
(59, 0),
(60, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_newsfeeds`
--

DROP TABLE IF EXISTS `jos_newsfeeds`;
CREATE TABLE IF NOT EXISTS `jos_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text NOT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(11) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(11) unsigned NOT NULL DEFAULT '3600',
  `checked_out` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_newsfeeds`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_plugins`
--

DROP TABLE IF EXISTS `jos_plugins`;
CREATE TABLE IF NOT EXISTS `jos_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `element` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) NOT NULL DEFAULT '',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder` (`published`,`client_id`,`access`,`folder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `jos_plugins`
--

INSERT INTO `jos_plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Authentication - Joomla', 'joomla', 'authentication', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(2, 'Authentication - LDAP', 'ldap', 'authentication', 0, 2, 0, 1, 0, 0, '0000-00-00 00:00:00', 'host=\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),
(3, 'Authentication - GMail', 'gmail', 'authentication', 0, 4, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 'Authentication - OpenID', 'openid', 'authentication', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(5, 'User - Joomla!', 'joomla', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'autoregister=1\n\n'),
(6, 'Search - Content', 'content', 'search', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch_archived=1\n\n'),
(7, 'Search - Contacts', 'contacts', 'search', 0, 3, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(8, 'Search - Categories', 'categories', 'search', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(9, 'Search - Sections', 'sections', 'search', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(10, 'Search - Newsfeeds', 'newsfeeds', 'search', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(11, 'Search - Weblinks', 'weblinks', 'search', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(12, 'Content - Pagebreak', 'pagebreak', 'content', 0, 10000, 1, 1, 0, 0, '0000-00-00 00:00:00', 'enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),
(13, 'Content - Rating', 'vote', 'content', 0, 4, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(14, 'Content - Email Cloaking', 'emailcloak', 'content', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'mode=1\n\n'),
(15, 'Content - Code Hightlighter (GeSHi)', 'geshi', 'content', 0, 5, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(16, 'Content - Load Module', 'loadmodule', 'content', 0, 6, 1, 0, 0, 62, '2011-06-05 13:32:44', 'enabled=1\nstyle=0\n\n'),
(17, 'Content - Page Navigation', 'pagenavigation', 'content', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'position=1\n\n'),
(18, 'Editor - No Editor', 'none', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(19, 'Editor - TinyMCE', 'tinymce', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 'mode=advanced\nskin=0\ncompressed=0\ncleanup_startup=0\ncleanup_save=2\nentity_encoding=raw\nlang_mode=0\nlang_code=en\ntext_direction=ltr\ncontent_css=1\ncontent_css_custom=\nrelative_urls=1\nnewlines=0\ninvalid_elements=applet\nextended_elements=\ntoolbar=top\ntoolbar_align=left\nhtml_height=550\nhtml_width=750\nelement_path=1\nfonts=1\npaste=1\nsearchreplace=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:%M:%S\ncolors=1\ntable=1\nsmilies=1\nmedia=1\nhr=1\ndirectionality=1\nfullscreen=1\nstyle=1\nlayer=1\nxhtmlxtras=1\nvisualchars=1\nnonbreaking=1\ntemplate=0\nadvimage=1\nadvlink=1\nautosave=1\ncontextmenu=1\ninlinepopups=1\nsafari=1\ncustom_plugin=\ncustom_button=\n\n'),
(20, 'Editor - XStandard Lite 2.0', 'xstandard', 'editors', 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(21, 'Editor Button - Image', 'image', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(22, 'Editor Button - Pagebreak', 'pagebreak', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(23, 'Editor Button - Readmore', 'readmore', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(24, 'XML-RPC - Joomla', 'joomla', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(25, 'XML-RPC - Blogger API', 'blogger', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', 'catid=1\nsectionid=0\n\n'),
(27, 'System - SEF', 'sef', 'system', 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(28, 'System - Debug', 'debug', 'system', 0, 2, 1, 0, 0, 0, '0000-00-00 00:00:00', 'queries=1\nmemory=1\nlangauge=1\n\n'),
(29, 'System - Legacy', 'legacy', 'system', 0, 3, 0, 1, 0, 0, '0000-00-00 00:00:00', 'route=0\n\n'),
(30, 'System - Cache', 'cache', 'system', 0, 4, 0, 1, 0, 0, '0000-00-00 00:00:00', 'browsercache=0\ncachetime=15\n\n'),
(31, 'System - Log', 'log', 'system', 0, 5, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(32, 'System - Remember Me', 'remember', 'system', 0, 6, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(33, 'System - Backlink', 'backlink', 'system', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(34, 'AllVideos (by JoomlaWorks)', 'jw_allvideos', 'content', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'gzipScripts=0\nvfolder=images/stories/videos\nvwidth=400\nvheight=300\ntransparency=transparent\nbackground=#010101\nbackgroundQT=black\ncontrolBarLocation=bottom\nlightboxLink=0\nlightboxWidth=800\nlightboxHeight=600\nafolder=images/stories/audio\nawidth=300\naheight=20\nautoplay=0\ndownloadLink=0\nembedForm=0\ndebugMode=0\n\n'),
(35, 'JA Popup', 'plg_japopup', 'system', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 'group1=fancybox\ngroup1-fancybox-centerOnScroll=1\ngroup1-fancybox-image_scale=1\ngroup1-fancybox-speed_in=500\ngroup1-fancybox-speed_out=500\ngroup1-highslide-outline=drop-shadow\ngroup1-highslide-speed_in=250\ngroup1-highslide-speed_out=250\ngroup1-shadowbox-animateFade=1\ngroup1-shadowbox-animate=1\ngroup1-shadowbox-animSequence=wh\ngroup1-shadowbox-overlayColor=#000\ngroup1-shadowbox-viewportPadding=20\ngroup1-multibox-contentColor=#FFF\ngroup1-multibox-showControls=1\nwidth=600\nheight=400\noverlay=1\noverlay_opacity=0.7\nadd_title=1\nadd_desc=1\nimage_slideshow=one\n'),
(36, 'Content - JA Thumbnail', 'plg_jathumbnail', 'content', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 'content_mode-auto-manual-content_width=200\ncontent_mode-auto-manual-content_height=200\ncontent_mode-auto-manual-crop_image=1\ncontent_mode-auto-manual-content_align=left\nblog_mode-1-blog_leading_width=300\nblog_mode-1-blog_leading_height=300\nblog_mode-1-blog_intro_width=100\nblog_mode-1-blog_intro_height=100\nblog_mode-1-crop_image=1\n'),
(37, 'JA Tabs for Joomla! 1.5', 'ja_tabs', 'content', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 'style=default\n'),
(40, 'JA T3 Framework', 'jat3', 'system', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(39, 'Content - JA Disqus Debate Echo Plugin', 'plg_disqus_debate_echo', 'content', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 'source=both\ndisplay_on_list=1\ndisplay_on_home=1\nmode=automatic\nprovider=intensdebate\ndisplay_comment_count=1\n'),
(41, 'Search - K2', 'k2', 'search', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n'),
(42, 'System - K2', 'k2', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(43, 'User - K2', 'k2', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(44, 'JFirePHP', 'jfirephp', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'enable=1\nverbose=0\nredirectphp=1\nlimittodebug=0\nmaxObjectDepth=10\nmaxArrayDepth=20\nuseNativeJsonEncode=1\nincludeLineNumbers=1\n\n'),
(45, 'Simple Image Gallery PRO (by JoomlaWorks)', 'jw_sigpro', 'content', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 'galleries_rootfolder=images/stories\npopup_engine=jquery_slimbox\nthb_template=Default\nthb_width=200\nthb_height=160\njpg_quality=75\nshowcaptions=1\nenabledownload=1\ncache_expire_time=120\n'),
(46, 'System - Advanced Module Manager', 'advancedmodules', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(47, 'System - NoNumber! Elements', 'nonumberelements', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_data`
--

DROP TABLE IF EXISTS `jos_poll_data`;
CREATE TABLE IF NOT EXISTS `jos_poll_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollid` int(11) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pollid` (`pollid`,`text`(1))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `jos_poll_data`
--

INSERT INTO `jos_poll_data` (`id`, `pollid`, `text`, `hits`) VALUES
(1, 1, '如何预防出生缺陷1', 6),
(2, 1, '如何预防出生缺陷2', 2),
(3, 1, '如何预防出生缺陷3', 1),
(4, 1, '', 0),
(5, 1, '', 0),
(6, 1, '', 0),
(7, 1, '', 0),
(8, 1, '', 0),
(9, 1, '', 0),
(10, 1, '', 0),
(11, 1, '', 0),
(12, 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_date`
--

DROP TABLE IF EXISTS `jos_poll_date`;
CREATE TABLE IF NOT EXISTS `jos_poll_date` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vote_id` int(11) NOT NULL DEFAULT '0',
  `poll_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `jos_poll_date`
--

INSERT INTO `jos_poll_date` (`id`, `date`, `vote_id`, `poll_id`) VALUES
(1, '2011-02-04 12:08:36', 2, 1),
(2, '2011-02-06 16:27:36', 2, 1),
(3, '2011-05-15 07:18:59', 1, 1),
(4, '2011-06-06 07:47:59', 1, 1),
(5, '2011-07-17 09:09:51', 3, 1),
(6, '2011-07-17 10:22:37', 1, 1),
(7, '2011-07-17 10:29:19', 1, 1),
(8, '2011-07-24 06:05:43', 1, 1),
(9, '2011-09-04 13:55:59', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_menu`
--

DROP TABLE IF EXISTS `jos_poll_menu`;
CREATE TABLE IF NOT EXISTS `jos_poll_menu` (
  `pollid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pollid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_poll_menu`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_polls`
--

DROP TABLE IF EXISTS `jos_polls`;
CREATE TABLE IF NOT EXISTS `jos_polls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `voters` int(9) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `lag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_polls`
--

INSERT INTO `jos_polls` (`id`, `title`, `alias`, `voters`, `checked_out`, `checked_out_time`, `published`, `access`, `lag`) VALUES
(1, '您最想了解生殖健康哪方面只是？', '2011-02-04-12-06-53', 9, 62, '2011-07-17 10:08:17', 1, 0, 86400);

-- --------------------------------------------------------

--
-- Table structure for table `jos_sections`
--

DROP TABLE IF EXISTS `jos_sections`;
CREATE TABLE IF NOT EXISTS `jos_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` text NOT NULL,
  `scope` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_scope` (`scope`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jos_sections`
--

INSERT INTO `jos_sections` (`id`, `title`, `name`, `alias`, `image`, `scope`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `ordering`, `access`, `count`, `params`) VALUES
(1, '青春期', '', '2011-02-04-09-59-39', 'articles.jpg', 'content', 'left', '<p>青春期</p>\r\n<p> </p>', 1, 0, '0000-00-00 00:00:00', 1, 0, 3, ''),
(2, '新婚', '', '2011-02-04-09-59-57', '', 'content', 'left', '', 1, 62, '2011-02-04 10:36:22', 2, 0, 0, ''),
(3, '其他', '', '2011-05-01-14-02-07', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 3, 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_session`
--

DROP TABLE IF EXISTS `jos_session`;
CREATE TABLE IF NOT EXISTS `jos_session` (
  `username` varchar(150) DEFAULT '',
  `time` varchar(14) DEFAULT '',
  `session_id` varchar(200) NOT NULL DEFAULT '0',
  `guest` tinyint(4) DEFAULT '1',
  `userid` int(11) DEFAULT '0',
  `usertype` varchar(50) DEFAULT '',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext,
  PRIMARY KEY (`session_id`(64)),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_session`
--

INSERT INTO `jos_session` (`username`, `time`, `session_id`, `guest`, `userid`, `usertype`, `gid`, `client_id`, `data`) VALUES
('', '1317631031', 'f0s5krubq5agn388p1td4efsv1', 1, 0, '', 0, 0, '__default|a:8:{s:15:"session.counter";i:15;s:19:"session.timer.start";i:1317630988;s:18:"session.timer.last";i:1317631031;s:17:"session.timer.now";i:1317631031;s:22:"session.client.browser";s:137:"Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Ubuntu/11.04 Chromium/12.0.742.112 Chrome/12.0.742.112 Safari/534.30";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:71:"/home/kl68884/dev/project/video/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:13:"session.token";s:32:"826add368dc1bbaafec8efabca0a0896";}');

-- --------------------------------------------------------

--
-- Table structure for table `jos_stats_agents`
--

DROP TABLE IF EXISTS `jos_stats_agents`;
CREATE TABLE IF NOT EXISTS `jos_stats_agents` (
  `agent` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_stats_agents`
--


-- --------------------------------------------------------

--
-- Table structure for table `jos_templates_menu`
--

DROP TABLE IF EXISTS `jos_templates_menu`;
CREATE TABLE IF NOT EXISTS `jos_templates_menu` (
  `template` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menuid`,`client_id`,`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_templates_menu`
--

INSERT INTO `jos_templates_menu` (`template`, `menuid`, `client_id`) VALUES
('jisheng_video', 0, 0),
('khepri', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_users`
--

DROP TABLE IF EXISTS `jos_users`;
CREATE TABLE IF NOT EXISTS `jos_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `jos_users`
--

INSERT INTO `jos_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `gid`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES
(62, 'Administrator', 'admin', 'racke1983cn@hotmail.com', 'da24d7ab2c0d0d44b7f638a1fec69990:DYCTTqFTvG6nXzdj99FcW6PMDm85Z9BJ', 'Super Administrator', 0, 1, 25, '2011-02-04 09:43:19', '2011-09-06 13:52:01', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_weblinks`
--

DROP TABLE IF EXISTS `jos_weblinks`;
CREATE TABLE IF NOT EXISTS `jos_weblinks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`,`archived`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jos_weblinks`
--

