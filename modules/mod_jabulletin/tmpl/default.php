<?php
/*
# ------------------------------------------------------------------------
# JA Bulletin module for Joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/ 

defined('_JEXEC') or die('Restricted access'); ?>
<ul class="ja-bullettin<?php echo $params->get('moduleclass_sfx'); ?> clearfix">
<?php foreach ($list as $item) : ?>
	<li>
			<?php 
			$padding = ($params->get( 'show_image'))?"style=\"padding-left:".($params->get('width')+10)."px\"":"";
			if (isset($item->image)) : 
			?>
			<?php if( $item->image ) : ?>
				<a href="<?php echo $item->link; ?>" class="mostread<?php echo $params->get('moduleclass_sfx'); ?>-image">
					<?php echo $item->image; ?>
				</a>
			<?php endif; ?>
			<?php endif; ?>
			<div <?php echo $padding;?>>
			<a href="<?php echo $item->link; ?>" class="mostread<?php echo $params->get('moduleclass_sfx'); ?>"><?php echo $item->text; ?></a>
			<?php if (isset($item->date)) : ?>
			<br /><span><?php echo JHTML::_('date', $item->date, JText::_('DATE_FORMAT_LC4')); ?></span>
			<?php endif; ?>
			</div>

	</li>
<?php endforeach; ?>
</ul>