<?php
/*
# ------------------------------------------------------------------------
# JA Bulletin module for Joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/
// no direct access
defined ( '_JEXEC' ) or die ( 'Restricted access' );

require_once (JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php');
if (! class_exists ( 'modJABulletin' )) {
	class modJABulletin {
		function getMostRead(&$params) {
			global $mainframe;
			
			$db = & JFactory::getDBO ();
			$user = & JFactory::getUser ();
			
			$count = intval ( $params->get ( 'count', 5 ) );
			$catid = trim ( $params->get ( 'catid' ) );
			$secid = trim ( $params->get ( 'secid' ) );
			$show_front = $params->get ( 'show_front', 1 );
			$aid = $user->get ( 'aid', 0 );
			
			$contentConfig = &JComponentHelper::getParams ( 'com_content' );
			$access = ! $contentConfig->get ( 'shownoauth' );
			
			$nullDate = $db->getNullDate ();
			$date = & JFactory::getDate ();
			$now = $date->toMySQL ();
			
			$catid = $params->get ( 'category' );
			if (! is_array ( $catid ) && $catid != '') {
				$catid = preg_split ( '/,/', $catid );
			}
			
			if ($catid) {
				$catCondition = ' AND cc.id IN ("' . implode ( '","', $catid ) . '")';
			} 
			
			if ($secid) {
				$ids = explode( ',', $secid );
				JArrayHelper::toInteger( $ids );
				$secCondition = ' AND (s.id=' . implode( ' OR s.id=', $ids ) . ')';
			} 
			
			//Content Items only
			$query = 'SELECT a.*,' . ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,' . ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug' . ' FROM #__content AS a' . ' LEFT JOIN #__content_frontpage AS f ON f.content_id = a.id' . ' INNER JOIN #__categories AS cc ON cc.id = a.catid' . ' INNER JOIN #__sections AS s ON s.id = a.sectionid' . ' WHERE ( a.state = 1 AND s.id > 0 )' . ' AND ( a.publish_up = ' . $db->Quote ( $nullDate ) . ' OR a.publish_up <= ' . $db->Quote ( $now ) . ' )' . ' AND ( a.publish_down = ' . $db->Quote ( $nullDate ) . ' OR a.publish_down >= ' . $db->Quote ( $now ) . ' )' . ($access ? ' AND a.access <= ' . ( int ) $aid . ' AND cc.access <= ' . ( int ) $aid . ' AND s.access <= ' . ( int ) $aid : '') . ($catid ? $catCondition : '') . ($secid ? $secCondition : '') . ($show_front == '0' ? ' AND f.content_id IS NULL' : '') . ' AND s.published = 1' . ' AND cc.published = 1' . ' ORDER BY a.hits DESC';
			$db->setQuery ( $query, 0, $count );
			$rows = $db->loadObjectList ();
			
			return $rows;
		}
		
		function getLatest(&$params) {
			global $mainframe;
			
			$db = & JFactory::getDBO ();
			$user = & JFactory::getUser ();
			$userId = ( int ) $user->get ( 'id' );
			
			$count = ( int ) $params->get ( 'count', 5 );
			
			$catid = $params->get ( 'category' );
			if (! is_array ( $catid ) && $catid != '') {
				$catid = preg_split ( '/,/', $catid );
			}
			
			
			 $secid		= trim( $params->get('secid') );
			

			$show_front = $params->get ( 'show_front', 1 );
			$aid = $user->get ( 'aid', 0 );
			
			$contentConfig = &JComponentHelper::getParams ( 'com_content' );
			$access = ! $contentConfig->get ( 'shownoauth' );
			
			$nullDate = $db->getNullDate ();
			
			$date = & JFactory::getDate ();
			$now = $date->toMySQL ();
			
			$where = 'a.state = 1' . ' AND ( a.publish_up = ' . $db->Quote ( $nullDate ) . ' OR a.publish_up <= ' . $db->Quote ( $now ) . ' )' . ' AND ( a.publish_down = ' . $db->Quote ( $nullDate ) . ' OR a.publish_down >= ' . $db->Quote ( $now ) . ' )';
			
			// User Filter
			switch ($params->get ( 'user_id' )) {
				case 'by_me' :
					$where .= ' AND (created_by = ' . ( int ) $userId . ' OR modified_by = ' . ( int ) $userId . ')';
					break;
				case 'not_me' :
					$where .= ' AND (created_by <> ' . ( int ) $userId . ' AND modified_by <> ' . ( int ) $userId . ')';
					break;
			}
			
			// Ordering
			switch ($params->get ( 'ordering' )) {
				case 'm_dsc' :
					$ordering = 'a.modified DESC, a.created DESC';
					break;
				case 'c_dsc' :
				default :
					$ordering = 'a.created DESC';
					break;
			}
		
			if ($catid) {
				$catCondition = ' AND cc.id IN ("' . implode ( '","', $catid ) . '")';
			}
			if ($secid) {
				$ids = explode ( ',', $secid );
				JArrayHelper::toInteger ( $ids );
				$secCondition = ' AND (s.id=' . implode ( ' OR s.id=', $ids ) . ')';
			}
			
			// Content Items only
			$query = 'SELECT a.*, ' . ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,' . ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug' . ' FROM #__content AS a' . ($show_front == '0' ? ' LEFT JOIN #__content_frontpage AS f ON f.content_id = a.id' : '') . ' INNER JOIN #__categories AS cc ON cc.id = a.catid' . ' INNER JOIN #__sections AS s ON s.id = a.sectionid' . ' WHERE ' . $where . ' AND s.id > 0' . ($access ? ' AND a.access <= ' . ( int ) $aid . ' AND cc.access <= ' . ( int ) $aid . ' AND s.access <= ' . ( int ) $aid : '') . ($catid ? $catCondition : '') . ($secid ? $secCondition : '') . ($show_front == '0' ? ' AND f.content_id IS NULL ' : '') . ' AND s.published = 1' . ' AND cc.published = 1' . ' ORDER BY ' . $ordering;
			$db->setQuery ( $query, 0, $count );
			$rows = $db->loadObjectList ();
			
			return $rows;
		}
		
		function parseList(&$rows, &$params) {
			$i = 0;
			$showimg = $params->get ( 'show_image', 1 );
			$w = ( int ) $params->get ( 'width', 80 );
			$h = ( int ) $params->get ( 'height', 96 );
			$showdate = $params->get ( 'show_date', 1 );
		
			$thumbnailMode = $params->get( 'thumbnail_mode', 'crop' );
			$aspect 	   = $params->get( 'thumbnail_mode-resize-use_ratio', '1' );
			$crop = $thumbnailMode == 'crop' ? true:false;
			$lists = array ();
			$jaimage = JAImage::getInstance();
				
			foreach ( $rows as $row ) {
				$lists [$i]->link = JRoute::_ ( ContentHelperRoute::getArticleRoute ( $row->slug, $row->catslug, $row->sectionid ) );
				$lists [$i]->text = htmlspecialchars ( $row->title );
				if ($showdate) {
					$lists [$i]->date = strtotime ( $row->modified ) ? $row->created : $row->modified;
				}
				if ($showimg) {
					$imageSource = $jaimage->parseImage( $row->introtext . $row->fulltext );
					if ( $imageSource ) { 
						if( $thumbnailMode != 'none' ) {
							$imageURL = $jaimage->resize( $imageSource, $w, $h, $crop, $aspect );
							if( $imageURL ){
								if ( $imageURL == $imageSource ) {
									$width = $w ? "width=\"$w\"" : "";
									$height = $h ? "height=\"$h\"" : "";
									$lists [$i]->image = "<img src=\"$imageURL\" alt=\"{$lists[$i]->text}\" title=\"{$lists[$i]->text}\" $width $height />";
								} else {
									$lists [$i]->image = "<img src=\"$imageURL\" alt=\"{$lists[$i]->text}\" title=\"{$lists[$i]->text}\" />";
								}
							} else {
								$lists [$i]->image = '';
							}
						} else {
							$width = $w ? "width=\"$w\"" : "";
							$height = $h ? "height=\"$h\"" : "";
							$lists [$i]->image = "<img src=\"$imageSource\" alt=\"{$lists[$i]->text}\" title=\"{$lists[$i]->text}\" $width $height />";	
						}	
					}
				}
				$i ++;
			}
			return $lists;
		}
		
		function getList(&$params) {
			$type = $params->get ( 'type', 'latest' );
			switch ($type) {
				case 'latest' :
					$rows = $this->getLatest ( $params );
					break;
				case 'mostread' :
					$rows = $this->getMostRead ( $params );
					break;
			}
			if (isset ( $rows ))
				return $this->parseList ( $rows, $params );
			return null;
		}
		
		function addStyleFiles() {
			global $mainframe;
			$filename = 'ja.bulletin.css';
			$tplpath = DS . 'templates' . DS . $mainframe->getTemplate () . DS . 'css' . DS;
			$tplurl = '/templates/' . $mainframe->getTemplate () . '/css/';
			$modurl = '/modules/mod_jabulletin/tmpl/';
			$cssurl = $tplurl;
			if (! file_exists ( JPATH_SITE . $tplpath . $filename )) {
				$cssurl = $modurl;
			}
			$cssurl = JURI::base () . $cssurl;
			?>
			<script type="text/javascript">
				//<![CDATA[ 
					var links = document.getElementsByTagName ('link');
					var included = false;
					if (links.length) {
						for (var i=0;i<links.length;i++) {
							if (links[i].getAttribute('href').test('ja.bulletin.css')) {
								included = true;
								break;
							}
						}
					}
					if (!included) {
						var script = document.createElement('link');
						script.setAttribute('type', 'text/css');
						script.setAttribute('rel', 'stylesheet');
						script.setAttribute('href', '<?php
			echo $cssurl . $filename;
			?>');
						document.getElementsByTagName("head")[0].appendChild(script);
					}
				//]]>
				</script>
<?php
		}
	}
}
