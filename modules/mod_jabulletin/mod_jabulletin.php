<?php
/*
# ------------------------------------------------------------------------
# JA Bulletin module for Joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/ 
// no direct access
defined('_JEXEC') or die('Restricted access');

if (!defined ('_MODE_JABULLETIN_ASSETS_')) {
	define ('_MODE_JABULLETIN_ASSETS_', 1);
	JHTML::stylesheet('style.css','modules/'.$module->module.'/assets/');
	if (is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'css'.DS.$module->module.".css"))
	JHTML::stylesheet($module->module.".css",'templates/'.$mainframe->getTemplate().'/css/');
}

// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');
require_once (dirname(__FILE__).DS.'jaimage.php');
$helper = new modJABulletin();

// if enable caching data
$list = $helper->getList($params);
require(JModuleHelper::getLayoutPath('mod_jabulletin'));