<?php
/*
# ------------------------------------------------------------------------
# Module JA Facebooklikebox for J15
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/

// no direct access
defined('_JEXEC') or die('Restricted accessd');

global $mainframe;

//123144964369587 is Joomlart's Page ID
$aParams['id'] 						= 	$params->get( 'id', '123144964369587' );
$aParams['width'] 					= 	$params->get( 'width', 300 );
$aParams['height'] 					= 	$params->get( 'height', 400 );
$aParams['connections'] 			= 	$params->get( 'connections', '10' );
$aParams['stream'] 					= 	($params->get( 'stream', 1 )) ? 'true' : 'false';
$aParams['header'] 					= 	($params->get( 'header', 1 )) ? 'true' : 'false';
$aParams['colorscheme'] 			= 	($params->get( 'colorscheme', 1)) ? 'light' : 'dark';
$aParams['border_color'] 		= 	$params->get( 'border_color', '' );

$sFacebookQuery = '';
foreach ($aParams as $key => $value) {
	$sFacebookQuery .= "{$key}={$value}&amp;";
}

require( JModuleHelper::getLayoutPath('mod_jafacebooklikebox') );
?>