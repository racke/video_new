<?php 
/*
# ------------------------------------------------------------------------
# JA News Ticker module for Joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/
/**
 * JA News Sticker module allows display of article's title from sections or categories. \
 * You can configure the setttings in the right pane. Mutiple options for animations are also added, choose any one.
 * If you are using this module on Teline III template, * then the default module position is "headlines".
 **/
  // no direct access
defined('_JEXEC') or die('Restricted access');
?>
<script type="text/javascript">
/* <![CDATA[ */
	//$(window).addEvent('domready', function(){
		// options setting
		var options = { box:$('<?php echo $moduleID; ?>'), 
						items: $$('#<?php echo $moduleID; ?> .ja-headlines-item'), 
						mode: '<?php echo $animationType ;?>', 
						wrapper:$('jahl-wapper-items-<?php echo $moduleID; ?>'),
						buttons:{next: $$('.ja-headelines-next'), previous: $$('.ja-headelines-pre')},
						interval:<?php echo (int)$params->get('animation_interval', 3000);?>,
						fxOptions : { duration: <?php echo $params->get('animation_speed', 500);?>,
									  transition: <?php echo $params->get('animation_transition', 'Fx.Transitions.linear'); ?> ,
									  wait: false }	};
 
		var jahl = new JANewSticker( options );						  
	// });
/* ]]> */	
</script>