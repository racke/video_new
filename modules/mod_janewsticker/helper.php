<?php 
/*
# ------------------------------------------------------------------------
# JA News Ticker module for Joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/
/**
 * JA News Sticker module allows display of article's title from sections or categories. \
 * You can configure the setttings in the right pane. Mutiple options for animations are also added, choose any one.
 * If you are using this module on Teline III template, * then the default module position is "headlines".
 **/
  // no direct access
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE . '/components/com_content/helpers/route.php');
if(file_exists(JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php'))
{
	require_once(JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php');
}
if(file_exists(JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'utilities.php'))
{
	require_once(JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'utilities.php');
}
/**
 * modjanewstickerHelper class.
 */
class modjanewstickerHelper {
	
	/**
	 * get listing items from rss link or from list of categories.
	 *
	 * @param JParameter $params
	 * @return array
	 */
	function getList( $params ){
		$rows = array();
		$using_mode = strtolower($params->get('using_mode',"catids"));
		$exclude_arr = array("rss","external");
		if( !in_array($using_mode,$exclude_arr)) {
			// check cache was endable ?
			if ( $params->get('enable_cache') ) {
				$cache =& JFactory::getCache();
				$cache->setCaching( true );
				$cache->setLifeTime( $params->get( 'cache_time', 30 ) * 60 );	
				$rows = $cache->get( array( (new modjanewstickerHelper() ) , 'getArticles' ), array( $params ) ); 
			} else {
				$rows = modjanewstickerHelper::getArticles( $params );
			}					
		}
		elseif( $using_mode == 'external')
		{
			$rows = modjanewstickerHelper::parseExternalLinks( $params->get("external_link","") );
		}
		else {
			$rows  = modjanewstickerHelper::dataFromRSS( $params );
			
		}
		return $rows;
	}
	/**
	 *get list external links
	 *@param string content 
	 *@return array list of external links
	 */
	function parseExternalLinks( $content ) {
		
		$regex = '#\[link ([^\]]*)\]([^\[]*)\[/link\]#m';
		preg_match_all ($regex, $content, $matches, PREG_SET_ORDER);
		
		$linkArray = array();
		if(!empty($matches))
		{
			foreach ($matches as $match) {
			$params = modjanewstickerHelper::parseParams( $match[1] );
			if (is_array($params)) {
				$url = isset($params['url'])?trim($params['url']):'';
				$title = isset($params['title'])?trim($params['title']):'janewsticker';
				$obj = new stdClass();
				$obj->title = $title;
				$obj->link = $url;
				$obj->introtext = str_replace("\n","<br />",trim($match[2]));
				$linkArray[] = $obj;
				}
			}
		}
		return $linkArray;
	}
	/**
	 * get articles from list of categories and follow up owner paramer.
	 *
	 * @param JParameter $params.
	 * @return array list of articles
	 */	
	function getArticles( $params ){
	
		global $mainframe; 
		
	 	$db	    = &JFactory::getDBO();
		
		$my = &JFactory::getUser();

		$aid	= $my->get( 'aid', 0 );
		$date =& JFactory::getDate();
		$now  = $date->toMySQL();
		if(strtolower($params->get('using_mode',"catids")) == 'com_k2')
		{
			$query = "SELECT a.*, cc.name AS categoryname,cc.id AS categoryid, cc.alias AS categoryalias, cc.params AS categoryparams";
			$query .= " FROM #__k2_items as a LEFT JOIN #__k2_categories cc ON cc.id = a.catid";
			$query .= " WHERE a.published = 1 AND a.access <= {$aid} AND a.trash = 0 AND cc.published = 1 AND cc.access <= {$aid} AND cc.trash = 0";
			$query .= " AND ( a.publish_up = ".$db->Quote( $db->getNullDate() )." OR a.publish_up <= ".$db->Quote($now)." )";
			$query .= " AND ( a.publish_down = ".$db->Quote( $db->getNullDate() )." OR a.publish_down >= ".$db->Quote($now)." )";
			
			if(is_array($params->get('catids'))){
				$catids = implode(',', $params->get('catids'));
				$query .= " AND cc.id IN($catids)";
			}
			elseif($params->get('k2catsid')>0){
				$catids = $params->get('k2catsid');
				$query .= " AND cc.id = $catids";
			}
		}
		else
		{
			$query 	= 	'SELECT a.*,cc.description as catdesc, cc.title as cattitle,s.description as secdesc, s.title as sectitle,' .
						' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,'.
						' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":",cc.id,cc.alias) ELSE cc.id END as catslug,'.
						' CASE WHEN CHAR_LENGTH(s.alias) THEN CONCAT_WS(":", s.id, s.alias) ELSE s.id END as secslug'
						. "\n FROM #__content AS a".
						' INNER JOIN #__categories AS cc ON cc.id = a.catid' .
						' INNER JOIN #__sections AS s ON s.id = a.sectionid'
						. "\n WHERE a.state = 1"
						. "\n AND ( a.publish_up = " . $db->Quote( $db->getNullDate() ) . " OR a.publish_up <= " . $db->Quote( $now  ) . " )"
						. "\n AND ( a.publish_down = " . $db->Quote( $db->getNullDate() ) . " OR a.publish_down >= " . $db->Quote( $now  ) . " )"
						. ( ( !$mainframe->getCfg( 'shownoauth' ) ) ? "\n AND a.access <= " . (int) $aid : '' )
						;

			if(is_array($params->get('catids'))){
				$catids = implode(',', $params->get('catids'));
				$query .= " AND cc.id IN($catids)";
			}
			elseif($params->get('catids')>0){
				$catids = $params->get('catids');
				$query .= " AND cc.id = $catids";
			}
		}
		$query .= ' ORDER BY ' . 'a.'.$params->get('sort_order_field' ,'created') . ' '.$params->get('sort_order','DESC');
		
		if($params->get('max_items', 5)){
			$query .=  ' LIMIT ' . $params->get('max_items', 5);
		}
		$db->setQuery($query);
		
		return $db->loadObjectlist();
	}
	
	/**
	 * get  list of items from rss list and process caching.
	 *
	 * @param JParameter $params.
	 * @return array list of articles
	 */
	function dataFromRSS( $params ){
		$data = array();
		if( trim($params->get('rss_link')) == '' ) {
			return $data;
		}
		$rssUrl =  $params->get('rss_link');

		//  get RSS parsed object
		$options = array();
		$options['rssUrl'] 		= $rssUrl;
		if ( $params->get('enable_cache') ) {
			$options['cache_time']  = $params->get( 'cache_time' , '30' ) ;
			$options['cache_time']	*= 60;
		} else {
			$options['cache_time'] = null;
		}

		$rssDoc =& JFactory::getXMLparser( 'RSS', $options );

		if ($rssDoc != false) {
			$items = $rssDoc->get_items();
			if( $items != null ){
				$tmp = array();
				foreach( $items as $item ){ 
					$obj = new stdClass();
					$obj->title = $item->get_title();
					$obj->link = $item->get_link();
					$obj->introtext = $item->get_description();
					$tmp[] = $obj;
				}
				$data = $tmp;
			}
		}
		return $data;	
	}
	
	/**
	 * trim string with max specify
	 *  
	 * @param string $title
	 * @param integer $max.
	 */
	function trimString( $title, $max=60  ){
		if( strlen($title) > $max ){
			return substr( $title, 0, $max ) . '...';
		} 
		return $title;
	}
	
	/**
	 * get description of item: trim string with max chars.
	 *
	 * @param string $introtext
	 * @param string $separator
	 * @param string $descMaxChars
	 * @return string
	 */
	function getDescription( $introtext, $separator, $descMaxChars=60 ){
		return $separator . ' ' . modJAHeadLinesHelper::trimString( strip_tags($introtext), $descMaxChars );
	}
	
	/**
	 * detect and get link with each resource
	 *
	 * @param string $item
	 * @param bool $useRSS.
	 * @return string.
	 */
	function getLink( $item, $useRSS=false ){
		if($useRSS){			
			return $item->link;
		} else {
		    if(!isset($item->slug) && !isset($item->catslug) && !isset($item->sectionid) && isset($item->categoryalias))
			{
				return	urldecode(JRoute::_(K2HelperRoute::getItemRoute($item->id.':'.urlencode($item->alias), $item->catid.':'.urlencode($item->categoryalias))));
			}
			else
			{
				return  JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug, $item->sectionid));
			}
		}
	}
	
	/**
	 * get size follow up animation type.
	 *
	 * @param JPramater $params
	 * @return integer.
	 */
	function getSize( $params ) {
		$mode = $params->get('animation_type');
		
		if( $mode == 'vertical'){
			return $params->get('height', 30);
		}	
		return $params->get('width', 500);
	}
	/**
	 * get parameters from configuration string.
	 *
	 * @param string $string;
	 * @return array.
	 */
	function parseParams( $string ) {
		$string = html_entity_decode($string, ENT_QUOTES);
		$regex = "/\s*([^=\s]+)\s*=\s*('([^']*)'|\"([^\"]*)\"|([^\s]*))/";
		 $params = null;
		 if(preg_match_all($regex, $string, $matches) ){
				for ($i=0;$i<count($matches[1]);$i++){ 
				  $key 	 = $matches[1][$i];
				  $value = $matches[3][$i]?$matches[3][$i]:($matches[4][$i]?$matches[4][$i]:$matches[5][$i]);
				  $params[$key] = $value;
				}
		  }
		  return $params;
	}
}
?>