
var JANEWSPRO_SLIDECATS = new Class({
	initialize: function(options) {
		
		this.options = $extend({
			panelid: '',
			moduleid: 0,
			duration: 400,
			animType: 'animMoveHor',
			changeTransition:	Fx.Transitions.Pow.easeIn
		}, options || {});
		
		panel = $(this.options.panelid);
		if(panel==null) return;				
		
		this.panelwrap = panel.getElement('div.ja-cats-slide-boxs');				
		this.panelwrap.setStyle('height', this.panelwrap.getElement('div.box-active').offsetHeight);						
		
		this.panels = this.panelwrap.getElements('div.ja-cats-slide-box');				
		this.cur_activePanel = this.panelwrap.getElement('div.box-active');
		
		this.next_bt = panel.getElement('.ja-newspro-control a.next');
		this.prev_bt = panel.getElement('.ja-newspro-control a.prev');
				
		if(this.next_bt!=null){
			this.next_bt.addEvent('click', function () {
		    	this.next();
		    }.bind(this));
		    
		    this.prev_bt.addEvent('click', function () {
		    	this.previous();
		    }.bind(this));
		    
		    this.anim = eval ('new '+this.options.animType + '(this)');
		}
		    
	},
	
    next: function(){      	
		this.cur_activePanel = this.panelwrap.getElement('div.box-active');
		
		if(this.cur_activePanel.getNext()==null) return;
		
		this.cur_activePanel.removeClass('box-active');
		this.activePanel = this.cur_activePanel.getNext();
		this.activePanel.addClass('box-active');		
		
    	this.anim.move (this.cur_activePanel, this.activePanel, false);  
    	
    	if (!this.mainfx) this.mainfx = new Fx.Style(this.panelwrap, 'height',{duration:this.options.duration});
		this.mainfx.stop();
		this.mainfx.start(this.activePanel.offsetHeight);

    },
    
    previous: function(){
    	this.cur_activePanel = this.panelwrap.getElement('div.box-active'); 	
    	var activePanel = this.cur_activePanel.getPrevious();
    	if(activePanel!=null){
    		this.activePanel = activePanel;
    		this.cur_activePanel.removeClass('box-active');
    		this.activePanel.addClass('box-active');
    		this.anim.move (this.cur_activePanel, this.activePanel, false);
    		if (!this.mainfx) this.mainfx = new Fx.Style(this.panelwrap, 'height',{duration:this.options.duration});
    		this.mainfx.stop();
    		this.mainfx.start(this.activePanel.offsetHeight);
    	}    	
    }
	
});


var animFade = new Class ({
	initialize: function(tabwrap) {
		this.options = tabwrap.options || {};
		this.tabwrap = tabwrap;
		this.changeEffect = new Fx.Elements(this.tabwrap.panels, {duration: this.options.duration});
		this.tabwrap.panels.setStyles({'opacity':0,'width':'100%'});
	},

	move: function (curTab, newTab, skipAnim) {
		if(this.options.changeTransition != 'none' && !skipAnim)
		{
			if (curTab)
			{
				curOpac = curTab.getStyle('opacity');
				var changeEffect = new Fx.Style(curTab, 'opacity', {duration: this.options.duration, transition: this.options.changeTransition});
				changeEffect.stop();
				changeEffect.start(curOpac,0);
			}
			curOpac = newTab.getStyle('opacity');
			var changeEffect = new Fx.Style(newTab, 'opacity', {duration: this.options.duration, transition: this.options.changeTransition});
			changeEffect.stop();
			changeEffect.start(curOpac,1);
		} else {
			if (curTab) curTab.setStyle('opacity', 0);
			newTab.setStyle('opacity', 1);
		}
	},
	reposition: function() {
	    if (this.tabwrap.activePanel) {
			this.changeEffect.stop();

			for (var i=this.tabwrap.activePanel._idx-1;i>=0;i--) {
			    this.tabwrap.panels[i].setStyle('opacity',0);
			}
		    for (i=this.tabwrap.activePanel._idx+1;i<this.tabwrap.panels.length;i++) {
		       this.tabwrap.panels[i].setStyle('opacity',0);
		     }		     		   
	    }
	}
});

var animMoveHor = new Class ({
	initialize: function(tabwrap) {
		this.options = tabwrap.options || {};
		this.tabwrap = tabwrap;
		this.changeEffect = new Fx.Elements(this.tabwrap.panels, {duration: this.options.duration});
	    var left = 0;
	    this.tabwrap.panels.each (function (panel) {
	      panel.setStyle('left', left);
	      left += panel.offsetWidth;
	    });
	    this.tabwrap.panels.setStyle('top', 0);
	},

	move: function (curTab, newTab, skipAnim) {
		if(this.options.changeTransition != 'none' && !skipAnim)
		{
			//this.changeEffect.stop();
			var obj = {};
			var offset = newTab.offsetLeft.toInt();
			i=0;			
			
			this.tabwrap.panels.each(function(panel) {
				obj[i++] = {'left':[panel.offsetLeft.toInt(), panel.offsetLeft.toInt() - offset] };			
			});
			
			this.changeEffect.start(obj);
		}
	},
	reposition: function() {
	    if (this.tabwrap.activePanel) {
			//this.changeEffect.stop();
	       	var left = this.tabwrap.activePanel.offsetLeft;
		    for (var i=this.tabwrap.activePanel._idx-1;i>=0;i--) {
		       left -= this.tabwrap.panels[i].offsetWidth;
		       this.tabwrap.panels[i].setStyle('left',left);
		     }
	       	var left = this.tabwrap.activePanel.offsetLeft;
		    for (i=this.tabwrap.activePanel._idx+1;i<this.tabwrap.panels.length;i++) {
		       left += this.tabwrap.panels[i-1].offsetWidth;
		       this.tabwrap.panels[i].setStyle('left',left);
		     }
	    }
	}
});

var animMoveVir = new Class ({
	initialize: function(tabwrap) {
		this.options = tabwrap.options || {};
		this.tabwrap = tabwrap;
		this.changeEffect = new Fx.Elements(this.tabwrap.panels, {duration: this.options.duration});
	
	    var top = 0;
	    this.tabwrap.panels.each (function (panel) {
	      panel.setStyle('top', top);     
	      top += Math.max(panel.offsetHeight,  panel.getParent().offsetHeight);
	    });
	    this.tabwrap.panels.setStyle('left', 0);
	},
	move: function (curTab, newTab, skipAnim) {
		if(skipAnim==false)
		{
      //reposition newTab
      
			this.changeEffect.stop();
			var obj = {}; 
			var offset = newTab.offsetTop.toInt();
			i=0;
			this.tabwrap.panels.each(function(panel) {
				obj[i++] = {'top':[panel.offsetTop.toInt(), panel.offsetTop.toInt() - offset]};			
			});
			this.changeEffect.start(obj);
		}
	},
	reposition: function() {
	    if (this.tabwrap.activePanel) {
				 this.changeEffect.stop();
	       var top = this.tabwrap.activePanel.offsetTop;
		     for (var i=this.tabwrap.activePanel._idx-1;i>=0;i--) {
		       top -= this.tabwrap.panels[i].offsetHeight;
		       this.tabwrap.panels[i].setStyle('top',top);
		     }
	       var top = this.tabwrap.activePanel.offsetTop;
		     for (i=this.tabwrap.activePanel._idx+1;i<this.tabwrap.panels.length;i++) {
			top += this.tabwrap.panels[i-1].offsetHeight;
		     	this.tabwrap.panels[i].setStyle('top',top);
		     }
	    }
	}
});
