<?php
/*
# ------------------------------------------------------------------------
# Ja NewsPro
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/
$db  = JFactory::getDBO();
$showcreator			= 	$helper->get( 'showcreator', 0 );
$showdate 				= 	$helper->get( 'showdate', 0 );
$maxchars 				= 	intval (trim( $helper->get( 'maxchars', 200 ) ));
$showreadmore 			= 	intval (trim( $helper->get( 'showreadmore', 1 ) ));
$showsubcattitle 		= 	trim( $helper->get( 'showsubcattitle' ));
$showcattitle 			= 	trim( $helper->get( 'showcattitle' ));
$groupbysubcat			=	intval (trim( $helper->get( 'groupbysubcat', 0 ) ));
$showusersetting		=	intval (trim( $helper->get( 'showusersetting', 0 ) ));
$maxsubCats 				=   intval (trim( $helper->get('numberCats', 3) ));

$animationType			= 	trim( $helper->get( 'animType', 'animMoveVir' ) );

$moduleid = $module->id;

/* Cols by Parent group */
$cols 	= intval (trim( $helper->get( 'cols', 1 ) ));
if(!$cols) $cols = 1;
$width = 99.9/$cols;

/* Cols by sub categorogy */
$subcols 	= intval (trim( $helper->get( 'subcols', 1 ) ));
if(!$subcols) $subcols = 1;
$subwidth = 99.9/$subcols;
$t = 0;
$count_sections = count($helper->_section);
?>
<?php 
if (!defined ('_MODE_JANEWPRO_ASSETS_SLIDE_CATS')) {
	define ('_MODE_JANEWPRO_ASSETS_SLIDE_CATS', 1);
	JHTML::script('script.js','modules/'.$module->module.'/tmpl/'.$theme.'/');
}
?>
<script type="text/javascript">
	var janewspro = null;
	var jasiteurl = '<?php echo JURI::root()?>';
	window.addEvent('load', function(){
		janewspro = new JANEWSPRO(); 
		new Tips($('ja-zinwrap-<?php echo $module->id?>').getElements('.jahasTip'), { maxTitleChars: 50, fixed: false, className: 'tool-tip janews-tool'});
	})
</script>

<div class="ja-zinwrap <?php echo $theme?>" id="ja-zinwrap-<?php echo $module->id?>">
	<div class="ja-zin clearfix">	   
	<?php if($count_sections){?>
		<?php 
		$col = 0;
		for ($z = 0; $z < $cols; $z ++) {
			$cls = $cols==1?'full':($z==0?'left':($z==$cols-1?'right':'center'));
			$k = $z;
			$pos = $z==0?'col-first':($z == $cols-1?'col-last':'');
			$col++;
			?>
			
			<div class="items-row" style="width: <?php echo $width?>%">							
			<?php 
			$j = 0;
			
			foreach ($helper->_section as $secid=>$section){
				
				$params_new = new JParameter('');
				$catid = $secid;				
				$cooki_name = 'mod'.$moduleid.'_'.$catid;
				if(isset($_COOKIE[$cooki_name]) && $_COOKIE[$cooki_name]!=''){
					$cookie_user_setting = $_COOKIE[$cooki_name];
					$arr_values = explode('&', $cookie_user_setting);
					if($arr_values){
						foreach ($arr_values as $row){
							list($k, $value) = explode('=', $row);
							if($k!=''){
								$params_new->set($k, $value);
							}
						}
					}
				}
		 					
				$introitems 	= 	intval (trim( $params_new->get( 'introitems', $helper->get( 'introitems', 1 ) )));
				$linkitems 		= 	intval (trim( $params_new->get( 'linkitems', $helper->get( 'linkitems', 0 ) ) ));
				$showimage		= 	intval (trim( $params_new->get( 'showimage', $helper->get( 'showimage', 1 ) ) ));				
							
				?>
			
				<?php $pos = $t==0?'row-first':($t == count($count_sections)-1?'row-last':'');?>
			
				
					<div class="ja-zinsec clearfix">
						
				    	<?php if($showcattitle){?>
				    	<h2>
							<a href="<?php echo $helper->cat_link[$secid];?>" title="<?php echo trim(strip_tags($helper->cat_desc[$secid]));?>">
								<span><?php echo $helper->cat_title[$secid];?></span>
							</a>
						</h2>
						<?php }?>
						
						<!-- User setting form -->
						<?php
						if($helper->get('showusersetting', 0)){
				    		$path = JModuleHelper::getLayoutPath( 'mod_janewspro', $helper->get('themes', 'default').DS.'blog_usersetting' );
							if (file_exists($path)) {
			                    require($path);
			                }
						}
		                ?>
						<!-- End -->
									
						<?php if(!$groupbysubcat){?>
							<div class="jazin-full" style="width: 100%">
								<?php
						    		$rows = $helper->articles[$secid];
						    		$path = JModuleHelper::getLayoutPath( 'mod_janewspro', $helper->get('themes', 'default').DS.'blog_item' );
									if (file_exists($path)) {
					                    require($path);
					                }
				                ?>
			                </div>  
			                
						<?php }else{?> <!-- Group by sub category -->
							<div id="ja-cats-slide-mainwrap-<?php echo $moduleid?>-<?php echo $secid?>">
																
								<?php 
								$next = 0;
								$sk = 0;
								$number_cats = count($helper->_categories[$secid]);
								if($maxsubCats<=0) $maxsubCats = $number_cats;
								$sub_number_cats = $number_cats - $maxsubCats;
								if(!$sub_number_cats) $sub_number_cats = 1;
								
								$cat_cols = $number_cats/$maxsubCats;
								?>
								<?php if($cat_cols>1){?>
								<!-- Control Buttons -->
								<div class="ja-newspro-control">
									<a href="javascript:void(0)" class="prev">
										<span><?php echo JText::_('Prev')?></span>
									</a> 
									| 
									<a href="javascript:void(0)" class="next">
										<span><?php echo JText::_('Next')?></span>					
									</a>
								</div>
								<?php }?>
								
								<div class="ja-cats-slide-boxs">
									
									<!-- Slide Box -->
									<?php for ($sc = 0; $sc < $cat_cols; $sc ++) {?>										
							    		<div class="ja-cats-slide-box <?php if($sc==0) echo 'box-active'?>">
							    			
								    		<?php
								    		
								    		$subcol = 0;						    								    		
											for ($sz = 0; $sz < $subcols; $sz ++) {
												$cls = $subcols==1?'full':($sz==0?'left':($sz==$subcols-1?'right':'center'));
												$sk = $sz;
												$spos = $sz==0?'col-first':($sz == $subcols-1?'col-last':'');
												$subcol++;
												?>
											
												<div class="item article-column <?php echo "column$subcol $spos";?>" style="width: <?php echo $subwidth?>%">
													<?php 
													for ($y = 0; $y < ($maxsubCats / $subcols) && $sk<($maxsubCats); $y ++) {
														if(!isset($helper->_categories[$secid][$sk+$next])) break;
														$cat = $helper->_categories[$secid][$sk+$next];
														if(!isset($helper->articles[$secid][$cat->id])) $helper->articles[$secid][$cat->id] = array();
														$rows = $helper->articles[$secid][$cat->id];
														$path = JModuleHelper::getLayoutPath( 'mod_janewspro', $helper->get('themes', 'default').DS.'blog_item' );
														if (file_exists($path)) {
															require($path);
														}
														$sk += $subcols;
													} ?>
												</div>
												
											<?php }?>
											<?php $next += $maxsubCats;?>
										</div>						    								    		
							    	<?php } ?>
						    	</div>
						    	
					    	</div>
					    	<script type="text/javascript">
								window.addEvent('load', function(){
									new JANEWSPRO_SLIDECATS({'panelid': 'ja-cats-slide-mainwrap-<?php echo $moduleid?>-<?php echo $secid?>', 'duration': <?php echo (int)$helper->get('duration', 400)?>, 'animType':'<?php echo $animationType?>'});		
								});
							</script>
				    	<?php }?>
				    </div>
					
					
				
				<?php 
				$k += $cols;
				$t++;
				$j++;
				unset($helper->_section[$secid]);
				if($j >= ($count_sections / $cols) && $k>=$count_sections){
					break;
				}
				?>
			<?php }?>
			</div>
			
		<?php }?>
		
	<?php }?>
	</div>
</div>
<script type="text/javascript">
	window.addEvent('load', function(){
		janewspro._bindingAndprocessingEventForm($('ja-zinwrap-<?php echo $module->id?>'));
	});
</script>