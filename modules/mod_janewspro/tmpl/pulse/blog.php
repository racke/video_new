<?php
/*
# ------------------------------------------------------------------------
# Ja NewsPro
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
# Author: JoomlArt.com
# Websites: http://www.joomlart.com - http://www.joomlancers.com.
# ------------------------------------------------------------------------
*/
$db  = JFactory::getDBO();
$moduleid = $module->id;
$showcattitle 			= 	trim( $helper->get( 'showcattitle' ));
$showusersetting		=	intval (trim( $helper->get( 'showusersetting', 0 ) ));
$groupbysubcat			=	intval (trim( $helper->get( 'groupbysubcat', 0 ) ));
/* Cols by Parent group */
$cols 	= intval (trim( $helper->get( 'cols', 1 ) ));
if(!$cols) $cols = 1;
$width = 99.9/$cols;

$t = 0;
$count_sections = count($helper->_section);
?>

<div class="ja-zinwrap <?php echo $theme?>" id="ja-zinwrap-<?php echo $module->id?>">
	<div class="ja-zin clearfix">	   
	<?php if($count_sections){?>
		<?php 
		for ($z = 0; $z < $cols; $z ++) {
			$k = $z;
			?>
			
			<div class="items-row" style="width: <?php echo $width?>%">							
			<?php 
			$j = 0;
			
			foreach ($helper->_section as $secid=>$section){?>		
				<?php 
				
				$params_new = new JParameter('');
				$catid = $secid;
				
				$cooki_name = 'mod'.$moduleid.'_'.$catid;
				if(isset($_COOKIE[$cooki_name]) && $_COOKIE[$cooki_name]!=''){
					$cookie_user_setting = $_COOKIE[$cooki_name];
					$arr_values = explode('&', $cookie_user_setting);
					if($arr_values){
						foreach ($arr_values as $row){
							list($k, $value) = explode('=', $row);
							if($k!=''){
								$params_new->set($k, $value);
							}
						}
					}
				}
								
				//$introitems 	= 	intval (trim( $params_new->get( 'introitems', $helper->get( 'introitems', 1 ) )));
				//$linkitems 		= 	intval (trim( $params_new->get( 'linkitems', $helper->get( 'linkitems', 0 ) ) ));
				$showimage		= 	intval (trim( $params_new->get( 'showimage', $helper->get( 'showimage', 1 ) ) ));	
				?>					
			
				<div class="ja-zinsec clearfix">
					
					<?php if($showcattitle){?>
			    	<h2>
						<a href="<?php echo $helper->cat_link[$secid];?>" title="<?php echo trim(strip_tags($helper->cat_desc[$secid]));?>">
							<span><?php echo $helper->cat_title[$secid];?></span>
						</a>
					</h2>
					<?php }?>
						
					<?php if(!$groupbysubcat){?>
						<div class="jazin-full" style="width: 100%">
							<?php
					    		$rows = $helper->articles[$secid];
					    		$path = JModuleHelper::getLayoutPath( 'mod_janewspro', $helper->get('themes', 'default').DS.'blog_item' );
								if (file_exists($path)) {
				                    require($path);
				                }
			                ?>
		                </div>  
		                
					<?php }else{?> <!-- Group by sub category -->
					
			    		<?php foreach ( $helper->_categories[$secid] as $cat):?>
			    			<div class="jazin-full" style="width: 100%">
			    			<?php 
			    			if(!isset($helper->articles[$secid][$cat->id])) $helper->articles[$secid][$cat->id] = array();
							$rows = $helper->articles[$secid][$cat->id];
							$path = JModuleHelper::getLayoutPath( 'mod_janewspro', $helper->get('themes', 'default').DS.'blog_item' );
							if (file_exists($path)) {
								require($path);
							}
			    			?>
			    			</div>
			    		<?php endforeach;?>
			    		
			    		
			    	<?php }?>
			    </div>
				
				
				<?php 
				$k += $cols;
				$t++;
				$j++;
				unset($helper->_section[$secid]);
				if($j >= ($count_sections / $cols) && $k>=$count_sections){
					break;
				}
				?>
			<?php }?>
			</div>
			
		<?php }?>
		
	<?php }?>
	</div>
</div>