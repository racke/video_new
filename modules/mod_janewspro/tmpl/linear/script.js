
var JANEWSPRO_LINEAR = new Class({
	initialize: function(options) {
		
		this.options = $extend({
			moduleid: '',
			secid: '',
			duration: 400,
			changeTransition:	Fx.Transitions.Pow.easeIn
		}, options || {});
		
		this.panel = $('ja-cats-slide-mainwrap-'+this.options.moduleid+'-'+this.options.secid);
		this.panelArticles = $('ja-articles-mainwrap-'+this.options.moduleid+'-'+this.options.secid);
		
		if(this.panel==null) return;				
		this._W = this.panel.offsetWidth;
		
		this.panelwrap = this.panel.getElement('ul.subcats-selection');		
		
		var width = 0;
		this.panelArticles.setStyle('height', this.panelArticles.getElement('div.ja-articles').getFirst().offsetHeight);				
		
		this.panelwrap.getChildren().each(function (el){
		    width += el.offsetWidth;		   
		    
		    el.getElement('a.subcat-title').addEvent('click', function(){
		    	var catid = el.getElement('a.subcat-title').getProperty('rel');
		    	if(!catid) return true;
		    	
		    	this.panelwrap.getChildren().removeClass('active');
		    	this.panelwrap.getElements('.subcat-more').setStyle('display', 'none');
		    	el.addClass('active');
		    	el.getElement('.subcat-more').setStyle('display', 'inline');		    	
		    	
		    	if($('ja-articles-'+this.options.moduleid+'-'+catid)!=null){
		    		cur_activePanel =  this.panelArticles.getElement('div.active');
            		cur_activePanel.removeClass('active');            		
            		var activePanel = $('ja-articles-'+this.options.moduleid+'-'+catid);
            		activePanel.addClass('active');
            		this.panelArticles.setStyle('height', activePanel.getFirst().offsetHeight);
            		
            		this.panels = this.panelArticles.getElements('div.ja-articles');		
            		if(this.anim==null) this.anim = new animFade(this);
            		
            		this.anim.move(cur_activePanel, activePanel, false);
            		return;
		    	}
		    	
		    	this.loading = this.panelArticles.getElement('.ja-newspro-loading');
		    	if(this.loading!=null){		    				    		
		    		this.panelArticles.getElement('div.active').setOpacity('0.3');		    			
		    		this.loading.setStyles({'display':'block', 'top': this.panelArticles.offsetHeight/2-10, 'left': this.panelArticles.offsetWidth/2});
		    	}
		    	
		    	var url = location.href;
		    	if(url.indexOf('#')>-1){
	        		url = url.substr(0, url.indexOf('#'));
	        	}
	        	if(url.indexOf('?')>-1) url += '&';
	        	else url += '?';
	        	url += 'janewspro_linear_ajax=1&moduleid='+this.options.moduleid + '&subcat='+ catid;
	        	
		    	if(requesting) return false;		    	
	        	requesting = true;
	        	
	            new Ajax(url, {
	                method: 'get',
	                onComplete: function (data) {	            	
	            		requesting = false;    
	            		if(this.loading!=null) this.loading.setStyle('display', 'none');
	            		cur_activePanel =  this.panelArticles.getElement('div.active');
	            		cur_activePanel.removeClass('active');
	            		
	            		var activePanel = new Element('div', {'class':'ja-articles active', 'id': 'ja-articles-'+this.options.moduleid+'-'+catid}).injectInside(this.panelArticles);
	            		activePanel.innerHTML = data;
	            		this.panelArticles.setStyle('height', activePanel.getFirst().offsetHeight);
	            		
	            		this.panels = this.panelArticles.getElements('div.ja-articles');				
	            		
	            		if(this.anim==null) this.anim = new animFade(this);
	            		
	            		this.anim.move(cur_activePanel, activePanel, false);
	            		
	            		
	                   	/*tooltip*/
	                   	var JTooltips = new Tips($$('#ja-articles-'+this.options.moduleid+'-'+catid + ' .jahasTip'), { maxTitleChars: 50, fixed: false, className: 'tool-tip janews-tool'});

	                }.bind(this),
	                
	                onFailure: function () {
	                	if(this.loading!=null) this.loading.setStyle('display', 'none');
	                	requesting = false;
	                    alert('fail request');
	                }
	            }).request();	            	            	            
	            
	            
		    }.bind(this));
		    
		}.bind(this));
		width += 10;
		this.panelwrap.setStyle('width', width);		
		
		if(this._W<width && this.panelwrap.getLast().offsetLeft>this._W){
			this.panel.getElement('.ja-newspro-control').setStyle('display', 'block');
		}
		
		this.next_bt = this.panel.getElement('.ja-newspro-control a.next');
		this.prev_bt = this.panel.getElement('.ja-newspro-control a.prev');
				
		if(this.next_bt!=null){
			this.next_bt.addEvent('click', function () {
		    	this.next();
		    }.bind(this));
		    
		    this.prev_bt.addEvent('click', function () {
		    	this.previous();
		    }.bind(this));		    		    
		}
		
		    
	},
	
    next: function(){
		
		var els = this.panelwrap.getChildren();
		var left = 0;
		var hasNext = false;
		if(this.active==null){
			this.active = els[0];
			this._L = 0;
		}		
				
		var _Wleft = this._W + this._L;
		
		for(var i=0; i<els.length; i++){
			el = els[i];			
			if(left>_Wleft){ 				
				this.active = el;
				this._index = i;				
				hasNext = true;
				break;
			}
			left += el.offsetWidth;				
		}

		if(!hasNext) return;
		
		this._L = left;
		
		if(this.changeEffect==null){
			this.changeEffect = new Fx.Style(this.panelwrap, 'margin-left',{duration:this.options.duration});
		}
		this.changeEffect.stop();
		this.changeEffect.start(-left);		
    },
    
    previous: function(){
    	var left = this._L;
    	
		var els = this.panelwrap.getChildren();
		var hasPrev = false;
		
    	if(this.active==null || this._index==null){
			return;
		}
    	for(var i=this._index; i>=0; i--){
    		el = els[i];
			if(left<=this._L-this._W){ 				
				this.active = el;
				this._index = i;
				hasPrev = true;
				break;
			}
			left = left - el.offsetWidth;			
		}
    	
    	var left = 0;
    	for(var i=0; i<this._index; i++){
    		el = els[i];						
			left += el.offsetWidth;				
    	}
    	this._L = left;
    	
    	if(!hasPrev) return;
    	
    	if(this.changeEffect==null){
			this.changeEffect = new Fx.Style(this.panelwrap, 'margin-left',{duration:this.options.duration});
		}
		this.changeEffect.stop();
		this.changeEffect.start(-left);		
    }
	
});

var animFade = new Class ({
	initialize: function(tabwrap) {
		this.options = tabwrap.options || {};
		this.tabwrap = tabwrap;
		this.changeEffect = new Fx.Elements(this.tabwrap.panels, {duration: this.options.duration});
		this.tabwrap.panels.setStyles({'opacity':0,'width':'100%'});
	},

	move: function (curTab, newTab, skipAnim) {
		if(this.options.changeTransition != 'none' && !skipAnim)
		{
			if (curTab)
			{
				curOpac = curTab.getStyle('opacity');
				var changeEffect = new Fx.Style(curTab, 'opacity', {duration: this.options.duration, transition: this.options.changeTransition});
				changeEffect.stop();
				changeEffect.start(curOpac,0);
			}
			curOpac = newTab.getStyle('opacity');
			var changeEffect = new Fx.Style(newTab, 'opacity', {duration: this.options.duration, transition: this.options.changeTransition});
			changeEffect.stop();
			changeEffect.start(curOpac,1);
		} else {
			if (curTab) curTab.setStyle('opacity', 0);
			newTab.setStyle('opacity', 1);
		}
	}
});