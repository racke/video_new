<?php
/**
 * Get all items from 首页视频 category
 *
 */

defined('_JEXEC') or die('Restricted access');

class FeaturedHelper{

	function getFeaturedArticles(){
		fb('======featured helper');
		jimport('joomla.filesystem.file');
		$items = array();
		$limit = 8;
		$db = &JFactory::getDBO();

		$query = "SELECT i.title, i.id";
		$query .= " FROM #__k2_items as i ";
		$query .= " WHERE i.featured=1";
		$query .= " AND i.trash!=1";
		$query .= " ORDER BY i.ordering DESC, i.created DESC";
		//fb('main video query: '.$query);
		$db->setQuery($query, 0, $limit);
		$items = $db->loadObjectList();
		
		return $items;
	}

}
?>
