<?php
/**
 * Get all items from 首页视频 category
 *
 */

defined('_JEXEC') or die('Restricted access');

class MainVideoHelper{

	function getMainVideos(){
		jimport('joomla.filesystem.file');
		$items = array();
		$limit = 8;
		$cname = '首页视频';
		$db = &JFactory::getDBO();

		$query = "SELECT i.title, i.video, c.name AS categoryname,c.id AS categoryid ";
		$query .= " FROM #__k2_items as i LEFT JOIN #__k2_categories c ON c.id = i.catid";
		$query .= " WHERE c.name='{$cname}'";
		$query .= " AND i.trash!=1";
		$query .= " ORDER BY i.created DESC";
		//fb('main video query: '.$query);
		$db->setQuery($query, 0, $limit);
		$items = $db->loadObjectList();
		
		return $items;
	}

}
?>
