﻿<?php
/**
* @version		$Id: mod_mostread.php 10381 2008-06-01 03:35:53Z pasamio $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2008 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
require_once(dirname(__FILE__).DS.'helper.php');

$siteUrl  = substr(JURI::root(), 0, -1);
$plgUrl = "/plugins/content/jw_allvideos/includes/players/jw_player5.6";
$swfUrl = $siteUrl.$plgUrl."/player.swf";
$jsUrl = $siteUrl.$plgUrl."/jwplayer.js";

$items = MainVideoHelper::getMainVideos();
$regexRemote = "#{flvremote}(.*?){/flvremote}#s";
$playlist = '[';
foreach ($items as $item){
	if($item->video){
		$tagcontent = preg_replace("/{.+?}/", "", $item->video);
		$playlist.='{';
		
		if(preg_match_all($regexRemote, $item->video, $matches, PREG_PATTERN_ORDER) > 0){
			if($tagcontent == ''){
				$playlist.='file: "media/k2/videos/filenotexist.flv", streamer: "'.$siteUrl.'/xmoov.php"';
			}else{
				$file = substr($tagcontent, 1); 
				$playlist.='file: "'.$file.'", streamer: "'.$siteUrl.'/xmoov.php"';
			}
		}else{
			$playlist.='file: "media/k2/videos/'.$tagcontent.'.flv", streamer: "'.$siteUrl.'/xmoov.php"';
		}
		
		
		
		//$playlist.='file: "'.$siteUrl.'/xmoov.php?file=media/k2/videos/'.$tagcontent.'.flv"';
		$playlist.='},';
	}
}
if(count($items)>0){
	$playlist = substr($playlist, 0, -1);
}
$playlist .= ']';
?>
<script type="text/javascript" src="<?php echo $jsUrl;?>"></script>


 <div class="videoContainer" style="margin:0px">
      <div  class="video indexVideo">
      <div id="main_video"></div>
      
       </div>
      <div class="description">
      <div class="titleImage"></div>
            <ul>
            
      <?php 
      $index = 0;
      foreach ($items as $item){
      ?>
                <li>
                    <div class="number"><?php echo $index+1;?></div>
                    <a href="#" onclick="javascript:jwplayer().playlistItem(<?php echo $index;?>);return false;"><?php echo $item->title;?></a>
                </li>
      
      
      	
      <?php 
      	$index++;
      }?>
                
            <div class="clr"></div>
            </ul>
      </div>
      <div class="clr"> </div>
    </div>
    
    <script>
	window.addEvent('domready', function(){      
	      
	      jwplayer('main_video').setup({
    autostart: true, 
    repeat:'always',
	flashplayer: '<?php echo $swfUrl;?>', 
    playlist: <?php echo $playlist;?>,
	height: 375, 
	width: 471,
	provider: 'http',
	'controlbar.position': 'bottom',
	'controlbar.idlehide': true,
	'viral.oncomplete': 'false',
	'viral.pause': 'false'
	      })
	});  </script>
