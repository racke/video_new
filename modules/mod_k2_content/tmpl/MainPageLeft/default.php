<?php
/**
 * @version		$Id: default.php 565 2010-09-23 11:48:48Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
//fb($this);
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="">

	<?php if(count($items)): ?>
  <ul class="commonRankingList <?php echo $params->get('new_class_sfx'); ?>">
    <?php foreach ($items as $key=>$item):	?>
    <li class="">



	<?php 
	$iParams = new JParameter($item->params);
	$mediaType = $iParams->get('mediaType')==''?'专题片':$iParams->get('mediaType');
	?>
      <?php if($params->get('itemTitle')): ?>
      <a class="" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
      <?php endif; ?>
	  <div class="type"><?php echo $mediaType;?></div>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>

	

</div>