<?php
/**
 * @version		$Id: default.php 565 2010-09-23 11:48:48Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
$category_id = $params->get('category_id', NULL);
$menu_id=0;
if($category_id==5){
//地区政策
  $menu_id=4;
}elseif($category_id==10){
//中心动态
  $menu_id=17;
}elseif($category_id==6){
//广播栏目
  $menu_id=16;
}

if(strpos($params->get('itemPreText'),'幸福延长') !== FALSE){
//make left modules display contents
    $menu_id=1;
}
?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="infoList <?php echo $params->get('moduleclass_sfx');?>">
	
	<?php if($params->get('itemPreText')): ?>
	 <div class="title"><?php echo $params->get('itemPreText'); ?></div>
	<?php endif; ?>


	<?php if(count($items)): ?>
  <ul>
    <?php foreach ($items as $key=>$item):	?>
   	<?php 
	$iParams = new JParameter($item->params);
	$mediaType = $iParams->get('mediaType')==''?'专题片':$iParams->get('mediaType');
	?>
    <li>
      <?php if($params->get('itemTitle')): ?>
      <a class="" style="float:left;width:92%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;" href="<?php echo $item->link; ?>"><?php echo $item->title; ?>&nbsp;&nbsp;</a><!--<div style="float:left;width:20%;"><?php echo $mediaType;?></div>-->
      <?php endif; ?>
    </li>
    <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <div class="more"><a href="index.php?option=com_k2&view=itemlist&layout=category&task=category&id=<?php echo $category_id;?>&Itemid=<?php echo $menu_id; ?>"></a></div>
</div>
