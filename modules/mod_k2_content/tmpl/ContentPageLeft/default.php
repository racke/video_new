<?php
/**
 * @version		$Id: default.php 565 2010-09-23 11:48:48Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>

<div class="ranking-wrap">
    <div class="subPageRankingTitle"> 点击排行</div>


	<?php if(count($items)): ?>
    <div id="k2ModuleBox44" class="">

        <ul class="commonRankingList test">
    <?php foreach ($items as $key=>$item):	?>
        <li class="">



            <a class="" href="<?php echo $item->link ?>"><?php echo $item->title ?></a>

        </li>
    <?php endforeach; ?>
            </ul>
        </div>

  <?php endif; ?>
   </div>
