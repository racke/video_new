<?php
/*
# ------------------------------------------------------------------------
# JA Disqus and Debate comment for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/
?>
<?php if (count ($this->_commentsID) && count ($this->_comments)): ?>
<div style="display:none;">
<?php echo implode ("\n\n", $this->_comments); ?>
</div>
<script type="text/javascript">
/* <![CDATA[ */
window.addEvent('domready', function(){
	ids = [<?php echo "'".implode ("','", $this->_commentsID)."'"; ?>];
	ids.each (function(id) {
		holder = $('JaCommentPlaceHolder'+id);
		comment = $('JaCommentContainer'+id);
		if ($chk(holder) && $chk(comment)) {
			holder.replaceWith(comment.remove());
		}
	});
});
/* ]]> */
</script>
<?php endif; ?>