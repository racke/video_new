<?php
/*
# ------------------------------------------------------------------------
# JA Disqus and Debate comment for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/

//JS-Kit config
$domain         = $this->plgParams->get('provider-jskit-domain');


$sefPath = $this->_sefpath;
$sefUrl = $this->_sefurl;
if ($this->isComment == false) {
	if (!defined('JACOMMENT_COUNT')) :
		$headscript = "
			<script type='text/javascript'>
			//<![CDATA[
			window.addEvent('domready', function(){
				var aCountComments = $$('span.js-kit-comments-count');
				if(aCountComments.length > 0) {
					Asset.javascript('http://js-kit.com/for/".$domain."/comments-count.js', {charset: 'utf-8'});
				}
			});
			//]]> 
			</script>
		";
		$this->document->addCustomTag($headscript);
		define('JACOMMENT_COUNT',1);
	endif;
?>
	<div class="jacomment-count"><a href="<?php echo $this->_path; ?>"><?php echo JText::_("COMMENTS"); ?> (<span class="js-kit-comments-count" uniq="<?php echo $sefPath; ?>">0</span>)</a></div>
<?php } else { ?>
	<div class="js-kit-comments" permalink="<?php echo $sefUrl;?>" path="<?php echo $sefPath;?>"></div>
	<script type="text/javascript" src="http://js-kit.com/for/<?php echo $domain?>/comments.js"></script>
<?php } ?>