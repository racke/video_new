<?php
/*
# ------------------------------------------------------------------------
# JA Disqus and Debate comment for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/ 

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );



class plgContentPlg_Disqus_debate_echo extends JPlugin
{
	var $_plgCode = "#{jacomment(.*?)}#i";
	var $_plgCodeDisable = "#{jacomment(\s*)disable}#i";
	
	var $_url = "";
	var $_path = "";
	var $_postid = "";
	
	var $source = "both";
	var $provider = "intensedebate";
	var $_comments = array();
	var $_commentsID = array();

	function plgContentPlg_Disqus_debate_echo( &$subject, $config )
	{
		parent::__construct( $subject, $config );

		$this->plugin = &JPluginHelper::getPlugin('content', 'plg_disqus_debate_echo');
		$this->plgParams = new JParameter($this->plugin->params);
		$this->source   = $this->plgParams->get('source', 'both');
		$provider         = $this->plgParams->get('provider', 'intensedebate');
		switch($provider) 
		{
			case 'disqus': $provider = 'disqus'; break;
			case 'jskit': $provider = 'jskit'; break;
			case 'intensdebate': $provider = 'intensedebate'; break;
			default: $provider = 'intensedebate'; break;
		}
		$this->provider = $provider;
		$this->stylesheet ($this->plugin);
	}

	function onPrepareContent( &$article, &$params, $limitstart )
	{
		global $mainframe;
		global $option;
		static $count = 0;
		
		static $addedUrls = array();
		
		//$sectionid      = $article->sectionid;
		if(!isset($article->catid)) {
			$article->catid = 0;
		}
		$catid          = $article->catid ;
		$option 		= JRequest::getCmd('option');
		$view 			= JRequest::getCmd('view');
		$itemid 		= JRequest::getInt('Itemid');
		$id				= JRequest::getInt('id');
				//
		$plugin = $this->plugin;
		$plgParams = $this->plgParams;
		
		//
		$check = true;
		if(!$article->id) {
			return '';
		}
		
		if ($this->source != 'both' && $this->source != $option) 
			$check = false;
		
		$display_on_home = (int) $plgParams->get('display_on_home', 1);
		if($view == 'frontpage' && $display_on_home != 1)
			$check = false;
			
		$display_on_list = (int) $plgParams->get('display_on_list', 1);
		if((($option == 'com_k2' && $view != 'item') || ($option == 'com_content' && $view != 'article' && $view != 'frontpage')) && $display_on_list != 1)
			$check = false;
		
		if($this->isDetailPage()) {
			//it is not called from detail view
			if($id && $id != $article->id) {
				$check = false;
			}
		}
		
		if(preg_match($this->_plgCodeDisable, $article->text)) {
			$check = false;
		}
		
		if (!$check) {
			$article->text = $this->removeCode($article->text);
			return;
		}
		
		$isComment = false;

		//GET PLUGIN CONFIG
		$mode   		= $plgParams->get('mode','id');
		
		if($this->isContentItem($article)) {
			$catids         = $plgParams->get('mode-automatic-catsid','');
		} else {
			$catids         = $plgParams->get('mode-automatic-k2catsid','');
		}
		$tagmode        = $plgParams->get('tagmode',0);
		$display_comment_count  = $plgParams->get('display_comment_count',1);
		
		//
		$user 		    =& JFactory::getUser();
		$document       =& JFactory::getDocument();
		$this->document =& $document;
		
		//CHECK PAGE WAS SELECTED TO DISPLAY COMMENT FORM
		switch ($mode)
		{
			case 'disable':
				$article->text = $this->removeCode($article->text);
				return;
				break;
			case 'manual':
				if (!preg_match($this->_plgCode, $article->text)) {
					return;
				}
				break;
			case 'automatic':
			default:
				if (is_array($catids)){
					$categories = $catids;
				} elseif ($catids==''){
					$categories[] = $catid;
				} else {
					$categories[] = $catids;
				}
				
				if( !(in_array($catid,$categories) || preg_match($this->_plgCode, $article->text))) {
					return;
				}
				break;
		}
		//END OF CHECK
		
		$text = 'text';
		if ($article->access <= $user->get('aid', 0)){
			if($option == "com_content") {
				$this->_path = JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catslug, $article->sectionid));
			} elseif($option == "com_k2") {
				if($this->isDetailPage()) {
					//priority: text->fulltext->introtext
					$text = isset($article->text) ? 'text' : isset($article->fulltext) ? 'fulltext' : 'introtext';
				} else {
					//priority: introtext->text->fulltext
					$text = isset($article->introtext) ? 'introtext' : isset($article->text) ? 'text' : 'fulltext';
				}
				$this->_path = K2HelperRoute::getItemRoute($article->id, $article->catid);
			} else {
				$this->_path = "#";
			}
			
		} else {
			$this->_path = JRoute::_("index.php?option=com_user&view=login");
		}
		
		
		if(!preg_match("/^\//", $this->_path)) {
			//convert to relative url
			$this->_path = JURI::root(true).'/'.$this->_path;
		}
		//convert to absolute url
		$this->_url = $this->getRootUrl().$this->_path;
		
		$this->_sefurl = $this->convertUrl($this->_path);
		$this->_sefpath = $this->convertUrl($this->_path, true);
		
		if(in_array($this->_url, $addedUrls)) {
			return ;
		}
		$addedUrls[] = $this->_url;
		
		if ($option == "com_content" && $view=='article') {
			$isComment = true;
		}
		elseif ($option == "com_k2" && $view=='item') {
			$isComment = true;
		}
	
		$this->_postid = md5($this->_url);
		
		if (!$isComment and $display_comment_count==0){ 
			return;
		}
		
		$this->isComment = $isComment;
		
		$provider = $this->provider;
		$output = $this->loadLayout ($this->plugin, $provider);
		
		/**
		 * JS Loading Optimize
		 * create place holder for comment 
		 */
		$uniqueID = $this->_postid;
		$container = 'JaCommentContainer'.$uniqueID;
		$holder = 'JaCommentPlaceHolder'.$uniqueID;
		
		$store = &Plg_comment_storage::getInstance();
		$store->_comments[] = '<div id="'.$container.'">'.$output.'</div>';
		$store->_commentsID[] = $uniqueID;
		
		$output = '<div id="'.$holder.'"></div>';
		
		if(isset($article->$text)) {
			if ($mode == 'manual' || preg_match($this->_plgCode, $article->$text)) {
				$article->$text = preg_replace( $this->_plgCode, $output, $article->$text );
			} else {
				$article->$text .= $output;
			}
		}
		
	
		return true;
		
	}
	
	/**
	 *
	 * @return (string) - root url without last slashes
	 */
	function getRootUrl() {
		$url = str_replace(JURI::root(true), '', JURI::root());
		$url = preg_replace("/\/+$/", '', $url);
		return $url;
	}
	
	function isDetailPage() {
		$option 	= JRequest::getVar('option');
		$view 		= JRequest::getVar('view');
		//if its a detail page
		if (($option == 'com_k2' && $view == 'item') || ($option == 'com_content' && $view == 'article')) {
			return true;
		}
		return false;
	}
	
	function isContentItem($article) {
		return isset($article->sectionid) ? true : false;
	}
	
	function isK2Item($article) {
		return isset($article->featured) ? true : false;
	}
	/**
	 * convert from normal format to sef format
	 *
	 * @param unknown_type $matches
	 * @return unknown
	 */
	function convertUrl($originalPath, $pathonly=false)
	{
		jimport( 'joomla.application.router' );
		// Get the router
		$router = new JRouterSite( array('mode' => JROUTER_MODE_SEF) );
	
		// Make sure that we have our router
		if (! $router) {
			if($pathonly)
				return $originalPath;
			else 
				return $this->getRootUrl().$originalPath;
		}
	
		/*if ( (strpos($originalPath, '&') !== 0 ) && (strpos($originalPath, 'index.php') !== 0) ) {
			if($pathonly)
				return $originalPath;
			else 
				return $this->getRootUrl().$originalPath;
		}*/
		
		// Build route
		$path = str_replace(JURI::root(true), '', $originalPath);
		$uri = &$router->build($path);
		
		$path = $uri->toString(array('path', 'query', 'fragment'));
		if(!preg_match("/^\/+/", $path)) {
			$path = "/{$path}";
		}
		//
		if($pathonly) {
			return $path;
		} else {
			$url = $this->getRootUrl();
			return $url.$path;
		}
	}

	function onAfterRender(){
		$store = &Plg_comment_storage::getInstance();
		
		$this->_comments = $store->_comments;
		$this->_commentsID = $store->_commentsID;
		
		$body = JResponse::getBody();
		$comments = $this->loadLayout($this->plugin, 'common');
		$countscript = '';
		if($this->provider == 'disqus') {
			$countscript = $this->loadLayout($this->plugin, 'disqus_count');
		}
		$body = str_replace('</body>', $comments."\n</body>",$body);
		$body = str_replace('</body>', $countscript."\n</body>",$body);
		JResponse::setBody($body);
	}
	
	function removeCode($content)
	{
		return preg_replace( $this->_plgCode, '', $content );
	}
	
	function getLayoutPath($plugin, $layout = 'default')
	{
		global $mainframe;

		// Build the template and base path for the layout
		$tPath = JPATH_BASE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$plugin->name.DS.$layout.'.php';
		$bPath = JPATH_BASE.DS.'plugins'.DS.$plugin->type.DS.$plugin->name.DS.'tmpl'.DS.$layout.'.php';
		// If the template has a layout override use it
		if (file_exists($tPath)) {
			return $tPath;
		} elseif (file_exists($bPath)) {
			return $bPath;
		}
		return '';
	}

	function loadLayout ($plugin, $layout = 'default') {
		$layout_path = $this->getLayoutPath ($plugin, $layout);
		if ($layout_path) {
			ob_start();
			require $layout_path;
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}
		return '';
	}

	function stylesheet ($plugin) {
		global $mainframe;
		JHTML::stylesheet('style.css','plugins/'.$plugin->type.'/'.$plugin->name.'/');
		if (is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'css'.DS.$plugin->name.".css"))
		JHTML::stylesheet($plugin->name.".css",'templates/'.$mainframe->getTemplate().'/css/');
	} 
}

class Plg_comment_storage {
	var $_comments = array();
	var $_commentsID = array();
	
	function &getInstance () {
		static $instance = null;
		
		if ($instance == null){ 
			$instance = new Plg_comment_storage();
		}
		
		return $instance;
	}
}

?>