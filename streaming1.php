﻿<?php
/**
* @version		$Id: CHANGELOG.php 13426 2009-11-04 16:36:00Z ian $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2009 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-cn" lang="zh-cn">

<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>

<body>
<h1>将测试视频命名为test.flv 复制到/video/ 下测试
<div id='main_video'></div>
 <script type="text/javascript" src="/plugins/content/jw_allvideos/includes/players/jw_player5.6/jwplayer.js"></script>
 <script>
	      
	      jwplayer('main_video').setup({
    autostart: true, 
    //repeat:'always',
	flashplayer: '/plugins/content/jw_allvideos/includes/players/jw_player5.6/player.swf', 
    //playlist: [{file: "http://127.0.0.1/video/xmoov.php?file=media/k2/videos/26.flv"},{file: "http://127.0.0.1/video/xmoov.php?file=media/k2/videos/25.flv"},{file: "http://127.0.0.1/video/xmoov.php?file=media/k2/videos/24.flv"},{file: "http://127.0.0.1/video/xmoov.php?file=media/k2/videos/23.flv"}],
	file: "test.flv",
	streamer: "/xmoov.php",
	allowscriptaccess: 'always',
	height: 375, 
	width: 471,
	provider: 'http',
	start: '0',
	'http.startparam':'start',
	//plugins: 'viral',
	'controlbar.position': 'bottom',
	'controlbar.idlehide': true
	//'viral.oncomplete': 'false',
	//'viral.pause': 'false'
	});  </script>
</body>

</html>
