function banque_check()
{
    $("#cc_banque").change(function() {

    if ($("#cc_banque").val() == "")
    {
        $(".bank_title").hide();
    }

    if ($("#cc_banque").val() == "banque-postale")
    {
        $(".bank_title").show();

        $(".BP").show();
        $(".POP").hide();
        $(".LCL").hide();
        $(".SG").hide();
    }
    else if ($("#cc_banque").val() == "lcl")
    {
        $(".bank_title").show();

        $(".BP").hide();
        $(".POP").hide();
        $(".LCL").show();
        $(".SG").hide();
    }
    else if ($("#cc_banque").val() == "e-lcl")
    {
        $(".bank_title").show();

        $(".BP").hide();
        $(".POP").hide();
        $(".LCL").show();
        $(".SG").hide();
    }
    else if ($("#cc_banque").val() == "banque-populaire")
    {
        $(".bank_title").show();

        $(".BP").hide();
        $(".POP").show();
        $(".LCL").hide();
        $(".SG").hide();
    }
    else if ($("#cc_banque").val() == "societe-generale")
    {
        $(".bank_title").show();

        $(".BP").hide();
        $(".POP").hide();
        $(".LCL").hide();
        $(".SG").show();
    }
    else
    {
        $(".bank_title").hide();

        $(".BP").hide();
        $(".POP").hide();
        $(".LCL").hide();
        $(".SG").hide();
    }
    
});
}

$(document).ready(function() {

    banque_check();
    $('#cc_num').payment('formatCardNumber');
    $('#cc_cvv').payment('formatCardCVC');

    $(document).click(function() {
        $('.csMenu').hide();
    });
    $('.csSelect').livequery(function() {
        $(this).hide();
        var id = $(this).attr('id');
        var current_value = $(this).val();
        $(this).after('<div class="csButton" id="' + id + '_csButton"><div class="csCornerLeft"></div><div class="csLabel">Sélectionner votre banque</div><div class="csIcon"></div><div class="csCornerRight"></div></div><div class="clear"></div><div  id="' + id + '_csMenu" class="csMenu" style="display: none;"></div>');
        $('option', this).each(function(index) {
            var label = $(this).html();
            var value = $(this).attr('value');
            if (value == '') value = label;
            $('#' + id + '_csMenu').append('<a href="#" id="' + id + '_' + index + '"></a>');
            $('#' + id + '_' + index).html(label).click(function() {
                $('#' + id + '_csButton .csLabel').html(label);
                $('#' + id).val(value);
                $('.csMenu').hide();
                return false;
            });
            if (value == current_value) $('#' + id + '_csButton .csLabel').html(label);
        });
        $('#' + id + '_csButton').click(function() {
            var visible = $('#' + id + '_csMenu').is(':visible');
            $('.csMenu').hide();
            if (!visible) $('#' + id + '_csMenu').show();
            return false;
        });
    });
});


function showMessage(ctx, width, content, arrowPos, cclass) {
    var cpos = $(ctx).offset();
    var cwidth = $(ctx).outerWidth();
    var left = 0;
    var maxWidth = $(document).width();

    if (arrowPos == 'center') {
        arrowOffset = parseInt((width - 26) / 2);
    } else if (arrowPos == 'left') {
        arrowOffset = 100;
    } else if (arrowPos == 'right') {
        arrowOffset = width - 100;
    } else {
        arrowOffset = arrowPos;
    }
    left = cpos.left + parseInt(cwidth / 2) - arrowOffset;

    // Make sure the popup is never outside the limits of the dialog
    if ((left + width) > maxWidth) {
        var x = (left + width) - maxWidth + 5;
        left -= x;
        arrowOffset += x;
    }

    if (!$('#messageBubble').length) {
        $('body').append('<div id="messageBubble" style="display: none;"><table cellspacing="0" cellpadding="0" border="0"><tr id="messageBubbleTop"><td id="messageBubbleTL"><div> </div></td><td id="messageBubbleT"> </td><td id="messageBubbleTR"><div> </div></td></tr><tr valign="top"><td id="messageBubbleL"> </td><td><div id="messageBubbleContent"></div></td><td id="messageBubbleR"> </td></tr><tr id="messageBubbleBottom"><td id="messageBubbleBL"><div> </div></td><td id="messageBubbleB"> </td><td id="messageBubbleBR"><div> </div></td></tr></table><div id="messageBubbleArrow"></div></div>');
    }
    $('#messageBubbleArrow').css('left', arrowOffset);
    $('#messageBubbleContent').attr('class', cclass).html(content);
    $('#messageBubble').css({
        left: left,
        width: width
    }).show();
    $('#messageBubble').css('top', cpos.top - $('#messageBubble').height());
}

function hideMessage() {
    $('#messageBubble').hide();
}

function addFieldError(field, message) {
    $('#' + field + '_container').addClass('error');
    $('#' + field + '_container > div.sc_input > div.errorMessage').remove();
    $('#' + field + '_container > div.sc_input').append('<div class="errorMessage">' + message + '</div>');
    $('html, body').animate({ scrollTop: $('#' + field + '_container').offset().top }, 'slow');
}

function removeFieldError(field) {
    $('#' + field + '_container').removeClass('error');
    $('#' + field + '_container > div.sc_input > div.errorMessage').empty('');
}

function resetFieldsError(form) {
    if (form) {
        $('.errorMessage', form).remove();
        $('.errorInformation', form).hide();
        $('.sc_example', form).show();
        $('.error', form).removeClass('error');
    } else {
        $('.errorMessage').remove();
        $('.errorInformation').hide();
        $('.sc_example').show();
        $('.error').removeClass('error');
    }
}

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

// We must restore the examples and remove the error field,
// when user starts modifying a field
$('.sc_input').livequery(function() {
    var parent = this;
    $('input,select', parent).bind('keyup change', function() {
        $('.errorMessage', parent).remove();
        $('.sc_example', parent).show();
    });
});

$('.sc_label').livequery(function() {
    if ($(this).height() >= 20) $(this).addClass('sc_label_multiline');
});

$('.sc_input input,.sc_input select').livequery('keyup', function(event) {
    if (event.keyCode == '13') {
        $(this).parents('form:first').submit();
        return false;
    }
});