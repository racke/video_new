<?php
/**
 * @copyright	Copyright (C) 2005 - 2007 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

include_once (dirname(__FILE__).DS.'/ja_vars.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">

<head>
<meta name="baidu-tc-verification" content="025bc3a8488833254b344db9e9466ff2" />
<jdoc:include type="head" />
<?php JHTML::_('behavior.mootools'); ?>

<link rel="stylesheet" href="<?php echo $tmpTools->baseurl(); ?>templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $tmpTools->baseurl(); ?>templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $tmpTools->templateurl(); ?>/css/newStyle/style.css" type="text/css" />
<!--[if lte IE 6]>
<link rel="stylesheet" href="<?php echo $tmpTools->templateurl(); ?>/css/newStyle/style_ie6.css" type="text/css" />
<![endif]-->

<script language="javascript" type="text/javascript" src="<?php echo $tmpTools->templateurl(); ?>/js/ja.script.js"></script>


<?php if ($tmpTools->getParam('rightCollapsible')): ?>
<script language="javascript" type="text/javascript">
var rightCollapseDefault='<?php echo $tmpTools->getParam('rightCollapseDefault'); ?>';
var excludeModules='<?php echo $tmpTools->getParam('excludeModules'); ?>';
</script>
<script language="javascript" type="text/javascript" src="<?php echo $tmpTools->templateurl(); ?>/js/ja.rightcol.js"></script>

<?php endif; ?>
<script type="text/javascript" src="flash/swfobject.js"></script>
		<script type="text/javascript">
		swfobject.embedSWF("/video/flash/header1.swf", "headerSWFWrapper", "848", "260", "9.0.0", "/video/flash/expressInstall.swf");
		</script>
<!--[if IE 7.0]>
<style type="text/css">
.clearfix {display: inline-block;}
</style>
<![endif]-->
<?php if ($tmpTools->isIE6()): ?>
<!--[if lte IE 6]>
<script type="text/javascript">
var siteurl = '<?php echo $tmpTools->baseurl();?>';

window.addEvent ('load', makeTransBG);
function makeTransBG() {
	fixIEPNG($E('.ja-headermask'), '', '', 1);
	fixIEPNG($E('h1.logo a'));
	fixIEPNG($$('img'));
	fixIEPNG ($$('#ja-mainnav ul.menu li ul'), '', 'scale', 0, 2);
}
</script>
<style type="text/css">
.ja-headermask, h1.logo a, #ja-cssmenu li ul { background-position: -1000px; }
#ja-cssmenu li ul li, #ja-cssmenu li a { background:transparent url(<?php echo $tmpTools->templateurl(); ?>/images/blank.png) no-repeat right;}
.clearfix {height: 1%;}
</style>
<![endif]-->
<?php endif; ?>

</head>

<body id="bd" class="fs<?php echo $tmpTools->getParam(JA_TOOL_FONT);?> <?php echo $tmpTools->browser();?>" >
<script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F1491b841dc916f0a959789735c5fb895' type='text/javascript'%3E%3C/script%3E"));
</script>
<a name="Top" id="Top"></a>


<div id="container">

<!-- BEGIN: HEADER -->
<div id="header">
  <div class="headerLeft">
    <ul class="nav">
      <li style="margin-left:35px"> <a href="">首&nbsp;&nbsp;&nbsp;&nbsp;页</a> <br/>
        <span >home page</span> </li>
      <li style="margin-left:40px"> <a href="index.php?option=com_k2&view=itemlist&layout=category&task=category&id=10&Itemid=17">中心动态</a> <br/>
        <span >Center News</span> </li>
      <li style="margin-left:40px"> <a href="index.php?option=com_yewupingtai&page_id=quxian">业务平台</a> <br/>
        <span >Data Platform</span> </li>
      <li style="margin-left:25px"> <a href="index.php?option=com_zhongxinjianjie&page_id=index">中心简介</a> <br/>
        <span > Introduction </span> </li>
      <li> <a href="index.php?option=com_rongyuguan&page_id=guojiaji">荣誉馆</a> <br/>
        <span > honour </span> </li>
    </ul>
  </div>
  
  
  
  
  
  <div class="headerRight">
  <div id="headerSWFWrapper">
  </div>
  
<script>
window.onload=function(){

}
</script>
    <div class="centerNav">
   


      <div class="centerNavLeft"> </div>
      <ul class="centerNavCenter">
       <!-- BEGIN: MAIN NAVIGATION -->
<?php if ($this->countModules('hornav')): ?>
<div id="ja-mainnavwrap">
	<div id="ja-mainnav" class="clearfix">
	<jdoc:include type="modules" name="hornav" />
	</div>
</div>
<?php endif; ?>

      </ul>
      <div class="centerNavRight"> </div>
      <?php if($this->countModules('user4')) : ?>
<div id="ja-search">
	<jdoc:include type="modules" name="user4" />
</div>
<?php endif; ?>
    </div>
    <?php  		$document   = &JFactory::getDocument();
   		$renderer   = $document->loadRenderer('module');
   		?>
    <?php echo $renderer->render(JModuleHelper::getModule('k2_tools', 'k2搜索'));?>
    
    

    
    <div class="clr"></div>
  </div>
  
  
  <div class="clr"> </div>
</div>


<jdoc:include type="component" />



<!-- BEGIN: FOOTER -->
<div id="footer"> 
	<div id="ja-footnav">
		<jdoc:include type="modules" name="user3" />
	</div>
	<div class="logo">上海人口和计划生育宣传教育中心<br></br>沪ICP备10029564号-1</div>
	<div class="copyright">
		<jdoc:include type="modules" name="footer" />
	</div>

	<jdoc:include type="modules" name="syndicate" />

</div>
<!-- END: FOOTER -->

</div>

<jdoc:include type="modules" name="debug" />


</body>

</html>
