<?php
/**
 * @copyright    Copyright (C) 2005 - 2007 Open Source Matters. All rights reserved.
 * @license        GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
include_once(dirname(__FILE__) . DS . '/ja_vars.php'); //Really need this?

?>
<!doctype html>
<html>
<head>
    <meta name="baidu-tc-verification" content="025bc3a8488833254b344db9e9466ff2"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="/video/media/system/css/modal.css" type="text/css"/>
    <link rel="stylesheet" href="/video/components/com_k2/css/k2.css" type="text/css"/>
    <link rel="stylesheet" href="/video/templates/system/css/system.css" type="text/css"/>
    <link rel="stylesheet" href="/video/templates/system/css/general.css" type="text/css"/>
    <link rel="stylesheet" href="/video/templates/jisheng2015/css/newStyle/style.css" type="text/css"/>
    <script src="/video/templates/jisheng2015/js/jquery-1.11.3.min.js"></script>
    <script src="/video/templates/jisheng2015/js/jquery.roundabout.min.js"></script>
    <script language="javascript" type="text/javascript"
            src="<?php echo $tmpTools->templateurl(); ?>/js/ja.script.js"></script>

    <!--<script type="text/javascript" src="flash/swfobject.js"></script>
    <script type="text/javascript">
        swfobject.embedSWF("/video/flash/header1.swf", "headerSWFWrapper", "848", "260", "9.0.0", "/video/flash/expressInstall.swf");
    </script>-->
    <script type="text/javascript">
        var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
        document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F1491b841dc916f0a959789735c5fb895' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <style>
        html,body{
            height: 100%;
        }
    </style>
</head>
<body id="bd" class="fs<?php echo $tmpTools->getParam(JA_TOOL_FONT);?> <?php echo $tmpTools->browser();?>" >
<?php
$menus = &JSite::getMenu();
$comp_name = JRequest::getVar('option');
if($comp_name == 'com_home'){
    echo "<script>location.href='/video/index.php?option=com_home_new'</script>";

}else if ($comp_name == 'com_home_new') {
    //Home has no menu
    include "home-wrap.php";
} else {
    include "non-home-wrap.php";
}
?>
</body>
</html>
