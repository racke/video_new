<?php
/**
 * @copyright	Copyright (C) 2005 - 2007 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$menus = &JSite::getMenu();

$aboutCenterMenus = $menus->getItems('menutype', 'about-center');
$mediaColumnsMenus = $menus->getItems('menutype', 'media_columns');
$scienceMenus = $menus->getItems('menutype', 'science');
$legalMenus = $menus->getItems('menutype', 'policy');
$scienceMenus_json = json_encode($scienceMenus);
$menuId = JRequest::getVar('Menuid');

$activeMenu = $menus->getItem($menuId);
$menuParams = new JParameter($activeMenu->params);
$pageHeaderImage = $menuParams->get('pageHeaderImage');
//echo $pageHeaderImage;
$viewType = JRequest::getVar('view', '');
$viewClass = $viewType == 'item' ? 'item-view' : '';

if($pageHeaderImage == null){
    $pageHeaderImage = "/video/templates/jisheng2015/images/header/banner.jpg";
}

function convertLink($menu){
    if(strpos($menu->link, 'latest') == false){
        return $menu->link.'&Menuid='.$menu->id;
    }else{
        return $menu->link.'&Itemid='.$menu->id.'&Menuid='.$menu->id;
    }
}

?>


    <jdoc:include type="head" />
    <!-- OPTIONAL replace it with jQUERY -->
    <?php //JHTML::_('behavior.mootools'); ?>

<!--    <link rel="stylesheet" href="--><?php //echo $tmpTools->baseurl(); ?><!--templates/system/css/system.css" type="text/css" />-->
<!--    <link rel="stylesheet" href="--><?php //echo $tmpTools->baseurl(); ?><!--templates/system/css/general.css" type="text/css" />-->
<!--    <link rel="stylesheet" href="--><?php //echo $tmpTools->templateurl(); ?><!--/css/newStyle/style.css" type="text/css" />-->

<style>
    .container-2015{
        width: 1124px;
        margin: 0 auto;
    }
    .content-wrap{
        padding: 0 25px;
        min-height: 500px;
        background: url("/video/templates/jisheng2015/images/content-bg.jpg") 0 101% no-repeat;
    }
    .item-view .content-wrap{
        /*background: none;*/
    }
    #footer-2015{
        background-color: #e3e8e1;
    }
    #header-2015{
        width: 1024px;
        margin: 0 auto;
    }
    .header-top{
        position: relative;
        height: 50px;
        padding: 10px 0;
        background: url('/video/templates/jisheng2015/images/header/title-bottom.png') 54% 89% no-repeat;
    }
    .header-top > *{
        display: inline-block;
        vertical-align: middle;
    }
    .banner-2015{


    }
    .header-top-right{
        position: absolute;
        right: 435px;
        display: inline-block;
        vertical-align: middle;
        color: #5a5a5a;
        cursor: pointer;
    }
    .header-top-right .menus, .header-top-right img{
        display: inline-block;
        vertical-align: middle;
        margin-left: 40px;
    }
    .header-top-right li{
        position: relative;
        list-style-type: none;
        display: inline-block;
        margin-left: 5px;
    }

    .item-view #containerLeft{
        /*width: 330px;*/
    }
</style>

<script>

    jQuery(document).ready(function(){
        jQuery('.menus li').click(function(){
            jQuery(this).find('ul').slideToggle();
        });

        jQuery('body').click(function(e){
            console.log(jQuery(e.target));
            if(jQuery(e.target).parents('.menus').length){
                return;
            }
            jQuery('.non-home-menu').each(function(){
                if(!jQuery(this).is(':hidden')){
                    jQuery(this).slideToggle();
                }
            })
        });
    });

</script>

<?php  		$document   = &JFactory::getDocument();
$renderer   = $document->loadRenderer('module');
?>

<div class="container-2015 <?php echo $viewClass; ?>">

    <!-- BEGIN: HEADER -->
    <div id="header-2015">
        <div class="header-top">
            <img src="/video/templates/jisheng2015/images/header/e-era.jpg" width="88px"></img>
            <div class="header-top-right">
                <ul class="menus">
                    <li><span>关于中心</span>
                        <ul id="about-center-menu" class="non-home-menu" style="display:none">
                            <?php
                            foreach($aboutCenterMenus as $menu){
                                echo("<a href='".convertLink($menu)."'><li>".$menu->name."</li></a>");
                            }
                            ?>
                        </ul>
                        <span class="separator">/</span>
                    </li>
                    <li>
                        <span>媒体栏目</span>
                        <ul id="science-menu" class="non-home-menu" style="display:none">
                            <?php
                            foreach($mediaColumnsMenus as $menu){
                                echo("<a href='".convertLink($menu)."'><li>".$menu->name."</li></a>");
                            }
                            ?>
                        </ul>
                        <span class="separator">/</span>
                    </li>
                    <li><span>科普园地</span>
                        <ul id="science-menu" class="non-home-menu" style="display:none">
                            <?php
                            foreach($scienceMenus as $menu){
                                echo("<a href='".convertLink($menu)."'><li>".$menu->name."</li></a>");
                            }
                            ?>
                        </ul>
                        <span class="separator">/</span>
                    </li>
                    <li><span>政策法规</span>
                        <ul id="science-menu" class="non-home-menu" style="display:none">
                            <?php
                            foreach($legalMenus as $menu){
                                echo("<a href='".convertLink($menu)."'><li>".$menu->name."</li></a>");
                            }
                            ?>
                        </ul>
                        <span class="separator">/</span>
                    </li>

                </ul>
                <a href="/video/index.php?option=com_home_new"><img src="/video/templates/jisheng2015/images/header/home-icon.png" ></a>
            </div>

            <form name="search-box" action="/video/index.php?option=com_k2&view=itemlist&task=search" method="get">
                <div class="home-search-wrap">
                    <input type="hidden" name="option" value="com_k2">
                    <input type="hidden" name="view" value="itemlist">
                    <input type="hidden" name="task" value="search">
                    <input type="text" name="searchword" class="home-search-input">

                    <div class="home-search-button" onclick="javascript:document.forms['search-box'].submit()">
                        <img src="/video/templates/jisheng2015/images/home/search-button.png"/>
                    </div>
                </div>
            </form>
        </div>
        <img src="<?php echo $pageHeaderImage ?>" alt="" height="165px">

        <div class="headerRight">
            <div class="centerNav">
            </div>


            <div class="clr"></div>
        </div>


        <div class="clr"> </div>
    </div>


    <div class="content-wrap">
        <jdoc:include type="component" />
        <div class="clr"> </div>
    </div>

    <style>
        #footer-2015 .footer-text{
            background: url('/video/templates/jisheng2015/images/footer-logo.png') left center no-repeat;
            margin: 0 auto;
            width: 560px;
            text-align: center;
            height: 40px;
            line-height: 40px;
        }

    </style>
    <!-- BEGIN: FOOTER -->

    <div id="footer-2015" style="position: relative;height: 65px;">
        <div style="position: absolute; bottom: -10px; left: 243px">
            <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/02/000/0000/40222310/CA020000000402223100001.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
        <div class="footer-text" style="height: 65px;line-height: 65px">上海人口和计划生育宣传教育中心&nbsp&nbsp上海人口视听国际交流中心&nbsp&nbsp沪ICP备10029564号-1</div>
    </div>
    <!-- END: FOOTER -->

</div>



