<?php
/**
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
//echo(JPATH_BASE.DS.'libraries'.DS.'joomla'.DS.'document'.DS.'html'.DS.'renderer'.DS.'module.php');
$pageId = $_REQUEST['page_id'];

//http://localhost/video/index.php?option=com_yewupingtai&page_id=quxian
?>
<link rel="stylesheet" href="static/style.css"></link>
<div id="main03">
<div class="left03">
    <div class="l032">
 
<?php if ($pageId == 'quxian'):?>  
    
      <div class="l032_1">区县栏目</div>
      <div class="l032_2">

        <div class="l032_22">
	
为18个区县提供全年12档的有线电视栏目片，每档片长5-10分钟，以“预防出生缺陷”、“流动人口”、“7.11世界人口日”、“预防<br />艾滋病”等为主要内容，是一档宣传人口政策、传播科普知识的电视栏目。

        </div>
        <div class="l032_21 shiji"></div>
      </div>
<?php elseif ($pageId == 'guangbo'):?>    
          <div class="l032_1">广播节目</div>
      <div class="l032_2">
        <div class="l032_22">

《健康人生》广播栏目，由市人口计生委和东方广播电台主办，宣教中心承办，每周一期，栏目紧跟市人口计生委的宣传重点，内容涵盖市民、听众所关心的热点问题，收听率在东广同类栏目中一直稳定在前列。
播出频率：AM792、FM89.9
播出时间：每周六13:00～14:00。


        </div>
        <div class="l032_21 guangbo"></div>
      </div>
      
<?php elseif ($pageId == 'chumo'):?>

<div class="l032_1">触摸屏</div>
      <div class="l032_2">

        <div class="l032_22">
《新世纪中国人口大观》多媒体触摸屏是宣教中心在上海科技信息化建设中研制开发的新型人口与计划生育宣传载体，利用触摸屏技术推广人口和计划生育相关科普知识，使普通群众通过手指轻触显示屏上的图符或文字就可以实现对主机的操作，了解相关信息；触摸屏应用先进的多媒体技术，采用DVD制作标准，装载有超大容量的视音频，画面清晰，内容丰富。
    多媒体触摸屏还有《科普之窗》、《健康城区》等系列，并可根据要求制作个性化版本。



        </div>
        <div class="l032_21 chumo"></div>
      </div>

<?php elseif ($pageId == 'shuma'):?>
<div class="l032_1">跨媒体平台</div>
      <div class="l032_2">

        <div class="l032_22">
	跨媒体传播平台是国家人口计生委于2007年批准的重要基本建设项目，由中国人口宣教中心承担该项目的两期工程，在全国各省市30余个宣教中心设置分站点，上海中心跨媒体平台已于2008年6月完成站点基建和设备调试，可与中国中心进行双相传送。现该跨媒体平台可实现电视电话会议、远程教育培训和数字电影频道三大功能。



        </div>
        <div class="l032_21 shuma"></div>
      </div>
<?php elseif ($pageId == 'pingguo'):?>
<div class="l032_1">青苹果之家</div>
      <div class="l032_2">

        <div class="l032_22">
	全国青少年科技教育基地——青苹果之家已完成基地建设，在原有生殖健康“校园行”活动的成功基础上，充分发挥 “青苹果之家”上海基地的作用，与上海市各区县品牌特色单位联手开展青少年健康教育活动，促进全民人口素质提高。


        </div>
        <div class="l032_21 pingguo"></div>
      </div>
<?php elseif ($pageId == 'yanbo'):?>
      <div class="l032_1">演播厅</div>
      <div class="l032_2">

        <div class="l032_22">
	中心拥有一个120平方米，可进行多机拍摄、现场切换录制的演播室。
2008年末至2009年初,中心对已经使用了20多年的演播室从视频、灯光和土建三大系统进行了技术改造,新添了由Panasonic公司生产 的P2HD系列中的AG-HPX500MC数字高清摄像机、AV-HS400AMC多格式切换台、CJ-28系列台词提示器、喜马拉雅高清非编组成的电视摄制系统；架设了高空移动灯架，添置了CEEDR-048D 48路固定式数字可控硅调光柜和CP-120D电脑调光台，将原来的单控灯光改建成集控可调节灯光系统；改建了空调、新风系统和背景幕布，09年6月正式投入使用。
新系统按国家高清电视标准建设，新系统的使用，能将中心演播室标清电视摄制能力提高到高清电视摄制的技术水平。



        </div>
        <div class="l032_21 yanbo"></div>
      </div>

<?php elseif ($pageId == 'menshi'):?>    
    <div class="l032_1">门市部</div>
      <div class="l032_2">

        <div class="l032_22">
	设在中心一楼，作为中心对外服务的窗口，陈列、展示中心制作或其他相关单位制作的人口、计划生育、生殖健康等方面的文图、视听、实物类宣传品，品种繁多、覆盖面广，满足全市区县乃至全国人口计生系统、目标人群的宣教需求。


        </div>
        <div class="l032_21 menshi"></div>
      </div>
    
    
<?php elseif ($pageId == 'peixun'):?>    
    <div class="l032_1">培训讲座</div>
      <div class="l032_2">

        <div class="l032_22">
	——宣教技能培训。针对上海各区县人口计生专职工作者开设宣<br />教技能培训班，由中心高级专业技术人员作为主要师资，每季度一期，每期40人，通过理论与实践相结合，提高人口计生宣教工作的服务技能。
    <br />&nbsp;&nbsp;&nbsp;&nbsp;——社区公益培训。为市民提供覆盖各年龄层的免费健康知识教育，全年约五十期，已遍及街道、企业、部队，受众达两万余人。



        </div>
        <div class="l032_21 peixun"></div>
      </div>
    
    
    
    
    
<?php endif;//elseif ():?>
 
  </div>
</div>
<?php include('sidebar.php');?>
</div>


