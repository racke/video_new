<?php
/**
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
//echo(JPATH_BASE.DS.'libraries'.DS.'joomla'.DS.'document'.DS.'html'.DS.'renderer'.DS.'module.php');
$pageId = $_REQUEST['page_id'];

//http://localhost/video/index.php?option=com_yewupingtai&page_id=quxian
?>
<link rel="stylesheet" href="static/style.css"></link>
<div id="main05">
	<div class="left05">
	<?php if ($pageId == 'wuye'):?>
	<div class="l05_2">
      <div class="l05_21">
        <div class="l05_211" style="width:300px">物业管理部</div>

      </div>
      <div class="l05_22"></div>
      <div class="l05_23 STYLE1"> 
 		物业管理部承担中心各项后勤保障及物业管理工作。主要的工作涉及：大楼消防、水电、电梯、电话、低值易耗品采购（保修）、车辆、车库、仓库、固定资产、展厅及会议室、治安安全保卫、门卫、大楼卫生、职工各项福利及领导交办的其他工作等。
      </div>
      <div class="l05_24"></div>
      <div class="l05_25"></div>
    </div>
	<?php elseif ($pageId == 'xinxijishu'):?>
	<div class="l05_2">
	<div class="l05_21">
        <div class="l05_211" style="width:300px;">信息技术部</div>

      </div>
      <div class="l05_22"></div>
      <div class="l05_23 STYLE1"> 
 		信息技术部从事以互动多媒体为形式、触摸屏为载<br />体的宣传品开发研制工作，为社区计生咨询中心、文化中心、医院、学校、商务楼等公共场所提供新型的多媒体产品。主要产品有《新世纪中国人口大观》、《人口大观——健康城市版》、《科普之窗——青少年版》。
该部门负责中心历年视听、文图资料的收集和整理，运用视听、文图资料库专用软件对资料进行系统管理。
该部门负责全中心120平方米的3讯道带编辑功能的广播级高清演播室，以及广播级的高清和标清摄像机、网络功能的专业编辑机房、编辑系统和录音设备的管理，为高质量的视频节目拍摄和制作提供保证。
该部门负责管理中心70多平方米、45座的跨媒体数码影院，该影院具有电视电话会议、远程教育培训以及电影播放等功能。

      </div>
      <div class="l05_24"></div>
      <div class="l05_25"></div>
    </div>
	
	<?php elseif ($pageId == 'dianshizhizuo'):?>
 <div class="l05_2">
      <div class="l05_21">
        <div class="l05_211" style="width:300px">电视制作部</div>

      </div>
      <div class="l05_22"></div>
      <div class="l05_23 STYLE1"> 
 		电视制作部是从事视听节目策划、制作的专业部门。专业人才集中，专业门类齐全，拍摄制作能力强大，具有广泛的社会网络资源,与上海市各区（县）人口计生委、上海市各委办局等相关部门、外省市人口计生委和宣教中心、社会团体、知名企业有着广泛的合作。
电视部每年紧扣国家和市人口计生委的宣教主题，制作一定数量的科教片、专题片以及公益广告，结合各时期的形势、任务以及相关的节日、纪念日，制作数十余档专题栏目片，在上海各区县的有线电视台播出。这些栏目片以科普教育为最大特色，为全国各省市、地县人口计生电视专栏节目提供强大的片源支持。

      </div>
      <div class="l05_24"></div>
      <div class="l05_25"></div>
    </div>
	
<?php elseif($pageId == 'kepuzhizuo'):?>
<div class="l05_2">
      <div class="l05_21">
        <div class="l05_211">科普合作部</div>

      </div>
      <div class="l05_22"></div>
      <div class="l05_23 STYLE1"> 
 		科普合作部不仅是中心对外合作的窗口部门，同时<br />也是从事人口计生文图宣传品创意、策划、编撰和设计的专业部门。部门所开展的各项科普合作业务范围广、业务内容丰富，为上海乃至全国提供优质的人口计生宣教服务。
<br />&nbsp;&nbsp;&nbsp;&nbsp;部门开展的科普合作范围主要包括人口计生文图宣传品开发、人口计生宣教技能和科普知识培训及国家、上海市重点项目宣传品发行等。部门拥有一系列具有前瞻性和深受群众欢迎的特色项目，如生殖健康校园行、青苹果之家、优孕学校等。涵盖了从婴幼儿早期教育到老年人养生保健，从城市社区生活到新农村建设，从科普知识介绍到政策问答等各方面。

      </div>
      <div class="l05_24"></div>
      <div class="l05_25"></div>
    </div>
    
<?php elseif($pageId == 'bangongshi'):?>
<div class="l05_2">
      <div class="l05_21">
        <div class="l05_211" style="width:300px">(综合)办公室</div>

      </div>
      <div class="l05_22"></div>
      <div class="l05_23 STYLE1"> 
 		（综合）办公室承担着党务、人事、财务、行政和<br />业务管理的综合职能。主要职能：
<br />1.负责日常党务工作，抓好党的建设和员工的思想政治工作；做好培养干部，发展党员的考核审查等工作；协调工、青、妇、团组织开展各项活动。
<br />2.负责员工聘用、考勤、人员调配、部门设置、奖惩考核、工资福利、职称职务、人事档案、退休退职、招聘返聘等工作的管理；处理机要保密文件和经办纪检监察相关工作。
<br />3.负责中心资金编制与安排，包括编制财政预算资金、组织各项业务收入、掌握资金使用方向；做好各类资金的会计核算工作，加强对各类资金的监督和控制，做好财务数据分析和预测；做好员工工资福利发放。
<br />4.负责项目管理，协调中心各部门和项目负责人做好重点财政项目的申请、实施和评估，及时跟踪、掌握项目进度；落实中心业务合同的会签、宣传品审核、制作报价，开具任务单；落实中心全年业务学习计划和员工继续教育。
<br />5.按照中心各项制度和领导要求，完成行政以及业务会议记录、督察督办，做好接待联络、公文处理、信息沟通、综合统计、档案管理等综合事务管理及领导交办的其他工作。

      </div>
      <div class="l05_24"></div>
      <div class="l05_25"></div>

    </div>
<?php elseif($pageId == 'index'):?>
<div class="l05_2">
      <div class="l05_21">
        <div class="l05_211">中心简介</div>

      </div>
      <div class="l05_22"></div>
      <div class="l05_23 STYLE1"> 
 	上海人口和计划生育宣传教育中心（上海人口视听<br />国际交流中心），作为联合国人口基金与中国政府合作项目实施单位之一，成立于1980年3月，是专业从事人口、计划生育、生殖健康宣传教育项目的多功能大众传播机构。
中心是上海市人口和计划生育委员会下属的公益性事业单位，也是国家人口和计划生育委员会批准从事国际合作及对外宣传的专业机构。
<br />&nbsp;&nbsp;&nbsp;&nbsp;中心拥有一批经过国内外专业培训、具有较高专业水平的高中级技术人才和一系列专业水准的视听制作设备、设施及现代传媒技术手段，并提倡运用高科技手段开发创新各类文图、视听、实物宣传品。
<br />&nbsp;&nbsp;&nbsp;&nbsp;多年来，中心全体同仁积极进取、开拓创新、立足上海、面向全国，在增进人口与计划生育、生殖健康大众传播教育，为广大基层社区和育龄人群提供优质服务方面，在紧跟世界潮流，开展国际交流、合作、培训及对外宣传等方面都取得了丰硕成果，赢得了广泛的社会赞誉。

      </div>
      <div class="l05_24"></div>
      <div class="l05_25"></div>
    </div>


<?php elseif ($pageId == 'lingdaobanzi'):?>
<div class="l05_2">
      <div class="l05_21">
        <div class="l05_211">领导班子</div>

      </div>
      <div class="l05_22"></div>
      <div class="l05_23 STYLE1 no-indent"> 
        <div class="l05_231">
          <div class="l05_2311"><img src="static/pic/main05-ld1.jpg" /></div>
         
          <div class="l05_2312">
            胡冰：中心总支委员会书记；中心主任；中心法人代表。 
            <br />&nbsp;&nbsp;&nbsp;全面负责中心党政工作；分管(综合)办公室，主抓人事、财务工作。
          </div>
        </div> 
        <div class="l05_231">

          <div class="l05_2311"><img src="static/pic/main05-ld2.jpg" /></div>
         
          <div class="l05_2312">杨永德：中心副主任。<br />
               &nbsp;&nbsp;&nbsp;分管科普合作部工作；分管物业管理部工作。
          </div>
        </div>
        <div class="l05_231">
          <div class="l05_2311"><img src="static/pic/main05-ld3.jpg" /></div>
         
          <div class="l05_2312">程海铭：中心副主任；中心工会主席。<br />

               &nbsp;&nbsp;&nbsp;协助中心主任抓(综合)办公室的行政、业务管理工作。
 
          </div>
        </div>
        <div class="l05_231">
          <div class="l05_2311"><img src="static/pic/main05-ld4.jpg" /></div>
         
          <div class="l05_2312">
            乔方：中心总支委员会副书记；中心副主任。 <br />
               &nbsp;&nbsp;&nbsp;协助总支书记抓党务工作；分管电视制作部、信息技术部工作。
          </div>

        </div>
      </div>
      <div class="l05_24"></div>
      <div class="l05_25"></div>
    </div>
    
    <?php elseif($pageId == 'rencaiduiwu'):?>
    <div class="l05_2">
      <div class="l05_21">
        <div class="l05_211">人员介绍</div>

      </div>
      <div class="l05_22"></div>
      <div class="l05_23 STYLE1"> 
 		现有在编员工80人，女性35人，男性45人。其中管理人员10人,专业技术人员62人，工勤人员8人。其中高级职称8人，占总人数的10%，中级职称22人，占总人数的27.5%，初级职称30人，占总人数的37.5%，已初步形成了以大专以上学历为主体，大学本科以上学历为骨干的人口计生宣教队伍。（截止2009年底）
      </div>
      <div class="l05_24"></div>
      <div class="l05_25"></div>
    </div>
<?php endif;?>		
	</div>
	<?php include('sidebar.php');?>
</div>