﻿<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

$menus = &JSite::getMenu();
$aboutCenterMenus = $menus->getItems('menutype', 'about-center');
$mediaColumnsMenus = $menus->getItems('menutype', 'media_columns');
$scienceMenus = $menus->getItems('menutype', 'science');
$legalMenus = $menus->getItems('menutype', 'policy');

$externalLinks = $menus->getItems('menutype', 'external_links');
$carouselLinks = $menus->getItems('menutype', 'carousel');;
$aboutCenterMenus_json = json_encode($aboutCenterMenus);
$scienceMenus_json = json_encode($scienceMenus);
fb($externalLinks);
fb($carouselLinks);
foreach (@$scienceMenus as $menu) {
//    fb($menu->name);
//    fb($menu->link);

}

$relatedLinks = array(
    '网站   : 宣教e时代' => 'http://www.sicpc.com',
    '手机网站   : 健康e随行' => 'http://m.sicpc.com',
    '微信   : 上海微健康' => 'http://',
    '手机网站   : 健康e随行2' => 'http://m.sicpc.com',
    '手机网站   : 健康e随行3' => 'http://m.sicpc.com',
    '手机网站   : 健康e随行4' => 'http://m.sicpc.com',
    '手机网站   : 健康e随行5' => 'http://m.sicpc.com'
);

//$carouselLinks = array(
//    array('image' => '/video/templates/jisheng2015/images/home/feature1.png', 'link' => 'http://www.google.com'),
//    array('image' => '/video/templates/jisheng2015/images/home/feature2.png', 'link' => 'http://www.baidu.com'),
//    array('image' => '/video/templates/jisheng2015/images/home/feature3.png', 'link' => 'http://www.taobao.com'),
//
//);

$featuredItems = array();
$limit = 8;
$db = &JFactory::getDBO();

$query = "SELECT i.title, i.id";
$query .= " FROM #__k2_items as i ";
$query .= " WHERE i.featured=1";
$query .= " AND i.trash!=1";
$query .= " ORDER BY i.modified DESC, i.ordering DESC";
fb('main video query: ' . $query);
$db->setQuery($query, 0, $featuredItems);
$featuredItems = $db->loadObjectList();

function convertLink($menu){
    if(strpos($menu->link, 'latest') == false){
        return $menu->link.'&Menuid='.$menu->id;
    }else{
        return $menu->link.'&Itemid='.$menu->id.'&Menuid='.$menu->id;
    }
}


?>


<style>

</style>
<script>
    //about center carousel
    $(document).ready(function () {
        $('.tree-menu').on('click', function () {
            $(this).find('ul').slideToggle();
        });

        $('.tree-menu ul').on('mouseenter', function(){
            if($(this).is(':hidden')){
                return;
            }
            $(this).one('mouseleave', function(){
                $(this).slideToggle();
            });
        });
        $('body').click(function(e){
            if($(e.target).parents('.tree-menu').length){
                return;
            }
            $('.tree-menu ul').each(function(){
               if(!$(this).is(':hidden')){
                   $(this).slideToggle();
               }
            });
        });


        (function ($) {
            $('#carousel').roundabout({
                minScale: 0.5,
                childSelector: "li",
                autoplay: true,
                autoplayDuration: 3000,
                autoplayPauseOnHover: true
            });

        }(jQuery));
        var articleContainer = $('.home-articles ul');
        var homeArticlesHeight = articleContainer.outerHeight(true);
        var containerHeight = $('.home-articles').height();
        var animateDuration = containerHeight * 50
        var completeCb = function () {
            articleContainer.css({top: containerHeight})
                .animate({top: -homeArticlesHeight}, animateDuration, 'linear', completeCb);
        }
        articleContainer
            .animate({top: -homeArticlesHeight}, animateDuration, 'linear', completeCb);
        $('.home-articles').mouseover(function () {
            articleContainer.stop();
        }).mouseleave(function () {
            articleContainer
                .animate({top: -homeArticlesHeight}, animateDuration, 'linear', completeCb);
        });

    });
</script>

<form name="search-box" action="/video/index.php?option=com_k2&view=itemlist&task=search" method="get">
    <div class="home-search-wrap">
        <input type="hidden" name="option" value="com_k2">
        <input type="hidden" name="view" value="itemlist">
        <input type="hidden" name="task" value="search">
        <input type="text" name="searchword" class="home-search-input">

        <div class="home-search-button" onclick="javascript:document.forms['search-box'].submit()">
            <img src="/video/templates/jisheng2015/images/home/search-button.png"/>
        </div>
    </div>
</form>

<div class="wrap">
    <div class="left-margin"></div>
    <div class="container">
        <div class="about-center-icon tree-menu" data-left="100" data-top="50">
            <img src="/video/templates/jisheng2015/images/home/about-center.png" alt="" width="130px" height="130px"/>
            <ul id="about-center-menu" class="home-menu" style="display:none">
                <?php
                foreach ($aboutCenterMenus as $menu) {
                    echo("<a href='" . convertLink($menu) . "'><li>" . $menu->name . "</li></a>");
                }
                ?>
            </ul>
        </div>

        <div class="media-column-icon tree-menu" data-left="80" data-top="240">
            <img src="/video/templates/jisheng2015/images/home/media-column.png" alt="" width="100px" height="100px"/>
            <ul id="media-colimn-menu" class="home-menu" style="display:none;top:65px;right:-50px">
                <?php
                foreach ($mediaColumnsMenus as $menu) {
                    echo("<a href='" . convertLink($menu) . "'><li>" . $menu->name . "</li></a>");
                }
                ?>
            </ul>
        </div>
        <div class="legal-icon tree-menu" data-left="400" data-top="250">
            <img src="/video/templates/jisheng2015/images/home/legal.png" alt="" width="100px" height="100px"/>
            <ul id="legal-menu" class="home-menu" style="display:none;right: -28px;">
                <?php
                foreach ($legalMenus as $menu) {
                    echo("<a href='" . convertLink($menu) . "'><li>" . $menu->name . "</li></a>");
                }
                ?>
            </ul>
        </div>
        <div class="science-base-icon tree-menu" data-left="400" data-top="80">
            <img src="/video/templates/jisheng2015/images/home/science-base.png" alt="" width="100px" height="100px"/>
            <ul id="science-menu" class="home-menu" style="display:none;top: 48px;right: -55px;">
                <?php
                foreach ($scienceMenus as $menu) {
                    echo("<a href='" . convertLink($menu) . "'><li>" . $menu->name . "</li></a>");
                }
                ?>
            </ul>
        </div>

        <div class="related-links">
            <?php

            foreach ($externalLinks as $link) {
                echo '<div><a href="' . $link->link . '">' . $link->name . '</a></div>';
            }
            ?>
        </div>

        <div class="home-articles">

            <ul>

                <?php
                $index = 0;
                foreach ($featuredItems as $item) {
                    ?>
                    <li>
                        <a href="/video/index.php?option=com_k2&view=item&id=<?php echo $item->id; ?>"><?php echo $item->title; ?></a>
                    </li>


                    <?php
                    $index++;
                } ?>

                <div class="clr"></div>
            </ul>
        </div>

        <div id="carousel-wrap">
            <ul id="carousel" class="top_slider roundabout-holder">
                <?php
                foreach($carouselLinks as $carousel){
                    ?>
                    <li>
                        <a href="<?php echo $carousel->link?>">
                            <img src="<?php echo $carousel->name?>" alt="" class="slide"/>
                        </a>
                    </li>
                    <?php
                }
                ?>


            </ul>
        </div>
        <div style="position: absolute; bottom: -10px; left: 310px">
            <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/02/000/0000/40222310/CA020000000402223100001.js' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
        <div class="home-footer">
            <script type="text/javascript">document.write(unescape("%3Cspan id='_ideConac' %3E%3C/span%3E%3Cscript src='http://dcs.conac.cn/js/02/000/0000/40222310/CA020000000402223100001.js' type='text/javascript'%3E%3C/script%3E"));</script>
            上海人口和计划生育宣传教育中心  上海人口视听国际交流中心  沪ICP备10029564号-1
        </div>

        <?php
        $document = &JFactory::getDocument();
        $renderer = $document->loadRenderer('module');
        //__::each(array('a', 'b', 'c'), function($s){
        //    echo 'item is '.$s;
        //});
        //$content = $renderer->render(JModuleHelper::getModule('k2_content', 'most_popular_main'));
        //fb($content);
        //echo $renderer->render(JModuleHelper::getModule('user4'));

        ?>


        <?php
        //        echo $renderer->render(JModuleHelper::getModule('k2_tools', 'k2搜索'));
        //echo $renderer->render(JModuleHelper::getModule('k2_content', 'main_happiness' ));
        ?>
        <?php
        //echo $renderer->render(JModuleHelper::getModule('k2_content', 'main_broadcast' ));
        ?>

        <?php
        //echo $renderer->render(JModuleHelper::getModule('k2_content', 'main_region_policy' ));
        ?>
    </div>
</div>
