<?php
/**
 * @version		$Id: category.php 478 2010-06-16 16:11:42Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
  <div id="containerPolicy">
  	<div class="policyContext">
    	<div class="title"><?php echo $this->params->get('titleImage');?></div>
    	<?php foreach($this->primary as $key=>$item): ?>
    	<p><a href="<?php echo $item->link;?>"><?php echo $item->title;?></a></p>
    	<?php endforeach;?>

        <!--  <p>关于下发《上海市再生育子女办理规定》</p>
        <p>上海市城镇生育保险办法</p>
        <p>中华人民共和国婚姻法</p>
        <p>中华人名共和国国务院令</p>
        <p>关于下发《上海市再生育子女办理规定》</p>
        <p>上海市城镇生育保险办法</p>
        <p>中华人民共和国婚姻法</p>
        <p>中华人名共和国国务院令</p>
        <p>关于下发《上海市再生育子女办理规定》</p>
        <p>上海市城镇生育保险办法</p>
        <p>中华人民共和国婚姻法</p>
        <p>中华人名共和国国务院令</p>
        <p>关于下发《上海市再生育子女办理规定》</p>
        <p>上海市城镇生育保险办法</p>
        <p>中华人民共和国婚姻法</p>
        <p>中华人名共和国国务院令</p><p>关于下发《上海市再生育子女办理规定》</p>
        <p>上海市城镇生育保险办法</p>
        <p>中华人民共和国婚姻法</p>
        <p>中华人名共和国国务院令</p>
        <p>关于下发《上海市再生育子女办理规定》</p>
        <p>上海市城镇生育保险办法</p>
       -->
       
    </div>
    	<?php if(count($this->pagination->getPagesLinks())): ?>
	<div class="k2Pagination">
		<?php if($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>
		<div class="clr"></div>
		<?php if($this->params->get('catPaginationResults')) echo $this->pagination->getPagesCounter(); ?>
	</div>
	<?php endif; ?>
  </div>
