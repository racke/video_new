<?php
/**
 * @version		$Id: category.php 478 2010-06-16 16:11:42Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<div id="containerLeft">
    <div class="ranking-wrap">
    <div class="subPageRankingTitle">点击排行</div>
    <!--<h2>category_video.php</h2>-->
    <?php 
 		$document   = &JFactory::getDocument();
   		$renderer   = $document->loadRenderer('module');
		echo $renderer->render(JModuleHelper::getModule('k2_content', 'most_popular_main'));
    ?>
  
    <div class="newUploadTitle"> </div>
    <?php
   		echo $renderer->render(JModuleHelper::getModule('k2_content', 'latest_videos_main' ));
    ?>
    </div>
</div>

<div id="containerRight">

    <div class="subPageRightLittleTitle"><?php echo $this->category->name; ?></div>
    <?php $i=0;?>
    <?php $size = count($this->primary);?>
		<?php for(; $i<$size; $i++ ): ?>
	    <?php if($i==0): ?><div class="subPageRightList"><?php endif;?>
	    <?php if($i%4==0 && $i!=0): ?><div class="clr"></div></div><div class="subPageRightList"><?php endif;?>
	    <?php $item = $this->primary[$i];?>
	          <div class="rankingList noDotLine">
                <a href="<?php echo $item->link;?>">
                <img class="rankingListPic_M" src="<?php echo $item->imageGeneric2;?>"></img>
                </a>
                <div class="clr"></div>
              </div>
    	<?php endfor;?>
 		<div class="clr"></div>
 		</div>   

</div>
    

