<?php
/**
 * @version		$Id: latest_video.php 501 2010-06-24 19:25:30Z joomlaworks $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div id="containerLeft">
  	<div class="ranking-wrap">
    <div class="subPageRankingTitle"> 点击排行</div>
    <?php
 		$document   = &JFactory::getDocument();
   		$renderer   = $document->loadRenderer('module');
        $module = JModuleHelper::getModule('k2_content', 'most_popular_main');
		echo $renderer->render($module);
    ?>
    </div>
    <div class="ranking-wrap">
        <div class="subPageRankingTitle"> 新品上传</div>
    <div class="newUploadTitle"> </div>
    <?php
   		echo $renderer->render(JModuleHelper::getModule('k2_content', 'latest_videos_main' ));
    ?>
    </div>
  </div>

  <div id="containerRight">
    <!--  <div class="subPageRightTitle"> </div>-->
      <div class="ranking-wrap">
    <img src="<?php echo $this->params->get('titleImage');?>"></img>
    <?php foreach($this->blocks as $key=>$block): ?>
        <?php if($this->source=='categories'): $category=$block; ?><?php endif; ?>
        <div class="subPageRightLittleTitle"><a href="<?php echo $category->link; ?>"><?php echo $category->name; ?></a></div>
        <div class="subPageRightList">
    	<?php 

    		$size = count($block->items);
    		// if($size >= 5)
    		// 	$size = 4;
    	?>
    	<?php //foreach ($block->items as $itemCounter=>$item): ?>
    	<?php for ($i = 0; $i < $size; ++$i): ?>
    	<?php $item = $block->items[$i]; fb('Media type is: '.$item->params->get('mediaType'));?>
		<div class="rankingList noDotLine">
            <!--<?php echo $item->title;?>-->
                <a href="<?php echo $item->link;?>">
                <img class="rankingListPic_M" src="<?php echo $item->imageGeneric2;?>" ></img>
                </a>
                <div class="clr"></div>
              
      	</div>
    <?php endfor;?>
            <a href="<?php echo $category->link; ?>" style="position: absolute;right: 0px;bottom: 0px;">
                <span style="color:green;font-weight: bold;font-size: 14px">更多</span>
            </a>
    <div class="clr"></div>
    </div>
    <?php endforeach;?>
      </div>
  </div>
