<?php
/**
 * @version		$Id: item.php 558 2010-09-22 11:25:17Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


?>


<script type="text/javascript">
var wentu = {
	images : []
};
</script>


<!--containerLeft Star-->
<div id="containerLeft"><?php 
$document   = &JFactory::getDocument();
$renderer   = $document->loadRenderer('module');
echo $renderer->render(JModuleHelper::getModule('k2_content', 'content_most_popular'));
//fb('imageName: '.JRequest::getString('imageName'));
$plg_tag = "gallery";
$regex = "#{".$plg_tag."}(.*?){/".$plg_tag."}#s";
$gallery_str = $this->item->gallery;
// find all instances of the plugin and put them in $matches
preg_match_all( $regex, $gallery_str, $matches );
// Number of plugins
$count = count( $matches[0] );
// fb($count);
// plugin only processes if there are any instances of the plugin in the text
if ( $count ) {
	$site_root_path = JPATH_SITE;
	$site_http_path = JURI::base();
	$root_folder = 'media/k2/galleries';
	$gal_folder = preg_replace("/{.+?}/", "", $matches[0][0]);
	$srcimgfolder = $root_folder.'/'.$gal_folder.'/';


	fb($site_root_path.'/'.$root_folder.'/'.$gal_folder.'/');
	// Execute code if the folder can be opened, or fail silently
	if ($dir = @ opendir($site_root_path.'/'.$root_folder.'/'.$gal_folder.'/')) {
		// initialize an array for matching files
		$found = array();
			
		// Create an array of file types
		$fileTypes = array('jpg','jpeg','gif','png');
			
		// Traverse the folder, and add filename to $found array if type matches
		while (false !== ($item = readdir($dir))) {
			$fileInfo = pathinfo($item);
			if (array_key_exists('extension', $fileInfo) && in_array(strtolower($fileInfo['extension']),$fileTypes)) {
				$found[] = $item;
			}
		}
			
		// Check if the $found array is not empty
		if ($found) {

			// Sort array
			switch ($sortorder) {
				case 0: sort($found); break;
				case 1: natsort($found); break; // Sort in natural, case-sensitive order
				case 2: natcasesort($found); break; // Sort in natural, case-insensitive order
				case 3: rsort($found); break;
				case 4: shuffle($found); break;
			}
		}
		?> <script type="text/javascript">
			<?php 
			//fb($found);
			for ($i=0; $i<count($found); $i++){ 
				// Assign source image and path to a variable
				$filename = $found[$i];
				$imageUrl = $site_http_path.$srcimgfolder.$filename;
			?>
				wentu.images.push('<?php echo $imageUrl?>');
			<?php 				
			}
			?>

			</script> <?php 
	}
}

$current_url = "http://" . $_SERVER['HTTP_HOST']  . $_SERVER['REQUEST_URI'];
parse_str($_SERVER['QUERY_STRING'], $url_vars);
//fb($url_vars);
$url_vars['imageName'] = 'new_image_name.jpg';
$new_url = "http://" . $_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'] . "?" .http_build_query($url_vars);
//fb($new_url);

$first_image = $site_http_path.$srcimgfolder.$found[0];
?></div>
  <!--containerRight Star-->
<div id="containerRight" class="showPageContainer">
  	<div class="leftTitle"><?php echo $this->item->title;?></div>
    <div class="content"> 
	<image id="imageHolder" width=515 height=630 src="<?php echo $first_image;?>" onclick="window.open(document.getElementById('imageHolder').src);" style="cursor:pointer;"></image>
    </div>
    <!--  <div class="bottomNumber">13</div>-->
    <div class="previewPage" style="display:none;"><a href="#" onclick="previousImage();return false;">上一页</a></div>
    <div class="nextPage"><a href="#" onclick="nextImage(); return false;">下一页</a></div>

</div>

<script type="text/javascript">
	window.addEvent('domready', function(){ 
//		alert('ready');
		//imageHolder = $E('img','imageHolder1');
		imageHolder = $('imageHolder');
		//imageHolder = document.getElementById('imageHolder1');
		//console.log(imageHolder);
		//console.log($E('a','imageHolder1'));
	});
	//console.log(wentu.images);
    var pageNum = 0;
   	var imageHolder;
	function previousImage (){
		pageNum--;
		imageHolder.src=wentu.images[pageNum];
		$$('.nextPage').setStyle('display', 'block');
		if(pageNum <= 0 ){
			$$('.previewPage').setStyle('display', 'none');
		}else{
			$$('.previewPage').setStyle('display', 'block');
		}
		return false;
	}

	function nextImage (){
		pageNum++;
		imageHolder.src=wentu.images[pageNum];
		$$('.previewPage').setStyle('display', 'block');
		if(pageNum >= (wentu.images.length-1)){
			$$('.nextPage').setStyle('display', 'none');
		}else{
			$$('.nextPage').setStyle('display', 'block');
		}
		return false;
	}
	
</script>
<!--  <link
	href="<?php echo JURI::root();?>components/com_k2/views/item/tmpl/static/wentu.css"
	rel="stylesheet" type="text/css" />-->

