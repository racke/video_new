<?php
/**
 * @version		$Id: item.php 558 2010-09-22 11:25:17Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


//fb($this->item);
//fb($this->item->category);
//fb($this->item->image);
?>

 <div id="containerCenterNews">
      <div class="centerNewsContext">
      	<div class="<?php echo($this->item->category->extraClass);?> title ">

            <div class="word">
		<?php echo($this->item->title);?> 
			</div>
            <div class="clr"> </div>
        </div>
        <?php if($this->item->image!=null):?>
        <div class="image">
        <img src="<?php echo($this->item->imageMedium);?>" ></img>
        </div>
		<?php endif;?>
		
       	<?php  echo str_replace('{K2Splitter}', '',$this->item->text); ?>
        

        
        
      </div>     
  </div>
