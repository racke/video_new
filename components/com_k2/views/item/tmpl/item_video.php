<?php
/**
 * @version		$Id: item.php 558 2010-09-22 11:25:17Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.gr
 * @copyright	Copyright (c) 2006 - 2010 JoomlaWorks, a business unit of Nuevvo Webware Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
$siteUrl  = substr(JURI::root(), 0, -1);
$plgUrl = "/plugins/content/jw_allvideos/includes/players/jw_player5.6";
$swfUrl = $siteUrl.$plgUrl."/player.swf";
$jsUrl = $siteUrl.$plgUrl."/jwplayer.js";
fb($this->item->video);
$file = '';
if($this->item->video){
	$tagcontent = preg_replace("/{.+?}/", "", $this->item->video);
	$regexFlv = "#{flv}(.*?){/flv}#s";
	$regexRemote = "#{flvremote}(.*?){/flvremote}#s";
	$regexMp3 = "#{mp3}(.*?){/mp3}#s";			
	// process tags
	if (preg_match_all($regexFlv, $this->item->video, $matches, PREG_PATTERN_ORDER) > 0){
		$file = '"media/k2/videos/'.$tagcontent.'.flv"';
	}else if(preg_match_all($regexRemote, $this->item->video, $matches, PREG_PATTERN_ORDER) > 0){
		if($tagcontent == ''){
			$file = 'filenotexist.flv';
		}
		$file = '"'.substr($tagcontent, 1).'"';
	}else{
		//$file = '"media/k2/videos/'.$tagcontent.'.mp3"';
        $file = '"'.substr($tagcontent, 1).'"';
	}
}
?>
<script type="text/javascript" src="<?php echo $jsUrl;?>"></script>

<!-- Start K2 Item Layout -->
<span id="startOfPageId<?php echo JRequest::getInt('id'); ?>"></span>

<div id="containerLeft">
    <?php 
 		$document   = &JFactory::getDocument();
   		$renderer   = $document->loadRenderer('module');
		echo $renderer->render(JModuleHelper::getModule('k2_content', 'content_most_popular'));
		
		//fb($this->item->imageXLarge);
		//fb($this->item->imageLarge);
    ?>
  </div>
  
  <div id="containerRight">
  	<div class="videoContainer">
    	<div class="video">
    		<div id="main_video"></div>
        </div>
        <div class="description" >
        	<span class="title"> 主创人员： </span>
		<?php fb($this->item);?>
            <!--<p><?php echo $this->item->introtext;?></p>-->
		<?php $credits = explode(',', $this->item->video_credits);?>
		<?php foreach($credits as $credit):?>
            <p style="margin-left:12px"><?php echo $credit;?></p>
		<?php endforeach;?>
        </div>
        <div class="clr">
        </div>
    </div>
    
   <?php if($this->item->params->get('itemRelated') && isset($this->relatedItems)): ?>
  <!-- Related items by tag -->
    <div class="relatedVideos">
    <div class="videoTitle"></div>
			<?php foreach($this->relatedItems as $key=>$item): ?>
				<div class="videoItem">
			    <a href="<?php echo $item->link ?>"><image class="videoImage" src="<?php echo $item->imageSmall;?>"></image>
    			<div class="relatedTitle"><?php echo $item->title; ?></div>
				</a>
				</div>
			<?php endforeach; ?>
	
    
    
    <div class="clr">
    </div>

		<!-- UY BEGIN -->
		<div id="uyan_frame"></div>
		<script type="text/javascript" src="http://v2.uyan.cc/code/uyan.js?uid=2086915"></script>
		<!-- UY END -->
  </div>
  <?php endif; ?>
  <?php if($file && !empty($file)):?>
  <script>jwplayer('main_video').setup({
    autostart: true, 
    repeat:'always',
	flashplayer: '<?php echo $swfUrl;?>', 
    file: <?php echo $file;?>,
	<?php if(strpos($file, '.flv') === false):?>
   	image: "<?php echo $this->item->imageXLarge;?>",
	<?php endif; ?>
	height: 375, 
	width: 471,
    <?php if(strpos($file, '.flv') != false):?>
	streamer: "<?php echo $siteUrl; ?>/xmoov.php",    
	provider: 'http',
    <?php endif;?>
	'controlbar.position': 'bottom',
	'controlbar.idlehide': true,
	'viral.oncomplete': 'false',
	'viral.pause': 'false'
	}); 
  </script>  
<?php endif;?>
</div>
